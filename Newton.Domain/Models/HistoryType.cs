﻿using System.Collections.Generic;

namespace Newton.Domain.Models
{
    public class HistoryType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<HistorySubType> HistorySubTypes { get; set; }
    }
}