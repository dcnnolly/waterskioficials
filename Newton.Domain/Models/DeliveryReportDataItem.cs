﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public class DeliveryReportDataItem : BaseReportDataItem
    {
        public string REPORTSTATUS { get; set; }
        public DateTime DATEADDED { get; set; }
        public int DNOTENUM { get; set; }
        public DateTime CREATDAT { get; set; }
        public string CREATEBY { get; set; }
        public DateTime CHECKDAT { get; set; }
        public string CHECKBY { get; set; }
        public string INVNUMBE { get; set; }
        public string ORDRDBY { get; set; }
        public DateTime ORDRDATE { get; set; }
        public string SOPNUMBE { get; set; }
        public string CUSTNAME { get; set; }
        public string CSTPONBR { get; set; }
        public string EUSERPONUM { get; set; }
        public string CNTCPRSN { get; set; }
        public string SLPRSNID { get; set; }
        public string SHIPADD1 { get; set; }
        public string SHIPADD2 { get; set; }
        public string SHIPADD3 { get; set; }
        public string SHIPADD4 { get; set; }
        public string SHIPADD5 { get; set; }
        public string SHIPADD6 { get; set; }
        public string SHIPADD7 { get; set; }
        public string SHIPADD8 { get; set; }
        public string SHIPADD9 { get; set; }
        public int LNITMSEQ { get; set; }
        public string VENDOR { get; set; }
        public string ITEMNMBR { get; set; }
        public string ITEMDESC { get; set; }
        public int QUANTITY { get; set; }
        public int QTYTPICK { get; set; }
        public string COMMENT1 { get; set; }
        public string COMMENT2 { get; set; }
        public string COMMENT3 { get; set; }
        public string COMMENT4 { get; set; }
    }
}
