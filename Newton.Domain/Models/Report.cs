﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public class Report
    {
        public Report()
        {
            ReportColumns = new List<ReportColumn>();
            Contacts = new List<ReportContact>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public ReportType ReportType { get; set; }
        public ReportFormat ReportFormat { get; set; }
        public ReportScheduleFrequency ReportScheduleFrequency { get; set; }
        public DateTime? ScheduleStartTime { get; set; }
        public DateTime? ScheduleEndTime { get; set; }
        public DateTime? ExecutionTime { get; set; }
        public int? ExecutionDayOfWeek { get; set; }
        public DateTime? ExecutionDay { get; set; }
        public string CustomerNumber { get; set; }
        public bool IncludeHeaders { get; set; }
        public bool Consolidate { get; set; }
        public ICollection<ReportContact> Contacts { get; set; }
        public ICollection<ReportColumn> ReportColumns { get; set; }
    }
}
