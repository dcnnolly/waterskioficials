﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public class ReportContact
    {
        public int Id { get; set; }
        public int ReportId { get; set; }
        public int ContactId { get; set; }
        public DateTime? LastProcessed { get; set; }

        public virtual Contact Contact { get; set; }
        public virtual Report Report { get; set; }
    }
}
