﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public enum ReportScheduleFrequency
    {
        Hourly = 1,
        Daily = 2,
        Weekly = 3,
        Monthly = 4
    }
}
