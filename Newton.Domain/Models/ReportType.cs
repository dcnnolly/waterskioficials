﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public enum ReportType
    {
        [Display(Name = "Invoice Report")]
        InvoiceReport = 1,
        [Display(Name = "Delivery Report")]
        DeliveryReport = 2,
        [Display(Name = "Serial Number Report")]
        SerialNumberReport = 3,
    }
}
