﻿using System;
using System.Collections.Generic;

namespace Newton.Domain.Models
{
    public class Contact
    {
        public Contact()
        {
            Roles = new List<Role>();
            Reports = new List<ReportContact>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string AlternateEmail { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public bool IsDeleted { get; set; }
        public string DeletedBy { get; set; }
        public string CustomerNumber { get; set; }

        public int? CountryId { get; set; }
        public int? RegionId { get; set; }

        public virtual Country Country { get; set; }
        public virtual Region Region { get; set; }

        public virtual ICollection<Role> Roles { get; set; }
        public virtual ICollection<ReportContact> Reports { get; set; }
    }
    
}