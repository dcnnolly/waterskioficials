﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public class Role
    {
        public Role()
        {
            Contacts = new List<Contact>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsUnique { get; set; }
        public virtual ICollection<Contact> Contacts { get; set; }
    }
}
