﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public class ReportColumn
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ColumnName { get; set; }
        public string CustomerNumber { get; set; }
        public string Description { get; set; }
        public int ReportId { get; set; }
        public bool IsSelected { get; set; }
        public int OrderInReport { get; set; }
        public virtual Report Report { get; set; }
    }
}
