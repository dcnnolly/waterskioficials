﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public class History
    {
        public string CustomerNumber { get; set; }
        public int Id { get; set; }
        public int ContactId { get; set; }
        public string CreatorName { get; set; }
        public string ContactName { get; set; }
        public DateTime DateCreated { get; set; }
        public int HistorySubTypeId { get; set; }
        public string Detail { get; set; }
        public HistorySubType HistorySubType { get; set; }
    }
}
