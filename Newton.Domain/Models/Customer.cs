﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public class Customer
    {
        public string CustomerNumber { get; set; }
        public string Name { get; set; }
    }
}
