﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public class AvailableReportColumn
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string ColumnName { get; set; }
        public string Description { get; set; }
        public bool IncludeByDefault { get; set; }
        public ReportType ReportType { get; set; }
    }
}
