﻿using System.Collections.Generic;

namespace Newton.Domain.Models
{
    public class CustomerInfo
    {
        public int Id { get; set; }
        public string CustomerNumber { get; set; }
        public string AppleResellerId { get; set; }
        public string AppleDEPId { get; set; }
        public string MicrosoftMPNId { get; set; }
        public string Parent { get; set; }
    }
}