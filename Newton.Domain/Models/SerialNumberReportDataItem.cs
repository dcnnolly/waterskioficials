﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public class SerialNumberReportDataItem : BaseReportDataItem
    {
        public string REPORTSTATUS { get; set; }
        public DateTime DATEADDED { get; set; }
        public int DNOTENUM { get; set; }
        public DateTime CREATDAT { get; set; }
        public string INVNUMBE { get; set; }
        public string SOPNUMBE { get; set; }
        public string CSTPONBR { get; set; }
        public string EUSERPONUM { get; set; }
        public string SHIPADD1 { get; set; }
        public string ITEMNMBR { get; set; }
        public string ITEMDESC { get; set; }
        public string VENDOR { get; set; }
        public int QUANTITY { get; set; }
        public string SERIALNUM { get; set; }
    }
}
