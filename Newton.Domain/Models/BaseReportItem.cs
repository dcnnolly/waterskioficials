﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public abstract class BaseReportDataItem
    {
        public Guid Id { get; set; }
        public string CUSTNMBR { get; set; }
        public DateTime DATEREPORTED { get; set; }
        public string PARENT { get; set; }
    }
}
