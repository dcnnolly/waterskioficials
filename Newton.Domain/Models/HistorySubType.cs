﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public class HistorySubType
    {
        public HistorySubType()
        {
            HistoryItems = new List<History>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int HistoryTypeId { get; set; }
        public virtual HistoryType HistoryType { get; set; }
        public virtual ICollection<History> HistoryItems { get; set; }
    }
}
