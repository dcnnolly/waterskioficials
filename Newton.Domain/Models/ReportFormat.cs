﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public enum ReportFormat
    {
        [Display(Name = "text")]
        text = 1,
        [Display(Name = "csv")]
        csv = 2
    }
}
