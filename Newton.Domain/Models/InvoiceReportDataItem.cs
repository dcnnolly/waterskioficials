﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Domain.Models
{
    public class InvoiceReportDataItem : BaseReportDataItem
    {
        public string REPORTSTATUS { get; set; }
        public DateTime DATEADDED { get; set; }
        public string DOCTYPE { get; set; }
        public string INVNUMBE { get; set; }
        public int LNITMSEQ { get; set; }
        public string ORIGNUMB { get; set; }
        public string MWHBASEID { get; set; }
        public DateTime DOCDATE { get; set; }
        public string CUSTNAME { get; set; }
        public string CSTPONBR { get; set; }
        public string EUSERPONUM { get; set; }
        public string CURNCYID { get; set; }
        public Decimal EXCHRATE { get; set; }
        public string SLPRSNID { get; set; }
        public string ITEMNMBR { get; set; }
        public string ITEMDESC { get; set; }
        public string ITMSHNAM { get; set; }
        public string CATEGORY { get; set; }
        public string SUBCATEGORY { get; set; }
        public int QUANTITY { get; set; }
        public decimal UNITPRICE { get; set; }
        public decimal O_UNITPRICE { get; set; }
        public decimal EXTDPRICE { get; set; }
        public decimal O_EXTDPRICE { get; set; }
    }
}
