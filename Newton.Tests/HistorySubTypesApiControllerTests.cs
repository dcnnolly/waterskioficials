﻿using System;
using NUnit.Framework;
using Newton.Web.Controllers;
using Newton.DAL;
using AutoMapper;
using Newton.Web.DTO;
using System.Web.Http.Results;
using System.Collections.Generic;
using Moq;
using Newton.Domain.Models;
using Newton.Web;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
        
namespace Newton.Tests
{
    [TestFixture]
    class HistorySubTypesApiControllerTests
    {
        private WaterSkiContext _context;
        private DbContextTransaction _transaction;
        private IMapper _mapper;
        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            System.Data.Entity.Database.SetInitializer(new NewtonInitializer());
            _context = new WaterSkiContext();

            var mapperMock = new Mock<IMapper>();

            mapperMock.Setup(m => m.Map<List<HistorySubType>, List<HistorySubTypeDTO>>(It.IsAny<List<HistorySubType>>()))
               .Returns(new Func<List<HistorySubType>, List<HistorySubTypeDTO>>(historySubList => DataHelper.GetDTViewModels2(historySubList)));

            _mapper = mapperMock.Object;
        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            _context.Database.Delete();
            _context.Dispose();
        }

        [SetUp]
        public void Init()
        {
            _transaction = _context.Database.BeginTransaction();

        }

        [TearDown]
        public void Cleanup()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }

        [Test]
       public void Sould_check_if_gets_all_history_subtypes_correctly()
        {
            List<HistorySubType> historySubTypes = _context.HistorySubTypes.Include("HistoryType").ToList();
            var expectedResult = _mapper.Map<List<HistorySubType>, List<HistorySubTypeDTO>>(historySubTypes);

            var historySubTypeApiController = new HistorySubTypesApiController(_context, _mapper);
            var result = historySubTypeApiController.Get();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_check_if_gets_history_subtype_correctly()
        {
            var expectedResult = _context.HistorySubTypes.Find(1);

            var historySubTypeApiController = new HistorySubTypesApiController(_context, _mapper);
            var result = historySubTypeApiController.Get(1) as OkNegotiatedContentResult<HistorySubType>;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Content, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Should_check_if_get_subtype_returns_notfound()
        {
            var historySubTypeApiController = new HistorySubTypesApiController(_context, _mapper);
            var result = historySubTypeApiController.Get(0) as NotFoundResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_deletes_history_subtype_correctly()
        {
            var historySubTypeApiController = new HistorySubTypesApiController(_context, _mapper);
            var result = historySubTypeApiController.Delete(2);

            var expectedResultNull = _context.HistorySubTypes.Find(2);

            Assert.That(result, Is.Not.Null);
            Assert.That(expectedResultNull, Is.Null);
        }

        [Test]
        public void Should_check_if_delete_history_subtype_returns_notfound()
        {
            var historySubTypeApiController = new HistorySubTypesApiController(_context, _mapper);
            var result = historySubTypeApiController.Delete(0) as NotFoundResult;

            Assert.That(result, Is.Not.Null);
        }
    }
}
