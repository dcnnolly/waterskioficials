﻿using System;
using NUnit.Framework;
using Newton.Web.Controllers;
using Newton.DAL;
using AutoMapper;
using Newton.Web.DTO;
using System.Web.Http.Results;
using System.Collections.Generic;
using Moq;
using Newton.Domain.Models;
using Newton.Web;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using System.Diagnostics;
using System.Web.Mvc;
using Newton.Web.ViewModels;
using System.Threading.Tasks;

namespace Newton.Tests
{
    [TestFixture]
    public class RolesControllerTests
    {
        private IMapper _mapper1;
        private IMapper _mapper2;
        private WaterSkiContext _context;
        private DbContextTransaction _transaction;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            System.Data.Entity.Database.SetInitializer(new NewtonInitializer());
            _context = new WaterSkiContext();

            var mapperMock = new Mock<IMapper>();

            mapperMock.Setup(m => m.Map<Role, RoleViewModel>(It.IsAny<Role>()))
               .Returns(new Func<Role, RoleViewModel>(viewmodels => DataHelper.GetDTViewModels(viewmodels)));

            _mapper1 = mapperMock.Object;

            mapperMock.Setup(m => m.Map<RoleViewModel, Role>(It.IsAny<RoleViewModel>()))
               .Returns(new Func<RoleViewModel, Role>(viewmodels => DataHelper.GetDTViewModels(viewmodels)));

            _mapper2 = mapperMock.Object;
        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            _context.Database.Delete();
            _context.Dispose();
        }

        [SetUp]
        public void Init()
        {
            _transaction = _context.Database.BeginTransaction();
        }

        [TearDown]
        public void Cleanup()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }

        [Test]
        public void Should_check_if_returns_role_index_view_correctly()
        {
            var roleController = new RolesController(_mapper1, _context);
            var result = roleController.Index() as ViewResult;

            Assert.That(result, Is.Not.Null);
            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
        }

        [Test]
        public void Should_check_if_returns_role_create_initialize_view_correctly()
        {
            var roleController = new RolesController(_mapper1, _context);
            var result = roleController.Create() as ViewResult;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Model, Is.Not.Null);
            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
            Assert.That(result.Model.GetType(), Is.EqualTo(typeof(RoleViewModel)));
        }

        [Test]
        public void Should_check_if_create_role_returns_to_index_if_value_is_null()
        {
            var roleController = new RolesController(_mapper1, _context);
            RoleViewModel nullItem = null;
            var result = roleController.Create(nullItem);

            Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));

        }

        //[Test]
        //public async Task Should_check_if_finishes_role_create_and_redirects_correctly()
        //{
        //    var roleController = new RolesController(_mapper1, _context);

        //    RoleViewModel testItem = new RoleViewModel
        //    {
        //        Name = "testItem"
        //    };
        //    var result = roleController.Create(testItem);
        //    await Task.Delay(10);

        //    var expectedResult = _context.Roles.Where(x => x.Name == "testItem").First();

        //    Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));
        //    Assert.That(expectedResult, Is.Not.Null);
        //    Assert.That(expectedResult.Name, Is.EqualTo("testItem"));
        //}

        [Test]
        public void Should_check_if_edit_role_redirects_to_index_correctly_if_invalid_id()
        {
            var roleController = new RolesController(_mapper2, _context);
            var result = roleController.Edit(0) as System.Web.Mvc.RedirectToRouteResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_edit_role_redirects_to_index_correctly_if_notfound()
        {
            var roleController = new RolesController(_mapper2, _context);
            var result = roleController.Edit(9999) as System.Web.Mvc.RedirectToRouteResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_edit_role_initializes_edit_correctly()
        {
            var roleController = new RolesController(_mapper2, _context);
            var result = roleController.Edit(1) as ViewResult;
            var resultRole = result.Model as RoleViewModel;
            var expectedResult = _mapper2.Map<Role, RoleViewModel>(_context.Roles.Find(1));

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Model, Is.Not.Null);
            Assert.That(resultRole.Id, Is.EqualTo(expectedResult.Id));
        }

        [Test]
        public void Should_check_if_edit_role_returns_to_index_if_value_is_null()
        {
            var roleController = new RolesController(_mapper1, _context);
            RoleViewModel nullItem = null;
            var result = roleController.Edit(nullItem);

            Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));
        }

        //[Test]
        //public async Task Should_check_if_edits_role_and_redirects_correctly()
        //{
        //    var roleController = new RolesController(_mapper2, _context);
        //    RoleViewModel testItem = new RoleViewModel
        //    {
        //        Name = "testItem",
        //        Id = 2
        //    };
        //    var result = roleController.Edit(testItem);
        //    await Task.Delay(10);

        //    var expectedResult = _context.Roles.Find(2);

        //    Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));
        //    Assert.That(expectedResult, Is.Not.Null);
        //    Assert.That(expectedResult.Name, Is.EqualTo("testItem"));
        //}

        [Test]
        public void Should_check_if_delete_role_redirects_to_index_correctly_if_invalid_id()
        {
            var roleController = new RolesController(_mapper2, _context);
            var result = roleController.Delete(0) as System.Web.Mvc.RedirectToRouteResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_delete_historytype_works_correctly()
        {
            var roleController = new RolesController(_mapper2, _context);
            var result = roleController.Delete(4) as System.Web.Mvc.RedirectToRouteResult;

            var expectedNull = _context.Roles.Find(4);

            Assert.That(result, Is.Not.Null);
            Assert.That(expectedNull, Is.Null);
        }
    }
}