﻿using System;
using NUnit.Framework;
using Newton.Web.Controllers;
using Newton.DAL;
using AutoMapper;
using Newton.Web.DTO;
using System.Web.Http.Results;
using System.Collections.Generic;
using Moq;
using Newton.Domain.Models;
using Newton.Web;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using System.Diagnostics;
using System.Web.Mvc;
using Newton.Web.ViewModels;

namespace Newton.Tests
{
    [TestFixture]
    public class ContactsControllerTests
    {
        private IMapper _mapper;
        private WaterSkiContext _context;
        private DbContextTransaction _transaction;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            System.Data.Entity.Database.SetInitializer(new NewtonInitializer());
            _context = new WaterSkiContext();

            var mapperMock = new Mock<IMapper>();

            mapperMock.Setup(m => m.Map<CreateContactViewModel, Contact>(It.IsAny<CreateContactViewModel>()))
               .Returns(new Func<CreateContactViewModel, Contact>(viewmodels => DataHelper.GetDTViewModels(viewmodels)));

            _mapper = mapperMock.Object;
        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            _context.Database.Delete();
            _context.Dispose();
        }

        [SetUp]
        public void Init()
        {
            _transaction = _context.Database.BeginTransaction();
        }

        [TearDown]
        public void Cleanup()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }

        [Test]
        public void Should_check_if_returns_contact_index_view_correctly()
        {
            var contactsController = new ContactsController(_context, _mapper);
            var result = contactsController.Index() as ViewResult;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Model, Is.Null);
            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Index");
        }

        [Test]
        public void Should_check_if_returns_customer_details_view_correctly()
        {
            var contactsController = new ContactsController(_context, _mapper);
            var result = contactsController.Details("LASERELEC      ") as ViewResult;
            var expectedResult = _context.Customers.Find("LASERELEC      ");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Model, Is.Not.Null);
            Assert.That(result.Model, Is.EqualTo(expectedResult));
            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Details");
        }

        [Test]
        public void Should_check_if_customer_details_returns_badrequest()
        {
            var contactsController = new ContactsController(_context, _mapper);
            var result = contactsController.Details("") as HttpStatusCodeResult;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.StatusCode, Is.EqualTo(400));
        }

        [Test]
        public void Should_check_if_customer_details_returns_notfound()
        {
            var contactsController = new ContactsController(_context, _mapper);
            var result = contactsController.Details("Not Existant") as HttpNotFoundResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_initiates_creating_a_contact_correctly()
        {
            var contactsController = new ContactsController(_context, _mapper);
            var result = contactsController.Create() as ViewResult;
            var expectedResult = _context.Customers.ToList();
            var resultModel = result.Model as CreateContactViewModel;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Model, Is.Not.Null);
            Assert.That(resultModel.Customers.Count, Is.EqualTo(expectedResult.Count));
            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Create");
        }

        [Test]
        public void Should_check_if_finishes_creating_a_system_contact_correctly()
        {
            var testItem = new CreateContactViewModel()
            {
                IsSystemContact = true,
                FirstName = "testName",
                Email = "test@test.test",
                CustomerNumber = "LASERELEC      "
            };

            var contactsController = new ContactsController(_context, _mapper);
            var result = contactsController.Create(testItem) as System.Web.Mvc.RedirectToRouteResult;
            var actualContact = _context.Contacts.Where(x => x.FirstName == testItem.FirstName).First();

            Assert.That(result, Is.Not.Null);
            Assert.That(actualContact, Is.Not.Null);
            Assert.That(actualContact.Email, Is.EqualTo(testItem.Email));
            Assert.That(actualContact.IsSystemContact);
            Assert.That(result.RouteValues.Values.First(), Is.EqualTo("Index"));
        }

        [Test]
        public void Should_check_if_finishes_creating_a_not_system_contact_correctly()
        {
            var testItem = new CreateContactViewModel()
            {
                IsSystemContact = false,
                FirstName = "testName",
                Email = "test@test.test",
                CustomerNumber = "LASERELEC      "
            };

            var contactsController = new ContactsController(_context, _mapper);
            var result = contactsController.Create(testItem) as System.Web.Mvc.RedirectToRouteResult;
            var actualContact = _context.Contacts.Where(x => x.FirstName == testItem.FirstName).First();

            Assert.That(result, Is.Not.Null);
            Assert.That(actualContact, Is.Not.Null);
            Assert.That(actualContact.Email, Is.EqualTo(testItem.Email));
            Assert.That(!actualContact.IsSystemContact);
            Assert.That(result.RouteValues.Values.First(), Is.EqualTo("Index"));

        }

    }
}