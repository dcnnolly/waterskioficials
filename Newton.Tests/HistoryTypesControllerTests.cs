﻿using System;
using NUnit.Framework;
using Newton.Web.Controllers;
using Newton.DAL;
using AutoMapper;
using Newton.Web.DTO;
using System.Web.Http.Results;
using System.Collections.Generic;
using Moq;
using Newton.Domain.Models;
using Newton.Web;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using System.Diagnostics;
using System.Web.Mvc;
using Newton.Web.ViewModels;
using System.Threading.Tasks;

namespace Newton.Tests
{
    [TestFixture]
    public class HistoryTypesControllerTests
    {
        private IMapper _mapper1;
        private IMapper _mapper2;
        private WaterSkiContext _context;
        private DbContextTransaction _transaction;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            System.Data.Entity.Database.SetInitializer(new NewtonInitializer());
            _context = new WaterSkiContext();

            var mapperMock = new Mock<IMapper>();

            mapperMock.Setup(m => m.Map<HistoryTypeViewModel, HistoryType>(It.IsAny<HistoryTypeViewModel>()))
               .Returns(new Func<HistoryTypeViewModel, HistoryType>(viewmodels => DataHelper.GetDTViewModels(viewmodels)));

            _mapper1 = mapperMock.Object;

            mapperMock.Setup(m => m.Map < HistoryType, HistoryTypeViewModel > (It.IsAny<HistoryType>()))
               .Returns(new Func<HistoryType, HistoryTypeViewModel>(viewmodels => DataHelper.GetDTViewModels(viewmodels)));

            _mapper2 = mapperMock.Object;
        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            _context.Database.Delete();
            _context.Dispose();
        }

        [SetUp]
        public void Init()
        {
            _transaction = _context.Database.BeginTransaction();
        }

        [TearDown]
        public void Cleanup()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }

        [Test]
        public void Should_check_if_returns_historytype_index_view_correctly()
        {
            var historyTypesController = new HistoryTypesController(_mapper1, _context);
            var result = historyTypesController.Index() as ViewResult;

            Assert.That(result, Is.Not.Null);
            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Index");
        }

        [Test]
        public void Should_check_if_returns_hisotrytype_create_initialize_view_correctly()
        {
            var historyTypesController = new HistoryTypesController(_mapper1, _context);
            var result = historyTypesController.Create() as ViewResult;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Model, Is.Not.Null);
            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName));
            Assert.That(result.Model.GetType(), Is.EqualTo(typeof(HistoryTypeViewModel)));
        }

        [Test]
        public void Should_check_if_create_historytype_returns_to_index_if_value_is_null()
        {
            var historyTypesController = new HistoryTypesController(_mapper1, _context);
            HistoryTypeViewModel nullItem = null;
            var result = historyTypesController.Create(nullItem);

            Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));

        }

        //[Test]
        //public async Task Should_check_if_finishes_historyType_create_and_redirects_correctly()
        //{
        //    var historyTypesController = new HistoryTypesController(_mapper1, _context);

        //    HistoryTypeViewModel testItem = new HistoryTypeViewModel
        //    {
        //        Name = "testItem"
        //    };
        //    var result = historyTypesController.Create(testItem);
        //    await Task.Delay(10);

        //    var expectedResult = _context.HistoryTypes.Where(x => x.Name == "testItem").First();

        //    Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));
        //    Assert.That(expectedResult, Is.Not.Null);
        //    Assert.That(expectedResult.Name, Is.EqualTo("testItem"));
        //}

        [Test]
        public void Should_check_if_edit_historytype_redirects_to_index_correctly_if_invalid_id()
        {
            var historyTypesController = new HistoryTypesController(_mapper2, _context);
            var result = historyTypesController.Edit(0) as System.Web.Mvc.RedirectToRouteResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_edit_historytype_redirects_to_index_correctly_if_notfound()
        {
            var historyTypesController = new HistoryTypesController(_mapper2, _context);
            var result = historyTypesController.Edit(9999) as System.Web.Mvc.RedirectToRouteResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_edit_historytype_initializes_edit_correctly()
        {
            var historyTypesController = new HistoryTypesController(_mapper2, _context);
            var result = historyTypesController.Edit(1) as ViewResult;
            var resultHistorySubType = result.Model as HistoryTypeViewModel;
            var expectedResult = _mapper2.Map<HistoryType, HistoryTypeViewModel>(_context.HistoryTypes.Find(1));

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Model, Is.Not.Null);
            Assert.That(resultHistorySubType.Id, Is.EqualTo(expectedResult.Id));
        }

        [Test]
        public void Should_check_if_edit_historytype_returns_to_index_if_value_is_null()
        {
            var historyTypesController = new HistoryTypesController(_mapper1, _context);
            HistoryTypeViewModel nullItem = null;
            var result = historyTypesController.Edit(nullItem);

            Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));
        }

        //[Test]
        //public async Task Should_check_if_edits_hisotryType_and_redirects_correctly()
        //{
        //    var historyTypesController = new HistoryTypesController(_mapper1, _context);
        //    HistoryTypeViewModel testItem = new HistoryTypeViewModel
        //    {
        //        Name = "testItem",
        //        Id = 2
        //    };
        //    var result = historyTypesController.Edit(testItem);
        //    await Task.Delay(10);

        //    var expectedResult = _context.HistoryTypes.Find(2);

        //    Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));
        //    Assert.That(expectedResult, Is.Not.Null);
        //    Assert.That(expectedResult.Name, Is.EqualTo("testItem"));
        //}

        [Test]
        public void Should_check_if_delete_historytype_redirects_to_index_correctly_if_invalid_id()
        {
            var historyTypesController = new HistoryTypesController(_mapper2, _context);
            var result = historyTypesController.Delete(0) as System.Web.Mvc.RedirectToRouteResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_delete_historytype_works_correctly()
        {
            var historyTypesController = new HistoryTypesController(_mapper2, _context);
            var result = historyTypesController.Delete(4) as System.Web.Mvc.RedirectToRouteResult;

            var expectedNull = _context.HistoryTypes.Find(4);

            Assert.That(result, Is.Not.Null);
            Assert.That(expectedNull, Is.Null);
        }
    }
}