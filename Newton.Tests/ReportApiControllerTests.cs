﻿using System;
using NUnit.Framework;
using Newton.Web.Controllers;
using Newton.DAL;
using AutoMapper;
using Newton.Web.DTO;
using System.Web.Http.Results;
using System.Collections.Generic;
using Moq;
using Newton.Domain.Models;
using Newton.Web;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using Newton.Web.Extensions;

namespace Newton.Tests
{
    [TestFixture]
    class ReportApiControllerTests
    {
        private WaterSkiContext _context;
        private DbContextTransaction _transaction;
        private IMapper _mapper1;
        private IMapper _mapper2;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            System.Data.Entity.Database.SetInitializer(new NewtonInitializer());
            _context = new WaterSkiContext();

            var mapperMock1 = new Mock<IMapper>();

            mapperMock1.Setup(m => m.Map<List<ReportColumn>, List<ReportColumnDTO>>(It.IsAny<List<ReportColumn>>()))
               .Returns(new Func<List<ReportColumn>, List<ReportColumnDTO>>(reportList => DataHelper.GetDTViewModels(reportList)));

            _mapper1 = mapperMock1.Object;

            var mapperMock2 = new Mock<IMapper>();

            mapperMock2.Setup(m => m.Map<List<AvailableReportColumn>, List<ReportColumnDTO>>(It.IsAny<List<AvailableReportColumn>>()))
               .Returns(new Func<List<AvailableReportColumn>, List<ReportColumnDTO>>(reportList => DataHelper.GetDTViewModels2(reportList)));

            _mapper2 = mapperMock2.Object;
        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            _context.Database.Delete();
            _context.Dispose();
        }

        [SetUp]
        public void Init()
        {
            _transaction = _context.Database.BeginTransaction();

        }

        [TearDown]
        public void Cleanup()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }

        [Test]
        public void Sould_check_if_gets_all_report_types_correctly()
        {
            var expectedResult = new List<ReportTypeDTO>();
            foreach (var type in Enum.GetValues(typeof(ReportType)).Cast<ReportType>())
            {
                expectedResult.Add(new ReportTypeDTO()
                {
                    Id = (int)type,
                    Name = type.GetDisplayName()
                });
            }

            var reportApiController = new ReportApiController(_context, _mapper1);
            var result = reportApiController.GetAllReportTypes();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_check_if_returns_all_report_columns_correctly()
        {
            var reportColumns = _context.ReportColumns.ToList();

            var expectedResult = _mapper1.Map<List<ReportColumn>, List<ReportColumnDTO>>(reportColumns);

            var reportApiController = new ReportApiController(_context, _mapper1);
            var result = reportApiController.GetAllReportColumns("LASERELEC      ");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_check_if_returns_selected_report_columns_correctly()
        {
            var reportColumns = _context.ReportColumns.ToList();

            var expectedResult = _mapper1.Map<List<ReportColumn>, List<ReportColumnDTO>>(reportColumns);

            var reportApiController = new ReportApiController(_context, _mapper1);
            var result = reportApiController.SelectedReportColumns("LASERELEC      ");

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_check_if_returns_all_report_formats_correctly()
        {
            var expectedResult = new List<ReportFormatDTO>();

            foreach (var format in Enum.GetValues(typeof(ReportFormat)).Cast<ReportFormat>())
            {
                expectedResult.Add(new ReportFormatDTO()
                {
                    Id = (int)format,
                    Name = format.ToString()
                });
            }

            var reportApiController = new ReportApiController(_context, _mapper1);
            var result = reportApiController.GetAllReportFormats();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_check_if_creates_new_report_correctly()
        {
            var selectedTestColumns = new List<ReportColumnDTO>();
            selectedTestColumns.Add(new ReportColumnDTO()
            {
                ColumnName = "ORIGNUMB",
                Name = "MWH Sales Order"
            });

            NewReportItemDTO testItem = new NewReportItemDTO()
            {
                ContactId = 1,
                CustomerNumber = "testCustomer",
                IsSubscribed = true,
                Id = 0,
                IncludeHeaders = false,
                Name = "testSubject",
                ReportType = (ReportType)1,
                ReportFormat = (ReportFormat)1,
                SelectedColumns = selectedTestColumns,
                ReportScheduleFrequency = (ReportScheduleFrequency)2,
                ReportScheduleFrequencyName = "testFrequency",
                ExecutionDay = 4,
                ExecutionTime = 15
            };

            var reportApiController = new ReportApiController(_context, _mapper1);
            var result = reportApiController.SaveReport(testItem) as IHttpActionResult;
            var reportResult = _context.Reports.Where(cr => cr.CustomerNumber == testItem.CustomerNumber).First();

            Assert.That(result, Is.Not.Null);
            Assert.That(reportResult.CustomerNumber == testItem.CustomerNumber);

        }

        [Test]
        public void Should_check_if_updates_report_correctly()
        {
            var selectedTestColumns = new List<ReportColumnDTO>();
            selectedTestColumns.Add(new ReportColumnDTO()
            {
                ColumnName = "ORIGNUMB",
                Name = "MWH Sales Order"
            });

            NewReportItemDTO testItem = new NewReportItemDTO()
            {
                ContactId = 6,
                CustomerNumber = "testCustomer",
                IsSubscribed = true,
                Id = 1,
                IncludeHeaders = false,
                Name = "testSubject",
                ReportType = (ReportType)1,
                ReportFormat = (ReportFormat)1,
                SelectedColumns = selectedTestColumns,
                ReportScheduleFrequency = (ReportScheduleFrequency)2,
                ReportScheduleFrequencyName = "testFrequency",
                ExecutionDay = 4,
                ExecutionTime = 15
            };

            var reportApiController = new ReportApiController(_context, _mapper1);
            var result = reportApiController.SaveReport(testItem) as IHttpActionResult;
            var reportResult = _context.Reports.Where(cr => cr.Id == testItem.Id).First();

            Assert.That(result, Is.Not.Null);
            Assert.That(reportResult.Id == testItem.Id);
            Assert.That(reportResult.CustomerNumber == testItem.CustomerNumber);
        }

        [Test]
        public void Should_check_if_updates_all_schedule_states_correctly()
        {
            var selectedTestColumns = new List<ReportColumnDTO>();
            selectedTestColumns.Add(new ReportColumnDTO()
            {
                ColumnName = "ORIGNUMB",
                Name = "MWH Sales Order"
            });

            NewReportItemDTO testItem = new NewReportItemDTO()
            {
                ContactId = 6,
                CustomerNumber = "testCustomer",
                IsSubscribed = true,
                Id = 1,
                IncludeHeaders = false,
                Name = "testSubject",
                ReportType = (ReportType)1,
                ReportFormat = (ReportFormat)1,
                SelectedColumns = selectedTestColumns,
                ReportScheduleFrequency = (ReportScheduleFrequency)1,
                ReportScheduleFrequencyName = "testFrequency",
                ScheduleStartTime = 8,
                ScheduleEndTime = 15
            };


            //-----------------------------Hourly
            var reportApiController = new ReportApiController(_context, _mapper1);
            var result = reportApiController.SaveReport(testItem) as IHttpActionResult;
            var reportResult = _context.Reports.Where(cr => cr.Id == testItem.Id).First();

            Assert.That(result, Is.Not.Null);
            Assert.That(reportResult.ScheduleStartTime, Is.Not.Null);
            Assert.That(reportResult.ScheduleEndTime, Is.Not.Null);
            Assert.That(reportResult.ReportScheduleFrequency, Is.EqualTo(testItem.ReportScheduleFrequency));

            //----------------------------Daily
            testItem.ReportScheduleFrequency = (ReportScheduleFrequency)2;
            testItem.ExecutionTime = 15;

            result = reportApiController.SaveReport(testItem) as IHttpActionResult;
            reportResult = _context.Reports.Where(cr => cr.Id == testItem.Id).First();

            Assert.That(result, Is.Not.Null);
            Assert.That(reportResult.ExecutionTime, Is.Not.Null);
            Assert.That(reportResult.ReportScheduleFrequency, Is.EqualTo(testItem.ReportScheduleFrequency));

            //----------------------------Weekly
            testItem.ReportScheduleFrequency = (ReportScheduleFrequency)3;
            testItem.ExecutionDayOfWeek = 4;

            result = reportApiController.SaveReport(testItem) as IHttpActionResult;
            reportResult = _context.Reports.Where(cr => cr.Id == testItem.Id).First();

            Assert.That(result, Is.Not.Null);
            Assert.That(reportResult.ExecutionDayOfWeek, Is.Not.Null);
            Assert.That(reportResult.ReportScheduleFrequency, Is.EqualTo(testItem.ReportScheduleFrequency));

            //----------------------------Monthly
            testItem.ReportScheduleFrequency = (ReportScheduleFrequency)4;
            testItem.ExecutionDay = 4;

            result = reportApiController.SaveReport(testItem) as IHttpActionResult;
            reportResult = _context.Reports.Where(cr => cr.Id == testItem.Id).First();

            Assert.That(result, Is.Not.Null);
            Assert.That(reportResult.ExecutionDay, Is.Not.Null);
            Assert.That(reportResult.ReportScheduleFrequency, Is.EqualTo(testItem.ReportScheduleFrequency));
        }

        [Test]
        public void Should_check_if_deletes_report_correctly()
        {
            var reportApiController = new ReportApiController(_context, _mapper1);
            var result = reportApiController.DeleteReport(2) as IHttpActionResult;

            var expectedNull = _context.Reports.Find(2);

            Assert.That(result, Is.Not.Null);
            Assert.That(expectedNull, Is.Null);
        }

        [Test]
        public void Should_check_if_gets_columns_of_a_new_report_correctly()
        {
            var reportApiController = new ReportApiController(_context, _mapper2);
            var result = reportApiController.GetColumns(0, "LASERELEC      ", 1) as OkNegotiatedContentResult<Tuple<List<ReportColumnDTO>, List<ReportColumnDTO>>>;

            List<AvailableReportColumn> allAvailableColumns = new List<AvailableReportColumn>();
            allAvailableColumns = _context.AvailableReportColumns.Where(x => x.ReportType == (ReportType)1).ToList();

            List<AvailableReportColumn> includeByDefaultColumns = allAvailableColumns.Where(x => x.IncludeByDefault).ToList();
            List<AvailableReportColumn> remainingColumns = allAvailableColumns.Except(includeByDefaultColumns).ToList();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Content.Item1.Count, Is.EqualTo(includeByDefaultColumns.Count));
            Assert.That(result.Content.Item2.Count, Is.EqualTo(remainingColumns.Count));
        }

        [Test]
        public void Should_check_if_gets_columns_of_an_existing_report_correctly()
        {
            var reportApiController = new ReportApiController(_context, _mapper1);
            var result = reportApiController.GetColumns(1, "LASERELEC      ", 1) as OkNegotiatedContentResult<Tuple<List<ReportColumnDTO>, List<ReportColumnDTO>>>;

            List<ReportColumn> selectedColumns = new List<ReportColumn>();
            List<ReportColumn> availableColumns = new List<ReportColumn>();

            selectedColumns = _context.ReportColumns.Where(r => r.ReportId == 1 && r.IsSelected == true).OrderBy(x => x.OrderInReport).ToList();
            availableColumns = _context.ReportColumns.Where(r => r.ReportId == 1 && r.IsSelected == false).ToList();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Content.Item1.Count, Is.EqualTo(selectedColumns.Count));
            Assert.That(result.Content.Item2.Count, Is.EqualTo(availableColumns.Count));
        }

        [Test]
        public void Should_check_if_removes_report_subscription_correctly()
        {
            var reportApiController = new ReportApiController(_context, _mapper1);
            var result = reportApiController.SubscribeReport(1, 1) as StatusCodeResult;

            var myReport = _context.Reports.Include("Contacts").Where(x => x.Id == 1).First();
            ReportContact expectedNull = myReport.Contacts.ToList().Find(x => x.ContactId == 1);

            Assert.That(result, Is.Not.Null);
            Assert.That(expectedNull, Is.Null);

        }

        [Test]
        public void Should_check_if_adds_report_subscription_correctly()
        {
            var reportApiController = new ReportApiController(_context, _mapper1);
            var result = reportApiController.SubscribeReport(1, 6) as StatusCodeResult;

            var myReport = _context.Reports.Include("Contacts").Where(x => x.Id == 1).First();
            ReportContact expectedResult = myReport.Contacts.ToList().Find(x => x.ContactId == 6);

            Assert.That(result, Is.Not.Null);
            Assert.That(expectedResult, Is.Not.Null);
            Assert.That(expectedResult.ContactId, Is.EqualTo(6));
        }



    }
}
