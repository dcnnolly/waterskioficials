﻿using System;
using NUnit.Framework;
using Newton.Web.Controllers;
using Newton.DAL;
using AutoMapper;
using Newton.Web.DTO;
using System.Web.Http.Results;
using System.Collections.Generic;
using Moq;
using Newton.Domain.Models;
using Newton.Web;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;

namespace Newton.Tests
{
    [TestFixture]
    class RolesApiControllerTests
    {
        private WaterSkiContext _context;
        private DbContextTransaction _transaction;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            System.Data.Entity.Database.SetInitializer(new NewtonInitializer());
            _context = new WaterSkiContext();

        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            _context.Database.Delete();
            _context.Dispose();
        }

        [SetUp]
        public void Init()
        {
            _transaction = _context.Database.BeginTransaction();

        }

        [TearDown]
        public void Cleanup()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }

        [Test]
        public void Sould_check_if_gets_all_history_subtypes_correctly()
        {
            var expectedResult = _context.Roles.ToList();

            var rolesApiController = new RoleApiController(_context);
            var result = rolesApiController.Get();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_check_if_gets_role_correctly()
        {
            var expectedResult = _context.Roles.Find(1);

            var rolesApiController = new RoleApiController(_context);
            var result = rolesApiController.Get(1) as OkNegotiatedContentResult<Role>;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Content, Is.EqualTo(expectedResult));
        }

        [Test]
        public void Should_check_if_get_role_returns_notfound()
        {
            var rolesApiController = new RoleApiController(_context);
            var result = rolesApiController.Get(0) as NotFoundResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_deletes_role_correctly()
        {
            var rolesApiController = new RoleApiController(_context);
            var result = rolesApiController.Delete(2);

            var expectedResultNull = _context.Roles.Find(2);

            Assert.That(result, Is.Not.Null);
            Assert.That(expectedResultNull, Is.Null);
        }

        [Test]
        public void Should_check_if_delete_role_returns_notfound()
        {
            var rolesApiController = new RoleApiController(_context);
            var result = rolesApiController.Delete(0) as NotFoundResult;

            Assert.That(result, Is.Not.Null);
        }
    }
}
