﻿using System;
using NUnit.Framework;
using Newton.Web.Controllers;
using Newton.DAL;
using AutoMapper;
using Newton.Web.DTO;
using System.Web.Http.Results;
using System.Collections.Generic;
using Moq;
using Newton.Domain.Models;
using Newton.Web;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;

namespace Newton.Tests
{
    [TestFixture]
    class CustomerApiControllerTests
    {
        private WaterSkiContext _context;
        private IMapper _mapper1;
        private IMapper _mapper2;
        private DbContextTransaction _transaction;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            System.Data.Entity.Database.SetInitializer(new NewtonInitializer());
            _context = new WaterSkiContext();

            var mapperMock1 = new Mock<IMapper>();

            mapperMock1.Setup(m => m.Map<CustomerInfo, CustomerInfoDTO>(It.IsAny<CustomerInfo>()))
               .Returns(new Func<CustomerInfo, CustomerInfoDTO>(customerInfoList => DataHelper.GetDTViewModels(customerInfoList)));

            _mapper1 = mapperMock1.Object;

            var mapperMock2 = new Mock<IMapper>();

            mapperMock2.Setup(m => m.Map<Customer, CustomerInfoDTO>(It.IsAny<Customer>()))
               .Returns(new Func<Customer, CustomerInfoDTO>(customerList => DataHelper.GetDTViewModels(customerList)));

            _mapper2 = mapperMock2.Object;
        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            _context.Database.Delete();
            _context.Dispose();
        }
    
        [SetUp]
        public void Init()
        {
            _transaction = _context.Database.BeginTransaction();
        }

        [TearDown]
        public void Cleanup()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }

        [Test]
        public void Should_check_if_gets_customer_with_customerInfo_correctly()
        {
            var contact = _context.Contacts.Find(1);
            var customer = _context.Customers.Find(contact.CustomerNumber);
            var customerApiController = new CustomerApiController(_context, _mapper1); 

            CustomerInfoDTO info = new CustomerInfoDTO();
            CustomerInfo customerInfo = _context.CustomerInfoItems.FirstOrDefault(ci => ci.CustomerNumber == customer.CustomerNumber);
            info = _mapper1.Map<CustomerInfo, CustomerInfoDTO>(customerInfo);

            var result = customerApiController.Get(1) as OkNegotiatedContentResult<CustomerInfoDTO>;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Content.Id, Is.EqualTo(info.Id));
        }

        //[Test]
        //public void Should_check_if_gets_customer_without_customerInfo_correctly()
        //{
        //    var customerApiController = new CustomerApiController(_context, _mapper2);
        //    Customer customer = _context.Customers.Add(new Customer
        //    {
        //        CustomerNumber = "CUSTOMERTEST",
        //        Name = "Test Customer"
        //    });

        //    Contact contact = _context.Contacts.Add(new Contact
        //    {
        //        FirstName = "TestName",
        //        Email = "Test@email.com",
        //        IsSystemContact = false,
        //        AppearInListOrderConfirmations = false,
        //        FirstInListOrderConfirmations = false,
        //        ReceiveAllOrderConfirmations = false,
        //        ReceiveOwnInvoices = false,
        //        ReceiveAllInvoices = false,
        //        ReceiveStatements = false,
        //        ReceiveShipConfirmations = false,
        //        ReceiveAllShipConfirmations = false,
        //        IsDeleted = false,
        //        CustomerNumber = "CUSTOMERTEST"
        //    });

        //    CustomerInfoDTO info = new CustomerInfoDTO();
        //    info = _mapper2.Map<Customer, CustomerInfoDTO>(customer);

        //    var result = customerApiController.Get(contact.Id) as OkNegotiatedContentResult<CustomerInfoDTO>;
        //    Assert.That(result, Is.Not.Null);
        //    Assert.That(result.Content.CustomerNumber, Is.EqualTo(info.CustomerNumber));
        //}


        [Test]
        public void Should_check_if_get_customer_returns_notFound_if_contact_notfound()
        {
            var customerApiController = new CustomerApiController(_context, _mapper1);
            var result = customerApiController.Get(0) as NotFoundResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_get_customer_returns_notFound_if_customer_notfound()
        {
            var customerApiController = new CustomerApiController(_context, _mapper1);
            Contact contact = _context.Contacts.Add(new Contact
            {
                FirstName = "TestName",
                Email = "Test@email.com",
                IsSystemContact = false,
                AppearInListOrderConfirmations = false,
                FirstInListOrderConfirmations = false,
                ReceiveAllOrderConfirmations = false,
                ReceiveOwnInvoices = false,
                ReceiveAllInvoices = false,
                ReceiveStatements = false,
                ReceiveShipConfirmations = false,
                ReceiveAllShipConfirmations = false,
                IsDeleted = false
            });

            var result = customerApiController.Get(contact.Id) as NotFoundResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_gets_customer_number()
        {
            var contact = _context.Contacts.Find(1);
            var customerNumber = contact.CustomerNumber;

            var customerApiController = new CustomerApiController(_context, _mapper1);
            var result = customerApiController.GetNumber(1) as OkNegotiatedContentResult<string>;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Content, Is.EqualTo(customerNumber));
        }

        [Test]
        public void Should_check_if_updates_customer()
        {
            var customer = _context.CustomerInfoItems.Find(1);
            customer.AppleDEPId = "Test";
            var customerApiController = new CustomerApiController(_context, _mapper1);
            customerApiController.UpdateDetails(customer);

            var result = _context.CustomerInfoItems.Find(1);
            Assert.That(result, Is.Not.Null);
            Assert.That(result.AppleDEPId, Is.EqualTo("Test"));
        }

    }
}
