﻿using Newton.Domain.Models;
using Newton.Web.DTO;
using Newton.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.Tests
{
    public class DataHelper
    {
        public static List<ContactRoleDTO> GetDTViewModels(List<Role> tasks)
        {
            var list = new List<ContactRoleDTO>();

            foreach (var item in tasks)
            {
                var viewModel = new ContactRoleDTO()
                {
                    Id = item.Id,
                    Name = item.Name
                };

                list.Add(viewModel);
            }

            return list;
        }

        public static CustomerInfoDTO GetDTViewModels(CustomerInfo tasks)
        {
            var item = new CustomerInfoDTO();

            item.Id = tasks.Id;
            item.CustomerNumber = tasks.CustomerNumber;

            return item;
        }

        public static CustomerInfoDTO GetDTViewModels(Customer tasks)
        {
            var item = new CustomerInfoDTO();

            item.CustomerNumber = tasks.CustomerNumber;
            item.Name = tasks.Name;

            return item;
        }

        public static List<HistoryListDTO> GetDTViewModels(List<History> tasks)
        {
            var list = new List<HistoryListDTO>();

            foreach (var item in tasks)
            {
                var viewModel = new HistoryListDTO()
                {
                    Id = item.Id,
                    CreatorName = item.CreatorName,
                    ContactId = item.ContactId,
                    ContactName = item.ContactName,
                    Detail = item.Detail
                };

                list.Add(viewModel);
            }
            
            return list;
        }

        public static List<HistorySubTypesDTO> GetDTViewModels(List<HistorySubType> tasks)
        {
            var list = new List<HistorySubTypesDTO>();

            foreach (var item in tasks)
            {
                var viewModel = new HistorySubTypesDTO()
                {
                    Id = item.Id,
                    Name = item.Name,
                    TypeId = item.HistoryTypeId,
                    TypeName = "test"
                };

                list.Add(viewModel);
            }

            return list;
        }

        public static History GetDTViewModels(NewHistoryItemDTO tasks)
        {
            var item = new History();

            item.Detail = tasks.Detail;
            item.DateCreated = tasks.DateCreated;
            item.Id = tasks.Id;
            item.ContactId = tasks.ContactId;
            item.ContactName = tasks.ContactName;
            item.CreatorName = tasks.CreatorName;
            item.HistorySubTypeId = tasks.HistorySubTypeId;

            return item;
        }

        public static List<HistorySubTypeDTO> GetDTViewModels2(List<HistorySubType> tasks)
        {
            var list = new List<HistorySubTypeDTO>();

            foreach (var item in tasks)
            {
                var viewModel = new HistorySubTypeDTO()
                {
                    Id = item.Id,
                    Name = item.Name
                };

                list.Add(viewModel);
            }

            return list;
        }
        public static List<ReportColumnDTO> GetDTViewModels(List<ReportColumn> tasks)
        {
            var list = new List<ReportColumnDTO>();

            foreach (var item in tasks)
            {
                var viewModel = new ReportColumnDTO()
                {
                    Name = item.Name,
                    ColumnName = item.ColumnName

                };

                list.Add(viewModel);
            }

            return list;
        }
        public static List<ReportColumnDTO> GetDTViewModels2(List<AvailableReportColumn> tasks)
        {
            var list = new List<ReportColumnDTO>();

            foreach (var item in tasks)
            {
                var viewModel = new ReportColumnDTO()
                {
                    Name = item.Name,
                    ColumnName = item.ColumnName
                };

                list.Add(viewModel);
            }

            return list;
        }

        public static Contact GetDTViewModels(CreateContactViewModel tasks)
        {
            var item = new Contact();

            item.CustomerNumber = tasks.CustomerNumber;
            item.Email = tasks.Email;
            item.FirstName = tasks.FirstName;
            item.IsSystemContact = tasks.IsSystemContact;
            return item;
        }


        public static HistorySubType GetDTViewModels(HistorySubTypeViewModel tasks)
        {
            var item = new HistorySubType();

            item.Name = tasks.Name;
            item.HistoryTypeId = Int32.Parse(tasks.HistoryTypeId);
            item.Id = tasks.Id;
            return item;
        }

        public static HistorySubTypeViewModel GetDTViewModels(HistorySubType tasks)
        {
            var item = new HistorySubTypeViewModel();

            item.Name = tasks.Name;
            item.HistoryTypeId = tasks.HistoryTypeId.ToString();
            return item;
        }

        public static HistoryType GetDTViewModels(HistoryTypeViewModel tasks)
        {
            var item = new HistoryType();
            item.Name = tasks.Name;
            item.Id = tasks.Id;
            return item;
        }
        public static HistoryTypeViewModel GetDTViewModels(HistoryType tasks)
        {
            var item = new HistoryTypeViewModel();
            item.Name = tasks.Name;
            item.Id = tasks.Id;
            return item;
        }

        public static RoleViewModel GetDTViewModels(Role tasks)
        {
            var item = new RoleViewModel();
            item.Name = tasks.Name;
            item.Id = tasks.Id;
            return item;
        }

        public static Role GetDTViewModels(RoleViewModel tasks)
        {
            var item = new Role();
            item.Name = tasks.Name;
            item.Id = tasks.Id;
            return item;
        }
    }
}
