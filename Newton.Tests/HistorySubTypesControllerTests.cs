﻿using System;
using NUnit.Framework;
using Newton.Web.Controllers;
using Newton.DAL;
using AutoMapper;
using Newton.Web.DTO;
using System.Web.Http.Results;
using System.Collections.Generic;
using Moq;
using Newton.Domain.Models;
using Newton.Web;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using System.Diagnostics;
using System.Web.Mvc;
using Newton.Web.ViewModels;
using System.Threading.Tasks;

namespace Newton.Tests
{
    [TestFixture]
    public class HistorySubTypesControllerTests
    {
        private IMapper _mapper1;
        private IMapper _mapper2;
        private WaterSkiContext _context;
        private DbContextTransaction _transaction;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            System.Data.Entity.Database.SetInitializer(new NewtonInitializer());
            _context = new WaterSkiContext();

            var mapperMock = new Mock<IMapper>();

            mapperMock.Setup(m => m.Map<HistorySubTypeViewModel, HistorySubType>(It.IsAny<HistorySubTypeViewModel>()))
               .Returns(new Func<HistorySubTypeViewModel, HistorySubType>(viewmodels => DataHelper.GetDTViewModels(viewmodels)));

            _mapper1 = mapperMock.Object;

            mapperMock.Setup(m => m.Map<HistorySubType, HistorySubTypeViewModel>(It.IsAny<HistorySubType>()))
               .Returns(new Func<HistorySubType, HistorySubTypeViewModel>(viewmodels => DataHelper.GetDTViewModels(viewmodels)));

            _mapper2 = mapperMock.Object;
        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            _context.Database.Delete();
            _context.Dispose();
        }

        [SetUp]
        public void Init()
        {
            _transaction = _context.Database.BeginTransaction();
        }

        [TearDown]
        public void Cleanup()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }

        [Test]
        public void Should_check_if_returns_contact_index_view_correctly()
        {
            var historySubTypesController = new HistorySubTypesController(_mapper1, _context);
            var result = historySubTypesController.Index() as ViewResult;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Model, Is.Null);
            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Index");
        }

        [Test]
        public void Should_check_if_initiates_create_history_sub_type_correctly()
        {
            var historySubTypesController = new HistorySubTypesController(_mapper1, _context);
            var result = historySubTypesController.Create() as ViewResult;
            var expectedResult = _context.HistoryTypes.ToList();
            var resultModel = result.Model as HistorySubTypeViewModel;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Model, Is.Not.Null);
            Assert.That(resultModel.HistoryTypes.Count, Is.EqualTo(expectedResult.Count));
            Assert.IsTrue(string.IsNullOrEmpty(result.ViewName) || result.ViewName == "Create");
        }

        [Test]
        public void Should_check_if_create_returns_to_index_if_value_is_null()
        {
            var historySubTypesController = new HistorySubTypesController(_mapper1, _context);
            HistorySubTypeViewModel nullItem = null;
            var result = historySubTypesController.Create(nullItem);

            Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));

        }

        //[Test]
        //public async Task Should_check_if_creates_historySubType_and_redirects_correctly()
        //{
        //    await Task.Delay(10);
        //    var historySubTypesController = new HistorySubTypesController(_mapper1, _context);
        //    HistorySubTypeViewModel testItem = new HistorySubTypeViewModel
        //    {
        //        Name = "testItem",
        //        HistoryTypeId = "1"
        //    };
        //    var result = historySubTypesController.Create(testItem);
        //    await Task.Delay(10);

        //    var expectedResult = _context.HistorySubTypes.Where(x => x.Name == "testItem").First();

        //    Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));
        //    Assert.That(expectedResult, Is.Not.Null);
        //    Assert.That(expectedResult.Name, Is.EqualTo("testItem"));
        //}

        [Test]
        public void Should_check_if_edit_historysubtype_redirects_to_index_correctly_if_invalid_id()
        {
            var historySubTypesController = new HistorySubTypesController(_mapper2, _context);
            var result = historySubTypesController.Edit(0) as System.Web.Mvc.RedirectToRouteResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_edit_historysubtype_redirects_to_index_correctly_if_notfound()
        {
            var historySubTypesController = new HistorySubTypesController(_mapper2, _context);
            var result = historySubTypesController.Edit(9999) as System.Web.Mvc.RedirectToRouteResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_edit_historysubtype_initializes_edit_correctly()
        {
            var historySubTypesController = new HistorySubTypesController(_mapper2, _context);
            var result = historySubTypesController.Edit(1) as ViewResult;
            var resultHistorySubType = result.Model as HistorySubTypeViewModel;
            var expectedResult = _mapper2.Map<HistorySubType, HistorySubTypeViewModel>(_context.HistorySubTypes.Find(1));

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Model, Is.Not.Null);
            Assert.That(resultHistorySubType.Id, Is.EqualTo(expectedResult.Id));
        }

        [Test]
        public void Should_check_if_edit_returns_to_index_if_value_is_null()
        {
            var historySubTypesController = new HistorySubTypesController(_mapper1, _context);
            HistorySubTypeViewModel nullItem = null;
            var result = historySubTypesController.Edit(nullItem);

            Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));
        }

        //[Test]
        //public async Task Should_check_if_edis_hisotrySubType_and_redirects_correctly()
        //{
        //    await Task.Delay(10);
        //    var historySubTypesController = new HistorySubTypesController(_mapper1, _context);
        //    HistorySubTypeViewModel testItem = new HistorySubTypeViewModel
        //    {
        //        Name = "testItem",
        //        HistoryTypeId = "1",
        //        Id = 2
        //    };
        //    var result = historySubTypesController.Edit(testItem);
        //    await Task.Delay(10);

        //    var expectedResult = _context.HistorySubTypes.Find(2);

        //    Assert.That(result.Result.GetType(), Is.EqualTo(typeof(System.Web.Mvc.RedirectToRouteResult)));
        //    Assert.That(expectedResult, Is.Not.Null);
        //    Assert.That(expectedResult.Name, Is.EqualTo("testItem"));
        //}
    }
}