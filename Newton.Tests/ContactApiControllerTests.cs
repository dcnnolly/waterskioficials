﻿using System;
using NUnit.Framework;
using Newton.Web.Controllers;
using Newton.DAL;
using AutoMapper;
using Newton.Web.DTO;
using System.Web.Http.Results;
using System.Collections.Generic;
using Moq;
using Newton.Domain.Models;
using Newton.Web;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;
using System.Diagnostics;

namespace Newton.Tests
{
    [TestFixture]
    public class ContactApiControllerTests
    {
        private IMapper _mapper;
        private WaterSkiContext _context;
        private DbContextTransaction _transaction;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            System.Data.Entity.Database.SetInitializer(new NewtonInitializer());
            _context = new WaterSkiContext();

            var mapperMock = new Mock<IMapper>();

            mapperMock.Setup(m => m.Map<List<Role>, List<ContactRoleDTO>>(It.IsAny<List<Role>>()))
               .Returns(new Func<List<Role>, List<ContactRoleDTO>>(roleList => DataHelper.GetDTViewModels(roleList)));

            _mapper = mapperMock.Object;
        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            _context.Database.Delete();
            _context.Dispose();
        }

        [SetUp]
        public void Init()
        {
            _transaction = _context.Database.BeginTransaction();

            var roles = from role in _context.Roles
                        select new ContactRoleDTO()
                        {
                            Id = role.Id,
                            Name = role.Name
                        };
        }

        [TearDown]
        public void Cleanup()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }


        [Test]
        public void Should_check_if_returns_contact_roles_correctly()
        {
            var rolesCount = _context.Roles.Count();
            //getting the result
            var contact = _context.Contacts.Find(1);
            _context.Entry(contact).Collection(c => c.Roles).Load();
            var selectedRoles = contact.Roles.Select(x=>x.Id).ToList();
            var selectedRolesCount = selectedRoles.Count();
            List<int> uniqueRolesIds = (from co in _context.Contacts
                                        from r in co.Roles
                                        where co.CustomerNumber == contact.CustomerNumber
                                        where r.IsUnique == true
                                        select r.Id).ToList();
            selectedRoles.AddRange(uniqueRolesIds);
            var remainingRolesCount = rolesCount - selectedRoles.Distinct().Count();

            var contactApiController = new ContactApiController(_context, _mapper);
            var result = contactApiController.GetContactRoles(1) as OkNegotiatedContentResult<Tuple<List<ContactRoleDTO>, List<ContactRoleDTO>>>;

            Assert.That(result, Is.Not.Null);
            Assert.That(selectedRolesCount, Is.EqualTo(result.Content.Item1.Count()));
            Assert.That(remainingRolesCount, Is.EqualTo(result.Content.Item2.Count));
        }

        [Test]
        public void Should_check_if_returns_contact_correctly()
        {
            var contact = _context.Contacts.Find(1);

            var contactApiController = new ContactApiController(_context, _mapper);
            var result = contactApiController.Get(1) as OkNegotiatedContentResult<Contact>;

            Assert.That(result, Is.Not.Null);
            Assert.That(contact, Is.EqualTo(result.Content));
        }

        [Test]
        public void Should_check_if_get_contact_returns_notfound()
        {
            var contactApiController = new ContactApiController(_context, _mapper);
            var result = contactApiController.Get(0) as NotFoundResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_updates_contact_role_correctly()
        {
            var contact = _context.Contacts.Find(1);
            _context.Entry(contact).Collection(c => c.Roles).Load();
            var role = _context.Roles.Find(1);
            var contactApiController = new ContactApiController(_context, _mapper);
            var contactRole = contact.Roles.FirstOrDefault(r => r.Id == role.Id);

            contactApiController.UpdateContactRole(1, role.Id);
            _context.Entry(contact).Collection(c => c.Roles).Load();
            var resultRole = contact.Roles.FirstOrDefault(r => r.Id == role.Id);

            Assert.That(contactRole, Is.Not.EqualTo(resultRole));

            contactApiController.UpdateContactRole(1, role.Id);
            _context.Entry(contact).Collection(c => c.Roles).Load();
            resultRole = contact.Roles.FirstOrDefault(r => r.Id == role.Id);

            Assert.That(contactRole, Is.EqualTo(resultRole));

        }

        [Test]
        public void Should_check_if_update_contact_role_returns_notfound()
        {
            var contactApiController = new ContactApiController(_context, _mapper);
            var result = contactApiController.UpdateContactRole(0,0) as NotFoundResult;

            Assert.That(result, Is.Not.Null);
        }


        [Test]
        public void Should_check_if_updates_contact_correctly()
        {
            var contact = _context.Contacts.Find(1);

            contact.FirstName = "Test";
            contact.LastName = "Test";

            var contactApiController = new ContactApiController(_context, _mapper);
            contactApiController.UpdateDetails(1,contact);

            var result = _context.Contacts.Find(1);

            Assert.That(result, Is.Not.Null);
            Assert.That(result.FirstName, Is.EqualTo("Test"));
            Assert.That(result.LastName, Is.EqualTo("Test"));
        }

        [Test]
        public void Should_check_if_update_contact_returns_notfound()
        {
            var contact = _context.Contacts.Find(1);

            var contactApiController = new ContactApiController(_context, _mapper);
            var result = contactApiController.UpdateDetails(0, contact) as NotFoundResult;

            Assert.That(result, Is.Not.Null);
        }

        [Test]
        public void Should_check_if_gets_remaining_roles_correctly()
        {
            var roles = _context.Roles.ToList();
            var CustomerId = "LASERELEC      ";
            var contacts = _context.Contacts.Where(x => x.CustomerNumber == CustomerId).ToList();

            foreach (Contact contact in contacts)
            {
                _context.Entry(contact).Collection(c => c.Roles).Load();
                foreach (Role role in contact.Roles)
                    if (role.IsUnique)
                        roles.Remove(role);
            }

            var contactApiController = new ContactApiController(_context, _mapper);
            var result = contactApiController.GetRemainingRoles(CustomerId) as OkNegotiatedContentResult<List<Role>>;

            Assert.That(roles.Count, Is.EqualTo(result.Content.Count));
        }

        [Test]
        public void Should_check_if_gets_all_contact_roles_correctly()
        {
            var contactApiController = new ContactApiController(_context, _mapper);
            var result = contactApiController.GetAllRoles() as OkNegotiatedContentResult<List<ContactRoleDTO>>;

            var expectedCount = _context.Roles.OrderBy(x => x.Name).ToList().Count;

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Content.Count, Is.EqualTo(expectedCount));

        }

        [Test]
        public void Should_check_if_get_customer_reports_returns_correctly()
        {
            var contactApiController = new ContactApiController(_context, _mapper);
            var result = contactApiController.GetCustomerReports(1) as OkNegotiatedContentResult<IOrderedEnumerable<NewReportItemDTO>>;

            var expectedCount = _context.Reports.Where(r => r.CustomerNumber == "LASERELEC      ").ToList().Count;


            Assert.That(result, Is.Not.Null);
            Assert.That(result.Content.Count, Is.EqualTo(expectedCount));
        }

        [Test]
        public void Should_return_correct_count_of_contacts_in_advanced_search1()
        {
            var contactApiController = new ContactApiController(_context, _mapper);

            //---------------------------------------------------------TEST WITH SEARCHTEXT AND NO FILTERS
            var expectedResult = _context.Database.SqlQuery<Contact>(
                "SELECT * FROM Contacts " +
                "INNER JOIN Customers ON Contacts.CustomerNumber=Customers.CustomerNumber " +
                "WHERE Contacts.FirstName LIKE '%lis%' " +
                "OR Contacts.LastName LIKE '%lis%' " +
                "OR Contacts.CustomerNumber LIKE '%lis%' " +
                "OR Contacts.Email LIKE '%lis%' " +
                "OR Customers.Name LIKE '%lis%' " +
                "AND Exists(SELECT TOP 1 CustomerNumber FROM Customers WHERE Customers.CustomerNumber=Contacts.CustomerNumber)"
            ).ToList();

            var result = contactApiController.AdvancedSearch(new AdvancedSearchDTO()
            {
                AppearInListOrderConfirmations = false,
                FirstInListOrderConfirmations = false,
                OptOutMarketing = false,
                ReceiveAllInvoices = false,
                ReceiveAllOrderConfirmations = false,
                ReceiveAllShipConfirmations = false,
                ReceiveNewsletters = false,
                ReceiveOwnInvoices = false,
                ReceiveShipConfirmations = false,
                ReceiveSpecials = false,
                ReceiveStatements = false,
                ReceiveTraining = false,
                roleIds = new int[] { },
                UsedNotifications = new List<string>(),
                searchText = "lis"
            });
            Assert.That(result.Count(), Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_return_correct_count_of_contacts_in_advanced_search2()
        {

            //---------------------------------------------------------TEST WITH NO SEARCHTEXT AND ROLE FILTERS
            var contactApiController = new ContactApiController(_context, _mapper);
            var expectedResult = _context.Database.SqlQuery<int>(
                "SELECT Contacts.Id FROM Contacts " +
                "INNER JOIN ContactRoles ON Contacts.Id=ContactRoles.ContactId " +
                "INNER JOIN Roles ON ContactRoles.RoleId=Roles.Id " +
                "WHERE Roles.Id=2 OR Roles.Id=3 " +
                "AND Exists(SELECT TOP 1 CustomerNumber FROM Customers WHERE Customers.CustomerNumber=Contacts.CustomerNumber) " +
                "GROUP BY Contacts.Id"
            ).ToList();

            var result = contactApiController.AdvancedSearch(new AdvancedSearchDTO()
            {
                AppearInListOrderConfirmations = false,
                FirstInListOrderConfirmations = false,
                OptOutMarketing = false,
                ReceiveAllInvoices = false,
                ReceiveAllOrderConfirmations = false,
                ReceiveAllShipConfirmations = false,
                ReceiveNewsletters = false,
                ReceiveOwnInvoices = false,
                ReceiveShipConfirmations = false,
                ReceiveSpecials = false,
                ReceiveStatements = false,
                ReceiveTraining = false,
                roleIds = new int[] { 2, 3 },
                UsedNotifications = new List<string>(),
                searchText = ""
            });
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_return_correct_count_of_contacts_in_advanced_search3()
        {
            //---------------------------------------------------------TEST WITH NO SEARCHTEXT AND NOTIFICATION FILTER
            var contactApiController = new ContactApiController(_context, _mapper);
            var expectedResult = _context.Database.SqlQuery<Contact>(
                "SELECT * FROM Contacts " +
                "WHERE Contacts.AppearInListOrderConfirmations=1 " +
                "OR Contacts.ReceiveAllOrderConfirmations=1 " +
                "OR ReceiveSpecials=1 " +
                "AND Exists(SELECT TOP 1 CustomerNumber FROM Customers WHERE Customers.CustomerNumber=Contacts.CustomerNumber) "
            ).ToList();

            var result = contactApiController.AdvancedSearch(new AdvancedSearchDTO()
            {
                AppearInListOrderConfirmations = true,
                FirstInListOrderConfirmations = false,
                OptOutMarketing = false,
                ReceiveAllInvoices = false,
                ReceiveAllOrderConfirmations = true,
                ReceiveAllShipConfirmations = false,
                ReceiveNewsletters = false,
                ReceiveOwnInvoices = false,
                ReceiveShipConfirmations = false,
                ReceiveSpecials = true,
                ReceiveStatements = false,
                ReceiveTraining = false,
                roleIds = new int[] { },
                UsedNotifications = new List<string>() { "AppearInListOrderConfirmations", "ReceiveAllOrderConfirmations", "ReceiveSpecials" },
                searchText = ""
            });
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_return_correct_count_of_contacts_in_advanced_search4()
        {
            //---------------------------------------------------------TEST WITH SEARCHTEXT AND ROLE FILTER
            var contactApiController = new ContactApiController(_context, _mapper);
            var expectedResult = _context.Database.SqlQuery<int>(
                "SELECT Contacts.Id FROM Contacts " +
                "INNER JOIN ContactRoles ON Contacts.Id=ContactRoles.ContactId " +
                "INNER JOIN Roles ON ContactRoles.RoleId=Roles.Id " +
                "INNER JOIN Customers ON Contacts.CustomerNumber=Customers.CustomerNumber " +
                "WHERE (Roles.Id=2 " +
                    "OR Roles.Id=3) " +
                "AND (Contacts.FirstName LIKE '%laser%' " +
                    "OR Contacts.LastName LIKE '%laser%' " +
                    "OR Contacts.CustomerNumber LIKE '%laser%' " +
                    "OR Contacts.Email LIKE '%laser%' " +
                    "OR Customers.Name LIKE '%laser%') " +
                "AND Exists(SELECT TOP 1 CustomerNumber FROM Customers WHERE Customers.CustomerNumber=Contacts.CustomerNumber) " +
                "GROUP BY Contacts.Id"
            ).ToList();

            var result = contactApiController.AdvancedSearch(new AdvancedSearchDTO()
            {
                AppearInListOrderConfirmations = false,
                FirstInListOrderConfirmations = false,
                OptOutMarketing = false,
                ReceiveAllInvoices = false,
                ReceiveAllOrderConfirmations = false,
                ReceiveAllShipConfirmations = false,
                ReceiveNewsletters = false,
                ReceiveOwnInvoices = false,
                ReceiveShipConfirmations = false,
                ReceiveSpecials = false,
                ReceiveStatements = false,
                ReceiveTraining = false,
                roleIds = new int[] { 2, 3 },
                UsedNotifications = new List<string>() { },
                searchText = "laser"
            });
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_return_correct_count_of_contacts_in_advanced_search5()
        {

            //---------------------------------------------------------TEST WITH SEARCHTEXT AND NOTIFICATION FILTER
            var contactApiController = new ContactApiController(_context, _mapper);
            var expectedResult = _context.Database.SqlQuery<int>(
                "SELECT Contacts.Id FROM Contacts " +
                "INNER JOIN Customers ON Contacts.CustomerNumber=Customers.CustomerNumber " +
                "WHERE (Contacts.AppearInListOrderConfirmations=1 " +
                    "OR Contacts.ReceiveAllOrderConfirmations=1 " +
                    "OR ReceiveSpecials=1) " +
                "AND (Contacts.FirstName LIKE '%laser%' " +
                    "OR Contacts.LastName LIKE '%laser%' " +
                    "OR Contacts.CustomerNumber LIKE '%laser%' " +
                    "OR Contacts.Email LIKE '%laser%' " +
                    "OR Customers.Name LIKE '%laser%') " +
                "AND Exists(SELECT TOP 1 CustomerNumber FROM Customers WHERE Customers.CustomerNumber=Contacts.CustomerNumber) " +
                "GROUP BY Contacts.Id"
            ).ToList();

            var result = contactApiController.AdvancedSearch(new AdvancedSearchDTO()
            {
                AppearInListOrderConfirmations = true,
                FirstInListOrderConfirmations = false,
                OptOutMarketing = false,
                ReceiveAllInvoices = false,
                ReceiveAllOrderConfirmations = true,
                ReceiveAllShipConfirmations = false,
                ReceiveNewsletters = false,
                ReceiveOwnInvoices = false,
                ReceiveShipConfirmations = false,
                ReceiveSpecials = true,
                ReceiveStatements = false,
                ReceiveTraining = false,
                roleIds = new int[] { },
                UsedNotifications = new List<string>() { "AppearInListOrderConfirmations", "ReceiveAllOrderConfirmations", "ReceiveSpecials" },
                searchText = "laser"
            });
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_return_correct_count_of_contacts_in_advanced_search6()
        {

            //---------------------------------------------------------TEST WITH no SEARCHTEXT AND BOTH FILTERS
            var contactApiController = new ContactApiController(_context, _mapper);
            var expectedResult = _context.Database.SqlQuery<int>(
                "SELECT Contacts.Id FROM Contacts " +
                "INNER JOIN ContactRoles ON Contacts.Id=ContactRoles.ContactId " +
                "INNER JOIN Roles ON ContactRoles.RoleId=Roles.Id " +
                "WHERE (Contacts.AppearInListOrderConfirmations=1 " +
                    "OR Contacts.ReceiveAllOrderConfirmations=1 " +
                    "OR ReceiveSpecials=1) " +
                "AND (Roles.Id=2 " +
                    "OR Roles.Id=3)" +
                "AND Exists(SELECT TOP 1 CustomerNumber FROM Customers WHERE Customers.CustomerNumber=Contacts.CustomerNumber) " +
                "GROUP BY Contacts.Id"
            ).ToList();

            var result = contactApiController.AdvancedSearch(new AdvancedSearchDTO()
            {
                AppearInListOrderConfirmations = true,
                FirstInListOrderConfirmations = false,
                OptOutMarketing = false,
                ReceiveAllInvoices = false,
                ReceiveAllOrderConfirmations = true,
                ReceiveAllShipConfirmations = false,
                ReceiveNewsletters = false,
                ReceiveOwnInvoices = false,
                ReceiveShipConfirmations = false,
                ReceiveSpecials = true,
                ReceiveStatements = false,
                ReceiveTraining = false,
                roleIds = new int[] {2, 3},
                UsedNotifications = new List<string>() { "AppearInListOrderConfirmations", "ReceiveAllOrderConfirmations", "ReceiveSpecials" },
                searchText = ""
            });
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_return_correct_count_of_contacts_in_advanced_search7()
        {
            //---------------------------------------------------------TEST WITH SEARCHTEXT AND BOTH FILTERS
            var contactApiController = new ContactApiController(_context, _mapper);
            var expectedResult = _context.Database.SqlQuery<int>(
                "SELECT Contacts.Id FROM Contacts " +
                "INNER JOIN ContactRoles ON Contacts.Id=ContactRoles.ContactId " +
                "INNER JOIN Roles ON ContactRoles.RoleId=Roles.Id " +
                "INNER JOIN Customers ON Contacts.CustomerNumber=Customers.CustomerNumber " +
                "WHERE (Contacts.AppearInListOrderConfirmations=1 " +
                    "OR Contacts.ReceiveAllOrderConfirmations=1 " +
                    "OR ReceiveSpecials=1) " +
                "AND (Roles.Id=2 " +
                    "OR Roles.Id=3)" +
                "AND(Contacts.FirstName LIKE '%las%' " +
                    "OR Contacts.LastName LIKE '%las%' " +
                    "OR Contacts.CustomerNumber LIKE '%las%' " +
                    "OR Contacts.Email LIKE '%las%' " +
                    "OR Customers.Name LIKE '%las%') " +
                "AND Exists(SELECT TOP 1 CustomerNumber FROM Customers WHERE Customers.CustomerNumber=Contacts.CustomerNumber) " +
                "GROUP BY Contacts.Id"
            ).ToList();

            var result = contactApiController.AdvancedSearch(new AdvancedSearchDTO()
            {
                AppearInListOrderConfirmations = true,
                FirstInListOrderConfirmations = false,
                OptOutMarketing = false,
                ReceiveAllInvoices = false,
                ReceiveAllOrderConfirmations = true,
                ReceiveAllShipConfirmations = false,
                ReceiveNewsletters = false,
                ReceiveOwnInvoices = false,
                ReceiveShipConfirmations = false,
                ReceiveSpecials = true,
                ReceiveStatements = false,
                ReceiveTraining = false,
                roleIds = new int[] { 2, 3 },
                UsedNotifications = new List<string>() { "AppearInListOrderConfirmations", "ReceiveAllOrderConfirmations", "ReceiveSpecials" },
                searchText = "las"
            });
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_return_correct_count_of_contacts_in_advanced_search8()
        {
            //---------------------------------------------------------TEST WITH "=" SEARCHTEXT AND BOTH FILTERS
            var contactApiController = new ContactApiController(_context, _mapper);
            var expectedResult = _context.Database.SqlQuery<int>(
                "SELECT Contacts.Id FROM Contacts " +
                "INNER JOIN ContactRoles ON Contacts.Id=ContactRoles.ContactId " +
                "INNER JOIN Roles ON ContactRoles.RoleId=Roles.Id " +
                "INNER JOIN Customers ON Contacts.CustomerNumber=Customers.CustomerNumber " +
                "WHERE (Contacts.AppearInListOrderConfirmations=1 " +
                    "OR Contacts.ReceiveAllOrderConfirmations=1 " +
                    "OR ReceiveSpecials=1) " +
                "AND (Roles.Id=2 " +
                    "OR Roles.Id=3)" +
                "AND Contacts.CustomerNumber='LASERELEC      '" +
                "AND Exists(SELECT TOP 1 CustomerNumber FROM Customers WHERE Customers.CustomerNumber=Contacts.CustomerNumber) " +
                "GROUP BY Contacts.Id"
            ).ToList();

            var result = contactApiController.AdvancedSearch(new AdvancedSearchDTO()
            {
                AppearInListOrderConfirmations = true,
                FirstInListOrderConfirmations = false,
                OptOutMarketing = false,
                ReceiveAllInvoices = false,
                ReceiveAllOrderConfirmations = true,
                ReceiveAllShipConfirmations = false,
                ReceiveNewsletters = false,
                ReceiveOwnInvoices = false,
                ReceiveShipConfirmations = false,
                ReceiveSpecials = true,
                ReceiveStatements = false,
                ReceiveTraining = false,
                roleIds = new int[] { 2, 3 },
                UsedNotifications = new List<string>() { "AppearInListOrderConfirmations", "ReceiveAllOrderConfirmations", "ReceiveSpecials" },
                searchText = "=laserelec"
            });
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }
    }
}