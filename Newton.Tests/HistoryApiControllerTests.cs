﻿using System;
using NUnit.Framework;
using Newton.Web.Controllers;
using Newton.DAL;
using AutoMapper;
using Newton.Web.DTO;
using System.Web.Http.Results;
using System.Collections.Generic;
using Moq;
using Newton.Domain.Models;
using Newton.Web;
using System.Linq;
using System.Web.Http;
using System.Data.Entity;

namespace Newton.Tests
{
    [TestFixture]
    class HistoryApiControllerTests
    {
        private WaterSkiContext _context;
        private DbContextTransaction _transaction;
        private IMapper _mapper1;
        private IMapper _mapper2;
        private IMapper _mapper3;

        [OneTimeSetUp]
        public void OneTimeSetup()
        {
            System.Data.Entity.Database.SetInitializer(new NewtonInitializer());
            _context = new WaterSkiContext();

            var mapperMock1 = new Mock<IMapper>();
            var mapperMock2 = new Mock<IMapper>();
            var mapperMock3 = new Mock<IMapper>();

            mapperMock1.Setup(m => m.Map<List<History>, List<HistoryListDTO>>(It.IsAny<List<History>>()))
               .Returns(new Func<List<History>, List<HistoryListDTO>>(historyList => DataHelper.GetDTViewModels(historyList)));

            _mapper1 = mapperMock1.Object;

            mapperMock2.Setup(m => m.Map<List<HistorySubType>, List<HistorySubTypesDTO>>(It.IsAny<List<HistorySubType >> ()))
                .Returns(new Func<List<HistorySubType>, List<HistorySubTypesDTO>>(historyList => DataHelper.GetDTViewModels(historyList)));

            _mapper2 = mapperMock2.Object;

            mapperMock3.Setup(m => m.Map<NewHistoryItemDTO, History> (It.IsAny<NewHistoryItemDTO>()))
                .Returns(new Func<NewHistoryItemDTO, History>(historyItem => DataHelper.GetDTViewModels(historyItem)));

            _mapper3 = mapperMock3.Object;
        }

        [OneTimeTearDown]
        public void OneTimeTeardown()
        {
            _context.Database.Delete();
            _context.Dispose();
        }

        [SetUp]
        public void Init()
        {
            _transaction = _context.Database.BeginTransaction();

        }

        [TearDown]
        public void Cleanup()
        {
            _transaction.Rollback();
            _transaction.Dispose();
        }

        [Test]
        public void Should_get_the_list_of_all_history_items_correctly()
        {
            List<History> expectedResult = _context.HistoryItems.Include("HistorySubType").Include("HistorySubType.HistoryType").Where(x => x.CustomerNumber == "LASERELEC      ").ToList();
            var customerApiController = new HistoryApiController(_context, _mapper1);
            var result = customerApiController.Get("LASERELEC      ").ToList();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count != 0);
            Assert.That(result.Count, Is.EqualTo(expectedResult.Count));
        }

        [Test]
        public void Should_get_the_list_of_all_history_types_correctly()
        {
            List<HistorySubType> historyTypes = _context.HistorySubTypes.Include("HistoryType").ToList();
            var mappedHistoryTypes = _mapper2.Map<List<HistorySubType>, List<HistorySubTypesDTO>>(historyTypes);
            var customerApiController = new HistoryApiController(_context, _mapper2);
            var result = customerApiController.GetTypes();

            Assert.That(result, Is.Not.Null);
            Assert.That(result.Count, Is.EqualTo(mappedHistoryTypes.Count));
        }

        [Test]
        public void Should_check_if_history_item_saves_correctly()
        {
            NewHistoryItemDTO historyItem = new NewHistoryItemDTO
            {
                Detail = "testdetail",
                DateCreated = new DateTime(2015, 1, 18),
                ContactId = 1,
                ContactName = "testName",
                HistorySubTypeId = 1,
                HistoryTypeId = 1
            };
            var customerApiController = new HistoryApiController(_context, _mapper3);
            var result = customerApiController.SaveHistory(historyItem) as StatusCodeResult;
            var resultHistoryItem = _context.HistoryItems.Where(x => x.ContactName == historyItem.ContactName).First();

            Assert.That(result, Is.Not.Null);
            Assert.That(resultHistoryItem, Is.Not.Null);
            Assert.That(resultHistoryItem.Detail, Is.EqualTo(historyItem.Detail));

        }

    }
}
