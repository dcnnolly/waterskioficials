﻿using Newton.Web.Security.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Newton.Web.Security
{
    public class WaterSkiPrincipal : INewtonPrincipal
    {
        private List<string> _roles;

        public WaterSkiPrincipal(INewtonIdentity identity, List<string> roles)
        {
            Identity = identity;
            _roles = roles;
        }

        public INewtonIdentity Identity
        {
            get; private set;
        }

        IIdentity IPrincipal.Identity
        {
            get
            {
                return Identity;
            }
        }

        public bool IsInRole(string role)
        {
            return _roles.IndexOf(role) < 0 ? false : true;
        }

        public bool IsAdmin()
        {
            return IsInRole("NewtonAdmin");
        }
    }
}
