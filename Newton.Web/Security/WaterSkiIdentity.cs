﻿using Newton.Domain.Models;
using Newton.Web.Security.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.Security
{
    public class WaterSkiIdentity : INewtonIdentity
    {
        private User _user { get; set; }

        public WaterSkiIdentity(User user)
        {
            _user = user;
        }

        public string AuthenticationType
        {
            get
            {
                return "NewtonAuthentication";
            }
        }

        public bool IsAuthenticated
        {
            get; private set;
        }

        public string Name
        {
            get
            {
                return _user.Name;
            }
        }

        public string Username
        {
            get
            {
                return _user.Username;
            }
        }
    }
}
