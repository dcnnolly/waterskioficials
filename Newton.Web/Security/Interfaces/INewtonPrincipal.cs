﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;

namespace Newton.Web.Security.Interfaces
{
    public interface INewtonPrincipal : IPrincipal
    {
        bool IsAdmin();
    }
}