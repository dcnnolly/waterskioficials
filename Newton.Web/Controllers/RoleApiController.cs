﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace Newton.Web.Controllers
{
    public class RoleApiController : ApiController
    {
        private WaterSkiContext _context;

        public RoleApiController(WaterSkiContext context)
        {
            _context = context;
        }

        [HttpGet]
        [ActionName("allroles")]
        public IEnumerable<Role> Get()
        {
            return _context.UserRoles.ToList();
        }

        //[HttpGet]
        //[ActionName("remainingroles")]
        //public IEnumerable<Role> GetRemainingRoles()
        //{
        //    IEnumerable<Role> allRoles = _context.Roles.ToList();

        //    IEnumerable<Role> selectedRoles = _context.Roles.Include("Contacts").Where(x => x.Contacts() x.IsUnique == true).ToList();
        //}

        [HttpGet]
        [ActionName("role")]
        public IHttpActionResult Get(int id)
        {
            var role = _context.UserRoles.Find(id);

            if (role == null)
            {
                return NotFound();
            }

            return Ok(role);
        }

        [HttpPost]
        [ActionName("deleterole")]
        public IHttpActionResult Delete(int id)
        {
            var role = _context.UserRoles.Find(id);

            if (role == null)
            {
                return NotFound();
            }

            _context.UserRoles.Remove(role);
            _context.SaveChanges();

            return Ok();
        }
    }
}
