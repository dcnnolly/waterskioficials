﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;
using Newton.Web.DTO;

namespace Newton.Web.Controllers
{
    public class HistorySubTypesApiController : ApiController
    {
        private WaterSkiContext _context;
        private IMapper _mapper;

        public HistorySubTypesApiController(WaterSkiContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        [ActionName("allsubtypes")]
        public IEnumerable<HistorySubTypeDTO> Get()
        {
            List<HistorySubType> historySubTypes = _context.HistorySubTypes.Include("HistoryType").ToList();

            var view = _mapper.Map<List<HistorySubType>, List<HistorySubTypeDTO>>(historySubTypes);

            return view;
        }

        [HttpGet]
        [ActionName("subtype")]
        public IHttpActionResult Get(int id)
        {
            var subType = _context.HistorySubTypes.Find(id);

            if (subType == null)
            {
                return NotFound();
            }

            return Ok(subType);
        }

        [HttpPost]
        [ActionName("deletesubtype")]
        public IHttpActionResult Delete(int id)
        {
            var subType = _context.HistorySubTypes.Find(id);

            if (subType == null)
            {
                return NotFound();
            }

            _context.HistorySubTypes.Remove(subType);
            _context.SaveChanges();

            return Ok();
        }
    }
}