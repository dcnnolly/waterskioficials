﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.Entity;

namespace Newton.Web.Controllers
{
    public class HistoryTypesApiController : ApiController
    {
        private WaterSkiContext _context;

        public HistoryTypesApiController(WaterSkiContext context)
        {
            _context = context;
        }

        [HttpGet]
        [ActionName("alltypes")]
        public IEnumerable<HistoryType> Get()
        {
            return _context.HistoryTypes.ToList();
        }

        [HttpGet]
        [ActionName("type")]
        public IHttpActionResult Get(int id)
        {
            var type = _context.HistoryTypes.Find(id);

            if (type == null)
            {
                return NotFound();
            }

            return Ok(type);
        }

        [HttpPost]
        [ActionName("deletetype")]
        public IHttpActionResult Delete(int id)
        {
            var type = _context.HistoryTypes.Find(id);

            if (type == null)
            {
                return NotFound();
            }

            _context.HistoryTypes.Remove(type);
            _context.SaveChanges();

            return Ok();
        }
    }
}