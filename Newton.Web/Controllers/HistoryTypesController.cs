﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.Filters;
using Newton.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Newton.Web.Controllers
{
    public class HistoryTypesController : Controller
    {

        private readonly WaterSkiContext _context;

        private readonly IMapper _mapper;

        public HistoryTypesController(IMapper mapper, WaterSkiContext context)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: HistoryType
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new HistoryTypeViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Create(HistoryTypeViewModel viewModel)
        {
            if (viewModel == null)
            {
                return RedirectToAction("Index");
            }

            var model = _mapper.Map<HistoryTypeViewModel, HistoryType>(viewModel);

            _context.HistoryTypes.Add(model);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (id < 1)
            {
                return RedirectToAction("Index");
            }

            var model = _context.HistoryTypes.Find(id);

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = _mapper.Map<HistoryType, HistoryTypeViewModel>(model);

            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(HistoryTypeViewModel viewModel)
        {
            if (viewModel == null)
            {
                return RedirectToAction("Index");
            }

            var model = _mapper.Map<HistoryTypeViewModel, HistoryType>(viewModel);

            var currentModel = await _context.HistoryTypes.FindAsync(viewModel.Id);
            _context.Entry(currentModel).CurrentValues.SetValues(model);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (id < 1)
            {
                return RedirectToAction("Index");
            }

            HistoryType model = _context.HistoryTypes.Find(id);

            _context.HistoryTypes.Remove(model);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}