﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.Filters;
using Newton.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Newton.Web.Controllers
{
    public class HistorySubTypesController : Controller
    {

        private readonly WaterSkiContext _context;

        private readonly IMapper _mapper;

        public HistorySubTypesController(IMapper mapper, WaterSkiContext context)
        {
            _context = context;
            _mapper = mapper;
        }
        // GET: HistoryType
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            var view = new HistorySubTypeViewModel();

            view.HistoryTypes = new List<SelectListItem>();
            foreach (HistoryType type in _context.HistoryTypes)
            {
                view.HistoryTypes.Add(new SelectListItem()
                {
                    Text = type.Name,
                    Value = type.Id.ToString()
                });
            }

            return View(view);
        }

        [HttpPost]
        public async Task<ActionResult> Create(HistorySubTypeViewModel viewModel)
        {
            if (viewModel == null)
            {
                return RedirectToAction("Index");
            }

            var model = _mapper.Map<HistorySubTypeViewModel, HistorySubType>(viewModel);

            _context.HistorySubTypes.Add(model);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (id < 1)
            {
                return RedirectToAction("Index");
            }

            var model = _context.HistorySubTypes.Find(id);

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = _mapper.Map<HistorySubType, HistorySubTypeViewModel>(model);
            viewModel.HistoryTypes = new List<SelectListItem>();
            foreach (HistoryType type in _context.HistoryTypes)
            {
                viewModel.HistoryTypes.Add(new SelectListItem()
                {
                    Text = type.Name,
                    Value = type.Id.ToString()
                });
            }

            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(HistorySubTypeViewModel viewModel)
        {
            if (viewModel == null)
            {
                return RedirectToAction("Index");
            }

            var model = _mapper.Map<HistorySubTypeViewModel, HistorySubType>(viewModel);

            var currentModel = await _context.HistorySubTypes.FindAsync(viewModel.Id);
            _context.Entry(currentModel).CurrentValues.SetValues(model);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Delete(int id)
        {
            if (id < 1)
            {
                return RedirectToAction("Index");
            }

            HistorySubType model = _context.HistorySubTypes.Find(id);

            _context.HistorySubTypes.Remove(model);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}