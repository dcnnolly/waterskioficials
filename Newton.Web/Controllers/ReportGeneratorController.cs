﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.DTO;
using Newton.Web.Filters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Newton.Web.Controllers
{
    public class ReportGeneratorController : Controller
    {
        private WaterSkiContext _context;

        private IMapper _mapper;

        public ReportGeneratorController(WaterSkiContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        private FilePathResult GenerateReport(IEnumerable<int> contactId)
        {
            var contacts = _context.Contacts.Where(c => contactId.Contains(c.Id)).ToList();
            var contactReportItems = new List<ContactReportItemDTO>();

            foreach (var contact in contacts)
            {
                contactReportItems.Add(new ContactReportItemDTO()
                {
                    AlternateEmail = contact.AlternateEmail,
                    CustomerNumber = contact.CustomerNumber,
                    Email =  contact.Email,
                    FirstName = contact.FirstName,
                    Id = contact.Id,
                    LastName = contact.LastName,
                    Mobile = contact.Mobile,
                    Phone = contact.Phone,
                });
            }
            //var contactReportItems = _mapper.Map<List<Contact>, List<ContactReportItemDTO>>(contacts);

            string fullName = "contacts_" + Guid.NewGuid() + ".csv";
            string filePath = Path.Combine(Server.MapPath("~/App_Data"), fullName);

            using (var stream = System.IO.File.Create(filePath))
            using (var streamWriter = new StreamWriter(stream))
            {
                var fileContents = new StringBuilder();

                var properties = typeof(ContactReportItemDTO).GetProperties();

                var names = new List<string>();
                foreach (var propertyInfo in properties)
                {
                    names.Add(propertyInfo.Name);
                }
                fileContents.AppendLine(String.Join(",", names));

                foreach (var contact in contactReportItems)
                {
                    var dataLineArray = new List<string>();
                    foreach (var property in properties)
                    {
                        var value = (property.GetValue(contact) != null) ? property.GetValue(contact).ToString() : "";
                        dataLineArray.Add(value);
                    }

                    fileContents.AppendLine(String.Join(",", dataLineArray));
                }

                streamWriter.Write(fileContents.ToString());
            }

            return File(filePath, "text/csv", fullName);
        }

        [System.Web.Mvc.HttpPost, FileDownload]
        public FilePathResult DownloadReport([FromBody]IEnumerable<int> contactId)
        {
            return GenerateReport(contactId);
        }

        public ActionResult RemoveReportFile()
        {
            var files = Directory.GetFiles(Server.MapPath("~/App_Data"));

            foreach (var file in files)
            {
                System.IO.File.Delete(file);
            }

            return null;
        }
    }
}