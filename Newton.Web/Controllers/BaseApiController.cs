﻿using Newton.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Newton.Web.Controllers
{
    public class BaseApiController : ApiController
    {
        protected virtual new WaterSkiPrincipal User
        {
            get { return HttpContext.Current.User as WaterSkiPrincipal; }
        }

    }
}