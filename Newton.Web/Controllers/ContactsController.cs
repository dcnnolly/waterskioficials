﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.ViewModels;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Newton.Web.Controllers
{
    [Authorize]
    public class ContactsController : Controller
    {
        private readonly WaterSkiContext _context;

        private readonly IMapper _mapper;

        public ContactsController(WaterSkiContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: Home
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        // GET: Home/Details/5
        public ActionResult Details(string number)
        {
            if (string.IsNullOrEmpty(number))
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Customer customer = _context.Customers.Find(number);
            if (customer == null)
            {
                return HttpNotFound();
            }
            return View(customer);
        }

        // GET: Home/Create
        public ActionResult Create()
        {

            CreateContactViewModel vmCreateContext = new CreateContactViewModel();


            vmCreateContext.Customers = from x in _context.Customers
                                        orderby x.CustomerNumber
                                        select new SelectListItem()
                                        {
                                            Text = x.CustomerNumber + "-" + x.Name,
                                            Value = x.CustomerNumber
                                        };

            vmCreateContext.Roles = _context.UserRoles.ToList();

            return View(vmCreateContext);
        }

        // POST: Home/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CreateContactViewModel viewModel)
        {

            var newContact = _mapper.Map<CreateContactViewModel, Contact>(viewModel);
            
            _context.Contacts.Add(newContact);
            _context.SaveChanges();

            if (viewModel.selectedChks != null)
            {
                foreach (int roleId in viewModel.selectedChks)
                {
                    var r = _context.UserRoles.Where(x => x.Id == roleId).First();
                    newContact.Roles.Add(r);
                }
            }

            _context.SaveChanges();

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
