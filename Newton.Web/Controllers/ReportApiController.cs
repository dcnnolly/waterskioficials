﻿using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newton.Web.Extensions;
using AutoMapper;

namespace Newton.Web.Controllers
{
    public class ReportApiController : ApiController
    {
        private readonly WaterSkiContext _context;

        private readonly IMapper _mapper;

        public ReportApiController(WaterSkiContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        [ActionName("allreporttypes")]
        public IEnumerable<ReportTypeDTO> GetAllReportTypes()
        {
            var reportDtoList = new List<ReportTypeDTO>();

            foreach (var type in Enum.GetValues(typeof(ReportType)).Cast<ReportType>())
            {
                reportDtoList.Add(new ReportTypeDTO()
                {
                    Id = (int)type,
                    Name = type.GetDisplayName()
                });
            }

            return reportDtoList;
        }

        [HttpGet]
        [ActionName("allreportcolumns")]
        public IEnumerable<ReportColumnDTO> GetAllReportColumns(string CustomerNumber)
        {
            var reportColumns = _context.ReportColumns.ToList();

            var reportColumnDtoList = _mapper.Map<List<ReportColumn>, List<ReportColumnDTO>>(reportColumns);

            return reportColumnDtoList;
        }

        [HttpGet]
        [ActionName("selectedreportcolumns")]
        public IEnumerable<ReportColumnDTO> SelectedReportColumns(string CustomerNumber)
        {
            var reportColumns = _context.ReportColumns.ToList();

            var reportColumnDtoList = _mapper.Map<List<ReportColumn>, List<ReportColumnDTO>>(reportColumns);

            return reportColumnDtoList;
        }

        [HttpGet]
        [ActionName("allreportformats")]
        public IEnumerable<ReportFormatDTO> GetAllReportFormats()
        {
            var reportFormatDtoList = new List<ReportFormatDTO>();

            foreach (var format in Enum.GetValues(typeof(ReportFormat)).Cast<ReportFormat>())
            {
                reportFormatDtoList.Add(new ReportFormatDTO()
                {
                    Id = (int)format,
                    Name = format.ToString()//.GetDisplayName()
                });
            }

            return reportFormatDtoList;
        }

        [HttpPost]
        [ActionName("savereport")]
        public IHttpActionResult SaveReport(NewReportItemDTO DTOreport)
        {
            Report myReport;
            if (DTOreport.Id == 0)
            {
                myReport = new Report()
                {
                    Name = DTOreport.ReportType.GetDisplayName() + " - " + DTOreport.ReportFormat.GetDisplayName(),
                    ReportType = DTOreport.ReportType,
                    ReportFormat = DTOreport.ReportFormat,
                    ReportScheduleFrequency = DTOreport.ReportScheduleFrequency,
                    CustomerNumber = DTOreport.CustomerNumber,
                    IncludeHeaders = DTOreport.IncludeHeaders,
                    Consolidate = DTOreport.Consolidate
                };

                _context.Entry(myReport).State = System.Data.Entity.EntityState.Added;
                _context.SaveChanges();

                SubscribeReport(myReport.Id, DTOreport.ContactId);

                //saveallavailablecolumns----------------------------------------------------------
                var allAvailableColumns = _context.AvailableReportColumns.Where(cr => cr.ReportType == DTOreport.ReportType);
                foreach (var newCol in allAvailableColumns)
                {
                    _context.Entry(new ReportColumn()
                    {
                        Name = newCol.Name,
                        ColumnName = newCol.ColumnName,
                        CustomerNumber = DTOreport.CustomerNumber,
                        Description = newCol.Description,
                        ReportId = myReport.Id,
                        IsSelected = false,
                        OrderInReport = 1
                    }).State = System.Data.Entity.EntityState.Added;
                }
                _context.SaveChanges();

            }
            else
            {
                myReport = _context.Reports.Include("ReportColumns").Where(x => x.Id == DTOreport.Id).First();

                myReport.ReportType = DTOreport.ReportType;
                myReport.ReportFormat = DTOreport.ReportFormat;
                myReport.ReportScheduleFrequency = DTOreport.ReportScheduleFrequency;
                myReport.CustomerNumber = DTOreport.CustomerNumber;
                myReport.IncludeHeaders = DTOreport.IncludeHeaders;
                myReport.Consolidate = DTOreport.Consolidate;

                _context.Entry(myReport).State = System.Data.Entity.EntityState.Modified;
                _context.SaveChanges();

                //update changeable properties to default values
                foreach (var rc in _context.ReportColumns.Where(cr => cr.ReportId == myReport.Id)) {
                    rc.IsSelected = false;
                };
                _context.SaveChanges();

            }

            //updateselectedcolumns----------------------------------------------------------
            foreach (var sr in DTOreport.SelectedColumns)
            {
                ReportColumn rc = _context.ReportColumns.Where(x => x.ReportId == myReport.Id && x.ColumnName == sr.ColumnName).First();
                rc.IsSelected = true;
                rc.Name = sr.Name;
                rc.OrderInReport = DTOreport.SelectedColumns.IndexOf(sr);
                _context.Entry(rc).State = System.Data.Entity.EntityState.Modified;
            }
            _context.SaveChanges();

            //schedule----------------------------------------------------------
            myReport.ScheduleStartTime = null;
            myReport.ScheduleEndTime = null;
            myReport.ExecutionTime = null;
            myReport.ExecutionDayOfWeek = null;
            myReport.ExecutionDay = null;
            if (DTOreport.ReportScheduleFrequency == ReportScheduleFrequency.Hourly)
            {
                //Hourly - save the hours
                myReport.ScheduleStartTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DTOreport.ScheduleStartTime.Value, 00, 00);
                myReport.ScheduleEndTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DTOreport.ScheduleEndTime.Value, 00, 00);
            }
            else
            {
                if (DTOreport.ReportScheduleFrequency == ReportScheduleFrequency.Daily)
                {
                    //Daily - save the hour
                    myReport.ExecutionTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DTOreport.ExecutionTime.Value, 00, 00);
                }
                else if (DTOreport.ReportScheduleFrequency == ReportScheduleFrequency.Weekly)
                {
                    //Weekly - save the week day
                    myReport.ExecutionDayOfWeek = DTOreport.ExecutionDayOfWeek.Value;
                }
                else if (DTOreport.ReportScheduleFrequency == ReportScheduleFrequency.Monthly)
                {
                    myReport.ExecutionDay = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DTOreport.ExecutionDay.Value);
                }
            }
            _context.SaveChanges();

            return StatusCode(HttpStatusCode.Created);

        }

        [HttpPost]
        [ActionName("deletereport")]
        public IHttpActionResult DeleteReport(int id)
        {
            Report myReport = _context.Reports.Where(x => x.Id == id).First();

            _context.Reports.Remove(myReport);
            _context.SaveChanges();

            return StatusCode(HttpStatusCode.Created);
        }

        [HttpGet]
        [ActionName("columnswithselections")]
        public IHttpActionResult GetColumns(int ReportId, string CustomerNumber, int TypeId)
        {
            //selected columns
            List<ReportColumnDTO> dtoSelectedColumns = new List<ReportColumnDTO>();
            List<ReportColumnDTO> dtoAvailableColumns = new List<ReportColumnDTO>();

            if (ReportId == 0)
            {
                //all columns by customer and reporttype
                List<AvailableReportColumn> allAvailableColumns = new List<AvailableReportColumn>();
                allAvailableColumns = _context.AvailableReportColumns.Where(x => x.ReportType == (ReportType)TypeId).ToList();

                List<AvailableReportColumn> includeByDefaultColumns = allAvailableColumns.Where(x => x.IncludeByDefault).ToList();
                List<AvailableReportColumn> remainingColumns = allAvailableColumns.Except(includeByDefaultColumns).ToList();

                dtoSelectedColumns = _mapper.Map<List<AvailableReportColumn>, List<ReportColumnDTO>>(includeByDefaultColumns);
                dtoAvailableColumns = _mapper.Map<List<AvailableReportColumn>, List<ReportColumnDTO>>(remainingColumns);
            }
            else
            {
                //selected columns
                List<ReportColumn> selectedColumns = new List<ReportColumn>();
                List<ReportColumn> availableColumns = new List<ReportColumn>();

                selectedColumns = _context.ReportColumns.Where(r => r.ReportId == ReportId && r.IsSelected == true).OrderBy(x=>x.OrderInReport).ToList();
                availableColumns = _context.ReportColumns.Where(r => r.ReportId == ReportId && r.IsSelected == false).ToList();
                //convert to dtos
                dtoSelectedColumns = _mapper.Map<List<ReportColumn>, List<ReportColumnDTO>>(selectedColumns);
                dtoAvailableColumns = _mapper.Map<List<ReportColumn>, List<ReportColumnDTO>>(availableColumns);
            }
            
            Tuple<List<ReportColumnDTO>, List<ReportColumnDTO>> tplColumns = new Tuple<List<ReportColumnDTO>, List<ReportColumnDTO>>(dtoSelectedColumns, dtoAvailableColumns);

            return Ok(tplColumns);
        }

        [HttpPost]
        [ActionName("subscribereport")]
        public IHttpActionResult SubscribeReport(int ReportId, int ContactId)
        {
            var myReport = _context.Reports.Include("Contacts").Where(x => x.Id == ReportId).First();

            ReportContact selectedContact = myReport.Contacts.Where(x => x.ReportId == ReportId && x.ContactId == ContactId).FirstOrDefault();

            if (selectedContact != null)
            {
                _context.Entry(selectedContact).State = System.Data.Entity.EntityState.Deleted;
            }
            else
            {
                myReport.Contacts.Add(new ReportContact()
                {
                    ReportId = ReportId,
                    ContactId = ContactId
                });
            }

            _context.SaveChanges();

            return StatusCode(HttpStatusCode.Accepted);

        }
    }
}
