﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.DTO;
using Newton.Web.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Configuration;

namespace Newton.Web.Controllers
{
    public class ContactApiController : ApiController
    {
        private readonly WaterSkiContext _context;

        private readonly IMapper _mapper;

        public ContactApiController(WaterSkiContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        [ActionName("contact")]
        public IHttpActionResult Get(int id)
        {
            var contact = _context.Contacts.Find(id);
            
            if (contact == null)
            {
                return NotFound();
            }

            return Ok(contact);
        }

        [HttpPost]
        [ActionName("advancedSearch")]
        public IEnumerable<ContactListItemDTO> AdvancedSearch(AdvancedSearchDTO adv)
        {
            adv.searchText = adv.searchText.ToLower();
            string firstChar = adv.searchText.Length > 0 ? adv.searchText.Substring(0, 1) : null;
            string remainingText = adv.searchText.Length > 1 ? adv.searchText.Substring(1, adv.searchText.Length - 1) : "";

            var query = _context.Contacts.Include("Roles")
                .Where(c => c.IsDeleted == false
                //= sign filter NOT IN USE
                //&& (firstChar == null || //((firstChar == "=" && remainingText == c.CustomerNumber.ToLower()))
                //keyword filter
                // ||(c.CustomerNumber.Trim().ToLower() + " - " + _context.Customers.FirstOrDefault(customer => customer.CustomerNumber == c.CustomerNumber).Name.Trim().ToLower()).Contains(adv.searchText)
                //|| (_context.CustomerInfoItems.FirstOrDefault(customerInfoItem => customerInfoItem.CustomerNumber == c.CustomerNumber).Parent.Contains(adv.searchText))
                //|| (_context.CustomerInfoItems.FirstOrDefault(customerInfoItem => customerInfoItem.CustomerNumber == c.CustomerNumber).AppleDEPId.Contains(adv.searchText))
                //|| (_context.CustomerInfoItems.FirstOrDefault(customerInfoItem => customerInfoItem.CustomerNumber == c.CustomerNumber).AppleResellerId.Contains(adv.searchText))
                //|| (_context.CustomerInfoItems.FirstOrDefault(customerInfoItem => customerInfoItem.CustomerNumber == c.CustomerNumber).MicrosoftMPNId.Contains(adv.searchText))
                && (((c.FirstName.Trim().ToLower() + " " + c.LastName.Trim().ToLower()).Contains(adv.searchText))
                || (c.Email.Trim().ToLower().Contains(adv.searchText))
                || (c.Phone.Trim().ToLower().Contains(adv.searchText))
                || (c.Mobile.Trim().ToLower().Contains(adv.searchText)))
                //roles filters
                && (adv.roleIds.Count() == 0 || c.Roles.Any(l => adv.roleIds.Contains(l.Id))))
            .Select(c => new ContactListItemDTO
            {
                Id = c.Id,
                FullName = c.FirstName.Trim() + " " + c.LastName.Trim(),
                Email = c.Email.Trim(),
                Mobile = c.Mobile.Trim(),
                Phone = c.Phone.Trim(),
                Region = c.Region.Name.Trim(),
                Country = c.Country.Name.Trim(),
            });

            return query.ToList().OrderBy(x => x.CustomerName).ToList();
        }

        [HttpGet]
        [ActionName("roleswithselections")]
        public IHttpActionResult GetContactRoles(int id)
        {

            var listSelected = new List<ContactRoleDTO>();
            var listRemaining = new List<ContactRoleDTO>();

            var contact = _context.Contacts.Include("Roles").Where(x => x.Id == id).First();
            var selectedRoles = contact.Roles.ToList();

            List<int> uniqueRolesIds = (from co in _context.Contacts
                              from r in co.Roles
                              where co.IsDeleted == false
                              where co.CustomerNumber == contact.CustomerNumber
                              where r.IsUnique == true
                              select r.Id).ToList();
            uniqueRolesIds.AddRange(selectedRoles.Select(s => s.Id).ToList());
            var remainingRoles = _context.UserRoles.Where(r => !uniqueRolesIds.Distinct().Contains(r.Id)).ToList();
            
            listSelected = _mapper.Map<List<Role>, List<ContactRoleDTO>>(selectedRoles);
            listRemaining = _mapper.Map<List<Role>, List<ContactRoleDTO>>(remainingRoles);
            
            listSelected.OrderBy(x => x.Name);
            listRemaining.OrderBy(x => x.Name);

            Tuple<List<ContactRoleDTO>,List<ContactRoleDTO>> rolesData = new Tuple<List<ContactRoleDTO>, List<ContactRoleDTO>>(listSelected, listRemaining);

            return Ok(rolesData);
        }

        [HttpGet]
        public IEnumerable<Region> getRegions()
        {
            return _context.Regions.OrderBy(x => x.Name).ToList();
        }

        [HttpGet]
        public IEnumerable<Country> getCountries()
        {
            return _context.Countries.OrderBy(x=>x.Name).ToList();
        }

        [HttpPost]
        [ActionName("updatecontactrole")]
        public IHttpActionResult UpdateContactRole(int id, [FromBody]int roleId)
        {
            var contact = _context.Contacts.Find(id);
            var role = _context.UserRoles.Find(roleId);
            if (contact == null || role == null)
            {
                return NotFound();
            }

            _context.Entry(contact).Collection(c => c.Roles).Load();
            var contactRole = contact.Roles.FirstOrDefault(r => r.Id == roleId);
            if (contactRole == null)
            {
                contact.Roles.Add(role);
            }
            else
            {
                contact.Roles.Remove(role);
            }
            _context.SaveChanges();

            return StatusCode(HttpStatusCode.Accepted);
        }

        // PUT: api/HomeApi/5
        [ActionName("updatecontactdetails")]
        [HttpPut]
        public IHttpActionResult UpdateDetails(int id, Contact contactDetails)
        {
            if (id == 0)
            {
                _context.Contacts.Add(contactDetails);
                //_context.Entry(contactDetails).State = System.Data.Entity.EntityState.Added;
                _context.SaveChanges();
            }
            else
            {
                var contact = _context.Contacts.Find(id);
                if (contact == null)
                {
                    return NotFound();
                }

                _context.Entry(contact).CurrentValues.SetValues(contactDetails);
                _context.Entry(contact).Collection(c => c.Roles).Load();
                _context.SaveChanges();
            }

            return Ok(contactDetails.Id);
        }

        [HttpGet]
        [ActionName("customerreports")]
        public IHttpActionResult GetCustomerReports(int id)
        {
            var dtoList = new List<NewReportItemDTO>();

            var contact = _context.Contacts.Find(id);
            var reports = _context.Reports.Include("Contacts").Where(r => r.CustomerNumber == contact.CustomerNumber).ToList();
            foreach (var report in reports)
            {
                _context.Entry(report).Collection(c => c.ReportColumns).Load();

                var fullSchedule = report.ReportScheduleFrequency.ToString();

                if (report.ReportScheduleFrequency == ReportScheduleFrequency.Daily)
                {
                    fullSchedule += String.Format(" @ {0:htt}", report.ExecutionTime.Value);
                }
                else if (report.ReportScheduleFrequency == ReportScheduleFrequency.Hourly)
                {
                    fullSchedule += String.Format(", {0:htt} to {1:htt}", report.ScheduleStartTime, report.ScheduleEndTime);
                }
                else if (report.ReportScheduleFrequency == ReportScheduleFrequency.Monthly)
                {
                    fullSchedule += String.Format(", {0:dd} d.", report.ExecutionDay.Value);
                }
                else if (report.ReportScheduleFrequency == ReportScheduleFrequency.Weekly)
                {
                    fullSchedule += String.Format(", {0}", (DayOfWeek)report.ExecutionDayOfWeek.Value);
                }

                dtoList.Add(new NewReportItemDTO()
                {
                    //ContactId = contact.Id,
                    Id = report.Id,
                    ReportType = report.ReportType,
                    SelectedColumns = _mapper.Map<List<ReportColumn>, List<ReportColumnDTO>>(report.ReportColumns.ToList()),
                    ReportFormat = report.ReportFormat,
                    ReportScheduleFrequency = report.ReportScheduleFrequency,
                    ReportScheduleFrequencyName = fullSchedule,

                    ScheduleStartTime = (report.ScheduleStartTime != null) ? report.ScheduleStartTime.Value.Hour : 0,
                    ScheduleEndTime = (report.ScheduleEndTime != null) ? report.ScheduleEndTime.Value.Hour : 0,
                    
                    ExecutionTime = (report.ExecutionTime != null) ? report.ExecutionTime.Value.Hour : 0,
                    ExecutionDayOfWeek = (report.ExecutionDayOfWeek != null) ? report.ExecutionDayOfWeek.Value : 0,
                    ExecutionDay = (report.ExecutionDay != null) ? report.ExecutionDay.Value.Day : 0,

                    IncludeHeaders = report.IncludeHeaders,
                    Consolidate = report.Consolidate,

                    CustomerNumber = report.CustomerNumber,
                    ContactId = id,
                    Name = report.Name,
                    IsSubscribed = report.Contacts.Where(x=>x.ContactId == id && x.ReportId == report.Id).Count() > 0 ? true : false
                });
            }

            return Ok(dtoList.OrderByDescending(x=>x.Id));
        }

        [HttpGet]
        [ActionName("getremainingroles")]
        public IHttpActionResult GetRemainingRoles(string id)
        {

            var roles = _context.UserRoles.OrderBy(x=>x.Name).ToList();
            var contacts = _context.Contacts.Where(x => x.CustomerNumber == id && x.IsDeleted == false).ToList();

            foreach (Contact contact in contacts)
            {
                _context.Entry(contact).Collection(c => c.Roles).Load();
                foreach (Role role in contact.Roles)
                {
                    if (role.IsUnique == true)
                    {
                        roles.Remove(role);
                    }
                }
            }

            return Ok(roles);
        }

        [HttpGet]
        [ActionName("getallroles")]
        public IHttpActionResult GetAllRoles()
        {

            var roles = _context.UserRoles.OrderBy(x => x.Name).ToList();

            var rolesList = _mapper.Map<List<Role>, List<ContactRoleDTO>>(roles);

            List<ContactRoleDTO> rolesData = new List<ContactRoleDTO>(rolesList);

            return Ok(rolesData);
        }

        [HttpPost]
        [ActionName("deletecontact")]
        public IHttpActionResult DeleteContact(int id)
        {

            Contact contact = _context.Contacts.Find(id);
            contact.IsDeleted = true;

            History newHistoryRecord = new History();
            newHistoryRecord.ContactId = id;
            newHistoryRecord.ContactName = contact.FirstName + ' ' +  contact.LastName;
            newHistoryRecord.CreatorName = User.Identity.Name;
            newHistoryRecord.CustomerNumber = contact.CustomerNumber;
            newHistoryRecord.DateCreated = DateTime.Now;
            newHistoryRecord.Detail = "User deleted contact.";
            newHistoryRecord.HistorySubTypeId = int.Parse(WebConfigurationManager.AppSettings["HistoryDeleteTypeId"]);//History Informational
            _context.Entry(newHistoryRecord).State = System.Data.Entity.EntityState.Added;

            _context.SaveChanges();
            
            return Ok(HttpStatusCode.Accepted);
        }

    }
}
