﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Newton.Web.Controllers
{
    public class CustomerApiController : ApiController
    {
        private readonly WaterSkiContext _context;

        private readonly IMapper _mapper;

        public CustomerApiController(WaterSkiContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        [ActionName("getcustomerdata")]
        public IHttpActionResult Get(int id)
        {
            var contact = _context.Contacts.Find(id);

            if (contact == null)
            {
                return NotFound();
            }

            Customer customer = _context.Customers.Find(contact.CustomerNumber);

            if (customer == null)
            {
                return NotFound();
            }

            CustomerInfoDTO info = new CustomerInfoDTO();
            CustomerInfo customerInfo = _context.CustomerInfoItems.FirstOrDefault(ci => ci.CustomerNumber == customer.CustomerNumber);
            if(customerInfo == null)
            {
                info = _mapper.Map<Customer, CustomerInfoDTO>(customer);

            }
            else
            {
                info = _mapper.Map<CustomerInfo, CustomerInfoDTO>(customerInfo);
            }

            return Ok(info);
        }
        [HttpGet]
        [ActionName("getcustomernumber")]
        public IHttpActionResult GetNumber(int id)
        {
            var contact = _context.Contacts.Find(id);

            if (contact == null)
            {
                return NotFound();
            }

            string customerNumber = contact.CustomerNumber;

            return Ok(customerNumber);
        }

        [ActionName("updatecustomer")]
        [HttpPut]
        public IHttpActionResult UpdateDetails(CustomerInfo customerDetails)
        {
            CustomerInfo customer = _context.CustomerInfoItems.Where(x => x.Id == customerDetails.Id).FirstOrDefault();
            if (customer == null)
            {
                CustomerInfo newCustomerInfo = new CustomerInfo();
                newCustomerInfo.Id = customerDetails.Id;
                newCustomerInfo.CustomerNumber = customerDetails.CustomerNumber;
                newCustomerInfo.AppleResellerId = customerDetails.AppleResellerId;
                newCustomerInfo.AppleDEPId = customerDetails.AppleDEPId;
                newCustomerInfo.MicrosoftMPNId = customerDetails.MicrosoftMPNId;
                _context.CustomerInfoItems.Add(newCustomerInfo);
            }
            else
            {
                _context.Entry(customer).CurrentValues.SetValues(customerDetails);
            }

            _context.SaveChanges();

            return Ok();
        }
    }
}
