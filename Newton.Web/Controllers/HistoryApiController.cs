﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.DTO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Web.Http;

namespace Newton.Web.Controllers
{
    public class HistoryApiController : ApiController
    {
        private readonly WaterSkiContext _context;
        //private readonly WaterSkiRoleManager _roleManager;
        private readonly IMapper _mapper;
        
        public HistoryApiController(WaterSkiContext context, IMapper mapper)
        {
            _context = context;
            //_roleManager = roleManager;, WaterSkiRoleManager roleManager
            _mapper = mapper;
        }

        [HttpGet]
        [ActionName("allhistory")]
        public IEnumerable<HistoryListDTO> Get(int id)
        {
            //string customerNumber = _context.Contacts.Where(x => x.Id == id).First().CustomerNumber;
            //int[] allContactIds = _context.Contacts.Where(x => x.CustomerNumber == customerNumber).Select(x=>x.Id).ToArray();

            List<History> historyList = _context.HistoryItems.Include("HistorySubType").Include("HistorySubType.HistoryType").Where(x=> x.ContactId == id).ToList();
            return _mapper.Map<List<History>, List<HistoryListDTO>>(historyList).OrderByDescending(x=>x.Id);
        }

        [HttpGet]
        [ActionName("allhistorysubtypes")]
        public IEnumerable<HistorySubTypesDTO> GetTypes()
        {
            List<HistorySubType> historyTypes = _context.HistorySubTypes.Include("HistoryType").ToList();

            var roles = ((ClaimsIdentity)User.Identity).Claims
                .Where(c => c.Type == ClaimTypes.Role)
                .Select(c => c.Value);

            if (!roles.Contains("Admin"))
            {
                if (historyTypes.Any(x => x.Name == "Admin Comment"))
                    historyTypes.Remove(historyTypes.First(x=>x.Name == "Admin Comment"));
            }

            return _mapper.Map<List<HistorySubType>, List<HistorySubTypesDTO>>(historyTypes);
        }

        [HttpPost]
        [ActionName("savehistory")]
        public IHttpActionResult SaveHistory(NewHistoryItemDTO historyItem)
        {
            
            historyItem.CreatorName = User.Identity.Name;
            History history = _mapper.Map<NewHistoryItemDTO, History>(historyItem);

            _context.Entry(history).State = System.Data.Entity.EntityState.Added;
            _context.SaveChanges();

            HistorySubType hst = _context.HistorySubTypes.Where(x => x.Id == historyItem.HistorySubTypeId).First();
            hst.HistoryTypeId = historyItem.HistoryTypeId;

            _context.Entry(hst).State = System.Data.Entity.EntityState.Modified;
            _context.SaveChanges();

            return StatusCode(HttpStatusCode.Accepted);

        }

    }
}
