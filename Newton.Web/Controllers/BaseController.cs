﻿using Newton.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Newton.Web.Controllers
{
    public class BaseController : Controller
    {

        protected virtual new WaterSkiPrincipal User
        {
            get { return HttpContext.User as WaterSkiPrincipal; }
        }

    }
}