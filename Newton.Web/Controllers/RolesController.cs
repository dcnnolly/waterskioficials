﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.Filters;
using Newton.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Newton.Web.Controllers
{
    public class RolesController : Controller
    {
        private readonly WaterSkiContext _context;

        private readonly IMapper _mapper;

        public RolesController(IMapper mapper, WaterSkiContext context)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: Roles
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View(new RoleViewModel());
        }

        [HttpPost]
        public async Task<ActionResult> Create(RoleViewModel viewModel)
        {
            if (viewModel == null)
            {
                return RedirectToAction("Index");
            }

            var model = _mapper.Map<RoleViewModel, Role>(viewModel);

            _context.UserRoles.Add(model);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Edit(int id)
        {
            if (id < 1)
            {
                return RedirectToAction("Index");
            }

            var model = _context.UserRoles.Find(id);

            if (model == null)
            {
                return RedirectToAction("Index");
            }

            var viewModel = _mapper.Map<Role, RoleViewModel>(model);

            return View(viewModel);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(RoleViewModel viewModel)
        {
            if (viewModel == null)
            {
                return RedirectToAction("Index");
            }

            var model = _mapper.Map<RoleViewModel, Role>(viewModel);

            var currentModel = await _context.UserRoles.FindAsync(viewModel.Id);
            _context.Entry(currentModel).CurrentValues.SetValues(model);
            await _context.SaveChangesAsync();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public  ActionResult Delete(int id)
        {
            if (id < 1)
            {
                return RedirectToAction("Index");
            }

            Role model = _context.UserRoles.Find(id);

            _context.UserRoles.Remove(model);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}