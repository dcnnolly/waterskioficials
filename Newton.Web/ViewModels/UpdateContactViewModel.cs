﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.ViewModels
{
    public class UpdateContactViewModel
    {
        public Contact ContactDetails { get; set; }
        public int[] Roles { get; set; }
    }
}