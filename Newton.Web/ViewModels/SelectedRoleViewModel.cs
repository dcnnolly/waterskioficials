﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.ViewModels
{
    public class SelectedRoleViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool Selected { get; set; }
    }
}