﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.ViewModels
{
    public class EditCustomerViewModel
    {
        public CustomerInfo CustomerInfo { get; set; }
        public IList<Contact> AllContacts { get; set; }
        public Contact NewContact { get; set; }
    }
}