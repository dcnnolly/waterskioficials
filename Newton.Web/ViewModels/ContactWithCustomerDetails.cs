﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.ViewModels
{
    public class ContactWithCustomerDetails
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public int CustomerNumber { get; set; }
        public string CustomerName { get; set; }
    }
}