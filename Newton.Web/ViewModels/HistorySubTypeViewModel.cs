﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Newton.Web.ViewModels
{
    public class HistorySubTypeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public HistoryType HistoryType { get; set; }
        public string HistoryTypeId { get; set; }
        public List<SelectListItem> HistoryTypes { get; set; }
    }
}