﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Newton.Web.ViewModels
{
    public class CreateContactViewModel
    {
        public string CustomerNumber { get; set; }
        public IEnumerable<SelectListItem> Customers { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string EmailAlternative { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public bool IsSystemContact { get; set; }
        public bool AppearInListOrderConfirmations { get; set; }
        public bool FirstInListOrderConfirmations { get; set; }
        public bool ReceiveAllOrderConfirmations { get; set; }
        public bool ReceiveOwnInvoices { get; set; }
        public bool ReceiveAllInvoices { get; set; }
        public bool ReceiveStatements { get; set; }
        public bool ReceiveShipConfirmations { get; set; }
        public bool ReceiveAllShipConfirmations { get; set; }
        public bool? OptOutMarketing { get; set; }
        public bool? ReceiveSpecials { get; set; }
        public bool? ReceiveNewsletters { get; set; }
        public bool? ReceiveTraining { get; set; }
        public virtual ICollection<Role> Roles { get; set; }
        public int PrimaryRoleId { get; set; }
        public int[] selectedChks { get; set; }
    }
}