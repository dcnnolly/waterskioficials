﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;
using Autofac;
using Autofac.Integration.WebApi;
using Newton.Web.Controllers;
using AutoMapper;
using System.Collections.Generic;
using Autofac.Integration.Mvc;
using Newton.DAL;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using AuthenticationSample;
using AuthenticationSample.Models;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using System.Web;
using WorkManagerCMS.Data;
using WorkManagerCMS.Web;
using System.Web.Mvc;

[assembly: OwinStartup(typeof(Newton.Web.Startup))]

namespace Newton.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            var builder = new ContainerBuilder();
            var config = System.Web.Http.GlobalConfiguration.Configuration;

            builder.RegisterType<WaterSkiContext>().AsSelf().InstancePerRequest();
            builder.RegisterType<ApplicationUserStore>().As<IUserStore<ApplicationUser>>().InstancePerRequest();
            builder.RegisterType<ApplicationUserManager>().AsSelf().InstancePerRequest();
            builder.RegisterType<ApplicationSignInManager>().AsSelf().InstancePerRequest();
            builder.Register<IAuthenticationManager>(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
            builder.Register<IDataProtectionProvider>(c => app.GetDataProtectionProvider()).InstancePerRequest();

            builder.RegisterAssemblyTypes(typeof(AutoMapperConfiguration).Assembly).As<Profile>();
            builder.Register(context => new MapperConfiguration(cfg =>
            {
                foreach (var profile in context.Resolve<IEnumerable<Profile>>())
                {
                    cfg.AddProfile(profile);
                }
            })).AsSelf().SingleInstance();
            builder.Register(context => context.Resolve<MapperConfiguration>().CreateMapper(context.Resolve))
                .As<IMapper>()
                .InstancePerLifetimeScope();


            builder.RegisterControllers(typeof(ContactsController).Assembly);
            builder.RegisterApiControllers(typeof(ContactApiController).Assembly);
            
            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            app.UseAutofacMiddleware(container);
            app.UseAutofacMvc();

            app.UseAutofacWebApi(config);
            //app.UseWebApi(config);

            ConfigureAuth(app);




            //var builder = new ContainerBuilder();
            //var config = System.Web.Http.GlobalConfiguration.Configuration;

            //builder.RegisterType<WaterSkiContext>().AsSelf().InstancePerRequest();
            //builder.RegisterType<ApplicationUserStore>().As<IUserStore<ApplicationUser>>().InstancePerRequest();
            //builder.RegisterType<ApplicationUserManager>().AsSelf().InstancePerRequest();
            ////builder.RegisterType<ApplicationRoleStore>().As<IRoleStore<ApplicationRole>>().InstancePerRequest();

            //builder.RegisterType<RoleStore<IdentityRole>>().As<IRoleStore<IdentityRole, string>>();
            //builder.RegisterType<ApplicationRoleManager>();

            ////builder.RegisterType<WaterSkiRoleManager>().AsSelf().InstancePerRequest();

            //builder.RegisterType<ApplicationSignInManager>().AsSelf().InstancePerRequest();
            //builder.Register<IAuthenticationManager>(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
            //builder.Register<IDataProtectionProvider>(c => app.GetDataProtectionProvider()).InstancePerRequest();

            //builder.RegisterAssemblyTypes(typeof(AutoMapperConfiguration).Assembly).As<Profile>();
            //builder.Register(context => new MapperConfiguration(cfg =>
            //{
            //    foreach (var profile in context.Resolve<IEnumerable<Profile>>())
            //    {
            //        cfg.AddProfile(profile);
            //    }
            //})).AsSelf().SingleInstance();
            //builder.Register(context => context.Resolve<MapperConfiguration>().CreateMapper(context.Resolve))
            //    .As<IMapper>()
            //    .InstancePerLifetimeScope();


            //builder.RegisterControllers(typeof(ContactsController).Assembly);
            //builder.RegisterApiControllers(typeof(ContactApiController).Assembly);

            //IContainer container = builder.Build();
            //DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            //config.DependencyResolver = new AutofacWebApiDependencyResolver(container);

            //app.UseAutofacMiddleware(container);
            //app.UseAutofacMvc();

            //app.UseAutofacWebApi(config);
            ////app.UseWebApi(config);

            //ConfigureAuth(app);



            //var builder = new ContainerBuilder();
            //var config = System.Web.Http.GlobalConfiguration.Configuration;

            //builder.RegisterType<WaterSkiContext>().InstancePerRequest();

            //builder.RegisterApiControllers(typeof(RoleApiController).Assembly);
            //builder.RegisterControllers(typeof(RolesController).Assembly);

            //// Automapper injection settings

            //IContainer container = builder.Build();

            //System.Web.Mvc.DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
            //config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
            //// Configure the db context, user manager and signin manager to use a single instance per request
            ////app.CreatePerOwinContext(ApplicationDbContext.Create);
            ////app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create);
            ////app.CreatePerOwinContext<ApplicationSignInManager>(ApplicationSignInManager.Create);

            //builder.RegisterType<ApplicationUserManager>().AsSelf().InstancePerRequest();
            //builder.RegisterType<ApplicationSignInManager>().AsSelf().InstancePerRequest();
            //builder.Register<IAuthenticationManager>(c => HttpContext.Current.GetOwinContext().Authentication).InstancePerRequest();
            //builder.Register<IDataProtectionProvider>(c => app.GetDataProtectionProvider()).InstancePerRequest();


        }
    }
}
