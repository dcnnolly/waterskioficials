﻿using Newton.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Newton.Web.ViewExtensions
{
    public abstract class BaseViewPage : WebViewPage
    {
        public virtual new WaterSkiPrincipal User
        {
            get { return base.User as WaterSkiPrincipal; }
        }
    }

    public abstract class BaseViewPage<TModel> : WebViewPage<TModel>
    {
        public virtual new WaterSkiPrincipal User
        {
            get { return base.User as WaterSkiPrincipal; }
        }
    }
}