﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.DTO
{
    public class ContactReportItemDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string AlternateEmail { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public bool IsSystemContact { get; set; }
        public bool AppearInListOrderConfirmations { get; set; }
        public bool FirstInListOrderConfirmations { get; set; }
        public bool ReceiveAllOrderConfirmations { get; set; }
        public bool ReceiveOwnInvoices { get; set; }
        public bool ReceiveAllInvoices { get; set; }
        public bool ReceiveStatements { get; set; }
        public bool ReceiveShipConfirmations { get; set; }
        public bool ReceiveAllShipConfirmations { get; set; }
        public bool? OptOutMarketing { get; set; }
        public bool? ReceiveSpecials { get; set; }
        public bool? ReceiveNewsletters { get; set; }
        public bool? ReceiveTraining { get; set; }
        public string CustomerNumber { get; set; }
        public string PrimaryRoleName { get; set; }
    }
}