﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.DTO
{
    public class NewHistoryItemDTO
    {
        public string CustomerNumber { get; set; }
        public int Id { get; set; }
        public int ContactId { get; set; }
        public string CreatorName { get; set; }
        public string ContactName { get; set; }
        public DateTime DateCreated { get; set; }
        public int HistoryTypeId { get; set; }
        public int HistorySubTypeId { get; set; }
        public string Detail { get; set; }
    }
}