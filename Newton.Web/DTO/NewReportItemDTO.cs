﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.DTO
{
    public class NewReportItemDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ReportType ReportType { get; set; }
        public List<ReportColumnDTO> SelectedColumns { get; set; }
        public ReportFormat ReportFormat { get; set; }
        public ReportScheduleFrequency ReportScheduleFrequency { get; set; }
        public string ReportScheduleFrequencyName { get; set; }
        public string ExecutionWeekDay { get; set; }
        public string ExecutionMonthDay { get; set; }
        public int? ScheduleStartTime { get; set; }
        public int? ScheduleEndTime { get; set; }
        public int? ExecutionTime { get; set; }
        public int? ExecutionDayOfWeek { get; set; }
        public int? ExecutionDay { get; set; }
        public string CustomerNumber { get; set; }
        public bool IncludeHeaders { get; set; }
        public bool Consolidate { get; set; }
        public int ContactId { get; set; }
        public bool IsSubscribed { get; set; }
    }
}