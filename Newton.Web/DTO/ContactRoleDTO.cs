﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.DTO
{
    public class ContactRoleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}