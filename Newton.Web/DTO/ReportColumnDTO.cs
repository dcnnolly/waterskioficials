﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.DTO
{
    public class ReportColumnDTO
    {
        public string ColumnName { get; set; }
        public string Name { get; set; }
    }
}