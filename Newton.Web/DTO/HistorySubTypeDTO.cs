﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.DTO
{
    public class HistorySubTypeDTO
    {
        public int Id { get; set; }
        public string  Name { get; set; }
        public string HistoryTypeName { get; set; }
    }
}