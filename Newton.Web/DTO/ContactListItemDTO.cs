﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.DTO
{
    public class ContactListItemDTO
    {
        public string CustomerNumber { get; set; }
        public string CustomerName { get; set; }
        public int Id { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Region { get; set; }
        public string Country { get; set; }
    }
}