﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.DTO
{
    public class CustomerInfoDTO
    {
        public int Id { get; set; }
        public string CustomerNumber { get; set; }
        public string Name { get; set; }
        public string AppleResellerId { get; set; }
        public string AppleDEPId { get; set; }
        public string MicrosoftMPNId { get; set; }
        public string Parent { get; set; }
    }
}