﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.DTO
{
    public class AdvancedSearchDTO
    {
        public string searchText { get; set; }
        public int[] roleIds { get; set; }
        public bool AppearInListOrderConfirmations { get; set; }
        public bool FirstInListOrderConfirmations { get; set; }
        public bool ReceiveAllOrderConfirmations { get; set; }
        public bool ReceiveOwnInvoices { get; set; }
        public bool ReceiveAllInvoices { get; set; }
        public bool ReceiveStatements { get; set; }
        public bool ReceiveShipConfirmations { get; set; }
        public bool ReceiveAllShipConfirmations { get; set; }
        public bool? OptOutMarketing { get; set; }
        public bool? ReceiveSpecials { get; set; }
        public bool? ReceiveNewsletters { get; set; }
        public bool? ReceiveTraining { get; set; }
        public List<string> UsedNotifications { get; set; }
    }
}