﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.DTO
{
    public class HistoryListDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int ContactId { get; set; }
        public string CreatorName { get; set; }
        public string ContactName { get; set; }
        public string DateCreated { get; set; }
        public int TypeId { get; set; }
        public string TypeName { get; set; }
        public string SubTypeName { get; set; }
        public string Detail { get; set; }
    }
}