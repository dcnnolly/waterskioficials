﻿using System.Web;
using System.Web.Optimization;

namespace Newton.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //default bundles

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            //custom bundles

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            //bundles.Add(new StyleBundle("~/Content/mainpagecss").Include(
            //          "~/Content/accordion.css",
            //          "~/Content/site.css",
            //          "~/Content/Home/BasicStyles.css",
            //          "~/Content/Home/Sections/ContactDetails.css",
            //          "~/Content/Home/Sections/History.css",
            //          "~/Content/Home/Sections/Notifications.css",
            //          "~/Content/Home/Sections/Reporting.css"));

            bundles.Add(new ScriptBundle("~/Scripts/mainpageangular").Include(
                      "~/Scripts/ngTools/angular.js",
                      "~/Scripts/ngTools/spin.min.js",
                      "~/Scripts/ngTools/angular-spinner.min.js",
                      "~/Scripts/ngTools/angular-loading-spinner.js"));

            bundles.Add(new ScriptBundle("~/Scripts/mainpagejquery").Include(
                      "~/Scripts/PageSpecific/Contacts/jquery.easyAccordion.js",
                      "~/Scripts/utility.js",
                      "~/Scripts/afterresize.min.js",
                      "~/Scripts/PageSpecific/Contacts/TabMenuLoad.js",
                      "~/Scripts/multiselect.js"));

        }
    }
}
