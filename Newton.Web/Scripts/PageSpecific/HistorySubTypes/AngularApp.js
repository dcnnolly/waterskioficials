﻿
var app = angular.module('ngNewton', []);

app.factory('ngNewtonFactory', function ($http) {

    var myAPI = {};

    myAPI.getHistorySubTypeList = function () {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/historySubTypesapi/allsubtypes',
        });
    };

    myAPI.getHistorySubTypeData = function (historySubTypeId) {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/HistorySubTypesApi/' + historySubTypeId,
        });
    };

    myAPI.deleteHistorySubType = function (historySubTypeId) {
        return $http({
            method: 'POST',
            accept: 'json/application',
            url: '/api/HistorySubTypesApi/deletesubtype/' + historySubTypeId,
        });
    };

    return myAPI;
});

app.controller('ngNewtonCtrl', function ($scope, ngNewtonFactory) {



    $scope.showHistorySubTypeDetails = true;

    window.onload = function () { $scope.loadHistorySubTypesData() };

    $scope.HistorySubTypeList = [];

    $scope.SelectedHistorySubType = [];

    $scope.loadHistorySubTypesData = function () {
        $scope.HistorySubTypeList = [];
        $scope.SelectedHistorySubType.HistoryType = [];

        ngNewtonFactory.getHistorySubTypeList()
            .success(function (response) {
                $scope.HistorySubTypeList = response;
            })
            .finally(function (response) {
            });
    }

    $scope.loadHistorySubType = function (id) {

        ngNewtonFactory.getHistorySubTypeData(id)
            .success(function (response) {
                $scope.SelectedHistorySubType = response;
                $scope.showHistorySubTypeDetails = true;
            })
            .finally(function (response) {
            });
    }

    $scope.sortType = ['Name', 'HistoryTypeName'];
    $scope.sortReverse = false;
    $scope.sortBy = function (propertyName) {
        if ($scope.sortType[0] == propertyName) {
            $scope.sortReverse = !$scope.sortReverse;//backtodefault
        }
        else {
            $scope.sortType[1] = $scope.sortType[0];
            $scope.sortType[0] = propertyName;
        }
    };

    $scope.showOverlay = false;

    $scope.delete = function (id) {
        window.location.href = "/HistoryTypes/Delete/" + id;
    }

    $scope.confirmHistorySubTypeDeletion = function (item) {
        $scope.SelectedHistorySubType = item;
        if (!$scope.showDeleteScreen) {
            $scope.showDeleteScreen = !$scope.showDeleteScreen;
            $scope.showOverlay = !$scope.showOverlay;
        }
    }

    $scope.cancelHistorySubTypeDeletion = function (item) {
        if ($scope.showDeleteScreen) {
            $scope.showDeleteScreen = !$scope.showDeleteScreen;
            $scope.showOverlay = !$scope.showOverlay;
        }
    }

    $scope.deleteHistorySubType = function (item) {
        if ($scope.showDeleteScreen) {
            $scope.showDeleteScreen = !$scope.showDeleteScreen;
            $scope.showOverlay = !$scope.showOverlay;
        }
        ngNewtonFactory.deleteHistorySubType($scope.SelectedHistorySubType.Id)
            .success(function () {
                $scope.HistorySubTypeList.splice($scope.getIndex($scope.HistorySubTypeList, $scope.SelectedHistorySubType), 1);
            })
    }

    $scope.getIndex = function (roles, r) {
        for (var i = 0; i < roles.length; i++) {
            if (r.Id == roles[i].Id && r.Name == roles[i].Name) {
                return i;
            }
        }
    }
});