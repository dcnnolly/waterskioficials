﻿
var app = angular.module('ngNewton', []);

app.factory('ngNewtonFactory', function ($http) {

    var myAPI = {};

    myAPI.getHistoryTypeList = function () {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/historytypesapi/alltypes',
        });
    };

    myAPI.getHistoryTypeData = function (historyTypeId) {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/HistoryTypesApi/' + historyTypeId,
        });
    };

    myAPI.deleteHistoryType = function (historyTypeId) {
        return $http({
            method: 'POST',
            accept: 'json/application',
            url: '/api/HistoryTypesApi/deletetype/' + historyTypeId,
        });
    };

    return myAPI;
});

app.controller('ngNewtonCtrl', function ($scope, ngNewtonFactory) {

    $scope.delete = function (id) {
        window.location.href = "/HistorySubTypes/Delete/" + id;
    }

    $scope.showHistoryTypeDetails = true;

    window.onload = function () { $scope.loadHistoryTypesData() };

    $scope.HistoryTypeList = [];

    $scope.SelectedHistoryType = [];

    $scope.loadHistoryTypesData = function () {
        $scope.HistoryTypeList = [];

        ngNewtonFactory.getHistoryTypeList()
            .success(function (response) {
                $scope.HistoryTypeList = response;
            })
            .finally(function (response) {
            });
    }

    $scope.loadHistoryType = function (id) {

        ngNewtonFactory.getContactData(id)
            .success(function (response) {
                $scope.SelectedHistoryType = response;
                $scope.showCustomerDetails = true;
            })
            .finally(function (response) {
            });
    }

    $scope.sortType = 'Name';
    $scope.sortReverse = false;//latest at the top
    $scope.sortBy = function (propertyName) {
        if ($scope.sortType == propertyName) {
            $scope.sortReverse = !$scope.sortReverse;//backtodefault
        }
        $scope.sortType = propertyName;
    };

    $scope.showOverlay = false;

    $scope.confirmHistoryTypeDeletion = function (item) {
        $scope.SelectedHistoryType = item;
        if (!$scope.showDeleteScreen) {
            $scope.showDeleteScreen = !$scope.showDeleteScreen;
            $scope.showOverlay = !$scope.showOverlay;
        }
    }

    $scope.cancelHistoryTypeDeletion = function (item) {
        if ($scope.showDeleteScreen) {
            $scope.showDeleteScreen = !$scope.showDeleteScreen;
            $scope.showOverlay = !$scope.showOverlay;
        }
    }

    $scope.deleteHistoryType = function (item) {
        if ($scope.showDeleteScreen) {
            $scope.showDeleteScreen = !$scope.showDeleteScreen;
            $scope.showOverlay = !$scope.showOverlay;
        }
        ngNewtonFactory.deleteHistoryType($scope.SelectedHistoryType.Id)
            .success(function () {
                $scope.HistoryTypeList.splice($scope.getIndex($scope.HistoryTypeList, $scope.SelectedHistoryType), 1);
            })
    }

    $scope.getIndex = function (roles, r) {
        for (var i = 0; i < roles.length; i++) {
            if (r.Id == roles[i].Id && r.Name == roles[i].Name) {
                return i;
            }
        }
    }
});