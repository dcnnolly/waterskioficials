﻿$(document).ready(function () {
    var search = getParameterByName('search');
    $('#customersDropDownList').val(search).attr("selected", "selected")
})

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^#]*)||#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

//Basic info CONTACT validation
$("#basicInfoNext").click(function () {
    $('#generalInfoForm .col-sm-7').removeClass('has-error');
    $('#generalInfoForm .col-sm-7').removeClass('has-success');
    if (!$('#customersDropDownList').val())
        $('.col-sm-7:has(#customersDropDownList)').addClass('has-error');
    else
        $('.col-sm-7:has(#customersDropDownList)').addClass('has-success');
    if (!($("#nameTextBox").val()))
        $('.col-sm-7:has(#nameTextBox)').addClass('has-error');
    else
        $('.col-sm-7:has(#nameTextBox)').addClass('has-success');
    $('.col-sm-7:has(#lastNameTextBox)').addClass('has-success');
    if (!validateEmail(($("#emailAdressTextBox").val())))
        $('.col-sm-7:has(#emailAdressTextBox)').addClass('has-error');
    else
        $('.col-sm-7:has(#emailAdressTextBox)').addClass('has-success');
    if (!$("#altEmailAdressTextBox").val())
        $('.col-sm-7:has(#altEmailAdressTextBox)').addClass('has-success');
    else {
        if (!validateEmail(($("#altEmailAdressTextBox").val())))
            $('.col-sm-7:has(#altEmailAdressTextBox)').addClass('has-error');
        else
            $('.col-sm-7:has(#altEmailAdressTextBox)').addClass('has-success');
    }
    if (!$("#phoneTextBox").val())
        $('.col-sm-7:has(#phoneTextBox)').addClass('has-success');
    else {
        if (!$.isNumeric($("#phoneTextBox").val()))
            $('.col-sm-7:has(#phoneTextBox)').addClass('has-error');
        else
            $('.col-sm-7:has(#phoneTextBox)').addClass('has-success');
    }
    if (!$("#mobileTextBox").val())
        $('.col-sm-7:has(#mobileTextBox)').addClass('has-success');
    else {
        if (!$.isNumeric($("#mobileTextBox").val()))
            $('.col-sm-7:has(#mobileTextBox)').addClass('has-error');
        else 
            $('.col-sm-7:has(#mobileTextBox)').addClass('has-success');
    }
    if (!$('#generalInfoForm .col-sm-7').hasClass('has-error')) {
        $("#generalInfoForm").css("display", "none");
        $("#rolesForm").css("display", "inline-block");
    }
})
$("#nameTextBox").focus(function () {}).blur(function () {
    if (!($("#nameTextBox").val())) {
        $('.col-sm-7:has(#nameTextBox)').removeClass('has-success');
        $('.col-sm-7:has(#nameTextBox)').addClass('has-error');
    }
    else
        $('.col-sm-7:has(#nameTextBox)').addClass('has-success');
});
$("#customersDropDownList").focus(function () {}).blur(function () {
    if (!$('#customersDropDownList').val()) {
        $('.col-sm-7:has(#customersDropDownList)').removeClass('has-success');
        $('.col-sm-7:has(#customersDropDownList)').addClass('has-error');
    }
    else
        $('.col-sm-7:has(#customersDropDownList)').addClass('has-success');
});
$("#lastNameTextBox").focus(function () {}).blur(function () {
    $('.col-sm-7:has(#lastNameTextBox)').addClass('has-success');
});
$("#emailAdressTextBox").focus(function () {}).blur(function () {
    if (!validateEmail(($("#emailAdressTextBox").val()))) {
        $('.col-sm-7:has(#emailAdressTextBox)').removeClass('has-success');
        $('.col-sm-7:has(#emailAdressTextBox)').addClass('has-error');
    }
    else
        $('.col-sm-7:has(#emailAdressTextBox)').addClass('has-success');
});
$("#altEmailAdressTextBox").focus(function () {}).blur(function () {
    if (!$("#altEmailAdressTextBox").val())
        $('.col-sm-7:has(#altEmailAdressTextBox)').addClass('has-success');
    else {
        if (!validateEmail(($("#altEmailAdressTextBox").val()))) {
            $('.col-sm-7:has(#altEmailAdressTextBox)').removeClass('has-success');
            $('.col-sm-7:has(#altEmailAdressTextBox)').addClass('has-error');
        }
        else
            $('.col-sm-7:has(#altEmailAdressTextBox)').addClass('has-success');
    }
});
$("#phoneTextBox").focus(function () {}).blur(function () {
    if (!$("#phoneTextBox").val())
        $('.col-sm-7:has(#phoneTextBox)').addClass('has-success');
    else {
        if (!$.isNumeric($("#phoneTextBox").val())) {
            $('.col-sm-7:has(#phoneTextBox)').removeClass('has-success');
            $('.col-sm-7:has(#phoneTextBox)').addClass('has-error');
        }
        else 
            $('.col-sm-7:has(#phoneTextBox)').addClass('has-success');
    }
});
$("#mobileTextBox").focus(function () {}).blur(function () {
    if (!$("#mobileTextBox").val())
        $('.col-sm-7:has(#mobileTextBox)').addClass('has-success');
    else {
        if (!$.isNumeric($("#mobileTextBox").val())) {
            $('.col-sm-7:has(#mobileTextBox)').removeClass('has-success');
            $('.col-sm-7:has(#mobileTextBox)').addClass('has-error');
        }
        else
            $('.col-sm-7:has(#mobileTextBox)').addClass('has-success');
    }
});

//Basic info SYSTEM CONTACT validation
$("#basicInfoSystemNext").click(function () {
    $('#generalInfoForm .col-sm-7').removeClass('has-error');
    $('#generalInfoForm .col-sm-7').removeClass('has-success');
    if (!$('#customersDropDownList').val())
        $('.col-sm-7:has(#customersDropDownList)').addClass('has-error');
    else
        $('.col-sm-7:has(#customersDropDownList)').addClass('has-success');
    if (!($("#nameTextBox").val()))
        $('.col-sm-7:has(#nameTextBox)').addClass('has-error');
    else
        $('.col-sm-7:has(#nameTextBox)').addClass('has-success');
    if (!validateEmail(($("#emailAdressTextBox").val())))
        $('.col-sm-7:has(#emailAdressTextBox)').addClass('has-error');
    else
        $('.col-sm-7:has(#emailAdressTextBox)').addClass('has-success');
    if (!$('#generalInfoForm .col-sm-7').hasClass('has-error')) {
        $("#generalInfoForm").css("display", "none");
        $("#notificationsForm").css("display", "inline-block");
    }
})

//Create wizard slides moves
$("#rolesNext").click(function () {
    $("#rolesForm").css("display", "none");
    $("#notificationsForm").css("display", "inline-block");
})
$("#rolesBack").click(function () {
    $("#rolesForm").css("display", "none");
    $("#generalInfoForm").css("display", "inline-block");
})
$("#notificationsBack").click(function () {
    $("#notificationsForm").css("display", "none");
    $("#rolesForm").css("display", "inline-block");
})

$("#notificationsSystemBack").click(function () {
    $("#notificationsForm").css("display", "none");
    $("#generalInfoForm").css("display", "inline-block");
})

//Primary checkbox disable/enable
$("#rolesTable").on("click", "input[name='PrimaryRoleId']", function () {



    if ($(this).parent().find(".selectedCheckbox").is(':checked')) {
        $("#rolesForm").find(".primaryCheckbox").each(function () {
            if ($(this).prop('checked') == true) {
                $(this).prop('checked', false);
            }
        });
        $(this).prop('checked', true);
    } else {
        $(this).attr('checked', false);
        alert("Primary role must be one of selected roles!")
    }

                    //if ($(":checkbox[name='PrimaryRoleId']").is(":checked")) {
                    //    $("#rolesForm").find(".primaryCheckbox").each(function () {
                    //        if ($(this).prop('checked') == false) {
                    //            $(this).attr("disabled", true);
                    //        }
                    //    });
                    //} else {
                    //    $(":checkbox[name='PrimaryRoleId']").removeAttr("disabled");
                    //}
})

$("#rolesTable").on("click", "input[name='selectedChks']", function () {
    if ($(this).parent().find(".primaryCheckbox").is(':checked')) {
        alert("Remove primary role first.")
        return false;
    }
})

//System contact activate checkbox
$("#systemContactCheckbox").click(function () {
    $('#generalInfoForm .col-sm-7').removeClass('has-error');
    $('#generalInfoForm .col-sm-7').removeClass('has-success');
    if ($(this).is(':checked')) {
        $(".contactOnly").css('display', 'none')
        $("#basicInfoSystemNextContainer").css('display', 'initial');
        $("#notificationsSystemBack").css('display', 'initial');
    } else {
        $(".contactOnly").css('display', 'block');
        $("input.contactOnly").css('display', 'inline');
        $("#basicInfoSystemNextContainer").css('display', 'none');
        $("#notificationsSystemBack").css('display', 'none');
    }
})