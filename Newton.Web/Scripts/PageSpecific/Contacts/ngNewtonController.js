﻿
app.controller('ngNewtonCtrl', function ($scope, ngNewtonFactory, $sce) {

    //---------------------------------------------------------------------GLOBAL (CONTACTS)

    if (!localStorage.getItem("searchToken"))
        $scope.searchText = '';
    else
        $scope.searchText = localStorage.getItem("searchToken");

    if (!window.location.hash) {
        window.location.hash = "GeneralInfo";
    }
    else {
        document.getElementById(window.location.hash).className = "active";
    }

    $scope.changeHash = function (name) {
        window.location.hash = name;
        if ($scope.SelectedContactId) {
            $scope.loadCustomer($scope.SelectedContactId);
        }
    }

    $scope.showOverlay = false;

    $scope.showAddRoles = false;
    $scope.closeAllPopups = function () {
        $scope.showOverlay = false;
        //list your windows here
        $scope.showAddRoles = false;
        $scope.showEditScreen = false;
        $scope.showDeleteScreen = false;
        $scope.showContactDeletePopup = false;
        $scope.advancedSearchShowAddRoles = false;
        $scope.showAdvancedSearch = false;
    }

    $scope.$watch('searchText', function () {
        //this is loading first time
        if ($scope.searchText.length > 1) {
            $scope.submitAdvancedSearch($scope.searchText, $scope.Contact.Filters.SelectedRoles);
        }
        else {
            $scope.showOverlay = false;
            $scope.screenLoaded = true;
        }
    });

    $scope.$watch('filteredContacts.length', function () {
        if ($scope.filteredContacts) {
            if ($scope.filteredContacts[0] != undefined) {
                $scope.SelectedContactId = $scope.filteredContacts[0].Id;
                $scope.loadCustomer($scope.SelectedContactId);
            }
            else {
                $scope.frmContactDetails.$setPristine();
                $scope.SelectedContact = {};
            }
        }
    });

    $scope.Contact = [];
    //---------------------ADVANCED SEARCH

    $scope.Contact.Filters = [];
    $scope.Contact.Filters.AdvancedSearchUsed = null;
    $scope.Contact.Filters.AllRoles = [];
    $scope.Contact.Filters.SelectedRoles = [];

    $scope.submitAdvancedSearch = function (searchText, roles) {
        var advancedFilters = {};
        advancedFilters.searchText = searchText.length == 0 ? ' ' : searchText;
            advancedFilters.roleIds = extractOnePropertyFromList(roles, 'Id');
        ngNewtonFactory.getContactsAdvanced(advancedFilters).success(function (response) {
            $scope.ContactList = response;
            if (searchText != '' && response.length > 0) {
                localStorage.setItem("searchToken", searchText);
            }
            if (roles.length != 0) {
                $scope.Contact.Filters.AdvancedSearchUsed = (response.length > 0);
            }
            else {
                $scope.Contact.Filters.AdvancedSearchUsed = null;
            }
            $scope.screenLoaded = true;
        });
        $scope.closeAllPopups();
    }

    function extractOnePropertyFromList(list, propertyName) {
        var newList = [];
        for (var p = 0; p < list.length; p++) {
            newList.push((list[p])[propertyName]);
        }
        return newList;
    }

    $scope.showAdvancedSearch = false;
    $scope.initialiseAdvancedSearch = function (clear) {
        if (clear) {
            $scope.closeAllPopups();
            $scope.Contact.Filters.AdvancedSearchUsed = null;
            loadRolesForAdvancedSearch();
            //$scope.Contact.Filters.AvailableNotifications = angular.copy($scope.Contact.Filters.BackupAvailableNotifications);
            $scope.Contact.Filters.SelectedNotifications = [];
            $scope.submitAdvancedSearch($scope.searchText, $scope.Contact.Filters.SelectedRoles);
        }
        else {
            $scope.showOverlay = true;
            $scope.showAdvancedSearch = true;
            if ($scope.Contact.Filters.AllRoles.length == 0
                && $scope.Contact.Filters.SelectedRoles.length == 0) {
                loadRolesForAdvancedSearch();
            }
            //if ($scope.Contact.Filters.SelectedNotifications.length == 0
            //    && $scope.Contact.Filters.AvailableNotifications.length == 0) {
            //    $scope.Contact.Filters.AvailableNotifications = angular.copy($scope.Contact.Filters.BackupAvailableNotifications);
            //    $scope.Contact.Filters.SelectedNotifications = [];
            //}
        }
    }

    function loadRolesForAdvancedSearch() {
        $scope.Contact.Filters.AllRoles = [];
        $scope.Contact.Filters.SelectedRoles = [];
        ngNewtonFactory.getAllRoles()
            .success(function (response) {
                $scope.Contact.Filters.AllRoles = response;
            });
    }
    $scope.advancedSearchAddSelectedRole = function (role) {
        role.IsSelected = !role.IsSelected;
        $scope.Contact.Filters.SelectedRoles.push(role);
        $scope.Contact.Filters.AllRoles.splice($scope.getIndex($scope.Contact.Filters.AllRoles, role), 1);
    }
    $scope.advancedSearchRemoveSelectedRole = function (role) {
        role.IsSelected = !role.IsSelected;
        $scope.Contact.Filters.SelectedRoles.splice($scope.getIndex($scope.Contact.Filters.SelectedRoles, role), 1);
        $scope.Contact.Filters.AllRoles.push(role);
    }

    $scope.advancedSearchAddSelectedNotification = function (notification) {
        notification.IsSelected = !notification.IsSelected;
        $scope.Contact.Filters.SelectedNotifications.push(notification);
        //$scope.Contact.Filters.AvailableNotifications.splice($scope.getIndex($scope.Contact.Filters.AvailableNotifications, notification), 1);
    }
    $scope.advancedSearchRemoveSelectedNotification = function (notification) {
        notification.IsSelected = !notification.IsSelected;
        $scope.Contact.Filters.SelectedNotifications.splice($scope.getIndex($scope.Contact.Filters.SelectedNotifications, notification), 1);
        //$scope.Contact.Filters.AvailableNotifications.push(notification);
    }
    //---------------------------------------------------------------------CONTACT DETAILS
    $scope.sortType = ['CustomerName', 'FullName'];
    $scope.sortBy = function (propertyName) {
        if ($scope.sortType[0] == propertyName) {
            $scope.sortReverse = !$scope.sortReverse;//backtodefault
        }
        else {
            $scope.sortType[2] = $scope.sortType[1];
            $scope.sortType[1] = $scope.sortType[0];
            $scope.sortType[0] = propertyName;
        }
    };

    $scope.historySortType = ['CreatorName', 'TypeName', 'SubTypeName'];
    $scope.historySortBy = function (propertyName) {
        if ($scope.historySortType[0] == propertyName) {
            $scope.historyReverse = !$scope.historyReverse;//backtodefault
        }
        else {
            $scope.historySortType[2] = $scope.historySortType[1];
            $scope.historySortType[1] = $scope.historySortType[0];
            $scope.historySortType[0] = propertyName;
        }
    };

    $scope.reportingSortType = 'Name';
    $scope.sortReverse = false;//latest at the top
    $scope.historyReverse = false;//latest at the top
    $scope.reportingReverse = false;//latest at the top

    $scope.showActivate = false;

    $scope.showAccountsInvoicesOptions = false;

    $scope.ContactList = [];
    $scope.SelectedContactId = null;
    $scope.CreateContact = function (contactId) {
        ngNewtonFactory.getCustomerNumber(contactId)
            .success(function (response) {
                window.location = 'Contacts/Create?search=' + encodeURIComponent(response);
            })
    }

    //LOAD contact information
    $scope.SelectedContact = {};
    $scope.SelectedCustomer = [];
    //MAIN FUNCTION, LOAD ANY INFORMATION RELATED TO THE CUSTOMER/CONTACT DEPENDING ON SELECTIONS
    $scope.loadCustomer = function (id) {
        $scope.SelectedContactId = id;
        loadContact(id);
        switch (window.location.hash) {
            case "#GeneralInfo":
                $scope.frmContactDetails.$setPristine();
                loadRolesForContact(id);
                break;
            case "#Notifications":
                ngNewtonFactory.getContactData(id)
                    .success(function (response) {
                        $scope.SelectedContact = response;
                        $scope.checkForInvoicesOptions();
                    });
                break;
            case "#CustInfo":
                ngNewtonFactory.getCustomerData(id)
                    .success(function (response) {
                        $scope.SelectedCustomer = response;
                    });
                break;
            case "#WebLogins":
                for (i = 0; i < 2; i++) {
                    callLoginsService(i, $scope.filteredContacts[$scope.getIndex($scope.filteredContacts, { Id: $scope.SelectedContactId })].Email);
                }
                break;
            case "#History":
                $scope.History.Filters.SelectedTypes = ['Comment', 'Notification', 'History', 'Contact', 'Email'];
                loadHistory($scope.SelectedContactId);
                break;
            case "#Reporting":
                loadReports(id);
                break;
        }
    }

    $scope.checkForInvoicesOptions = function() {
        if ($scope.SelectedContact.ReceiveOwnInvoices == true || $scope.SelectedContact.ReceiveAllInvoices == true) {
            $scope.showAccountsInvoicesOptions = true;
        } else {
            $scope.showAccountsInvoicesOptions = false;
        }
    }

    function callLoginsService(i, email) {
        //email = email.replace(/ /g,'')
        var url = $("#indexHeader").attr('data-url');
        ngNewtonFactory.callLoginsService(url, i, email, 5)
            .success(function (response) {
                InitiateLoginButtons(i, response);
                $scope.WebLogins = response;
            });
    }

    function loadContact(id) {
        $scope.SelectedContact = {};
        ngNewtonFactory.getContactData(id)
            .success(function (response) {
                $scope.SelectedContact = response;
            });
    }

    function loadRolesForContact(id) {
        $scope.SelectedRoles = [];
        if ($scope.filteredContacts.length != 0) {
            ngNewtonFactory.getContactRoles(id)
                .success(function (response) {
                    $scope.SelectedRoles = response.m_Item1;
                    $scope.RemainingRoles = response.m_Item2;
                });
        }
        else {
            $scope.SelectedRoles = [];
        }
    }

    function loadRegions() {
        $scope.RegionList = [];
            ngNewtonFactory.getRegions()
                .success(function (response) {
                    $scope.RegionList = response;
                });
    }

    function loadCountries() {
        $scope.CountryList = [];
            ngNewtonFactory.getCountries()
                .success(function (response) {
                    $scope.CountryList = response;
                });
    }

    loadRegions();
    loadCountries();

    updateContactRole = function (role) {
        ngNewtonFactory.updateRole($scope.SelectedContact.Id, role.Id)
            .success(function (response) {
            });
    }
    $scope.addSelectedRole = function (role) {
        role.IsSelected = !role.IsSelected;
        updateContactRole(role);
        $scope.SelectedRoles.push(role);
        $scope.RemainingRoles.splice($scope.getIndex($scope.RemainingRoles, role), 1);
    }
    $scope.removeSelectedRole = function (role) {
        role.IsSelected = !role.IsSelected;
        updateContactRole(role);
        $scope.SelectedRoles.splice($scope.getIndex($scope.SelectedRoles, role), 1);
        $scope.RemainingRoles.push(role);
    }

    //created for roles but can be used for other collections
    $scope.getIndex = function (roles, r) {
        for (var i = 0; i < roles.length; i++) {
            if (r.Id == roles[i].Id && r.Name == roles[i].Name) {
                return i;
            }
        }
        return -1;
    }

    $scope.UpdateContactDetails = function (contact) {
        if ($scope.frmContactDetails.$valid || $scope.frmContactDetails.$error.required == undefined) {
            ngNewtonFactory.postUpdateContact(contact)
                .success(function (result) {

                    $scope.submitAdvancedSearch($scope.searchText, $scope.Contact.Filters.SelectedRoles);

                    //$scope.ContactList[$scope.getIndex($scope.ContactList, contact)].FullName = (contact.FirstName == null ? '' : contact.FirstName) + (contact.LastName == null ? '' : ' ' + contact.LastName);
                    //$scope.ContactList[$scope.getIndex($scope.ContactList, contact)].Email = contact.Email;
                    //$scope.ContactList[$scope.getIndex($scope.ContactList, contact)].Phone = contact.Phone;
                    //$scope.ContactList[$scope.getIndex($scope.ContactList, contact)].Mobile = contact.Mobile;
                    //$scope.ContactList[$scope.getIndex($scope.ContactList, contact)].Region =
                    //    $.grep($scope.RegionList, function (r) { return r.Id == contact.RegionId; })[0].Name;
                    //$scope.ContactList[$scope.getIndex($scope.ContactList, contact)].Country =
                    //    $.grep($scope.CountryList, function (r) { return r.Id == contact.CountryId; })[0].Name;

                    $scope.frmContactDetails.$setPristine();
                    loadRolesForContact(result);
                })
                .error(function () {
                    alert('Error, please try again later');
                });
        }
        else {
            angular.forEach($scope.frmContactDetails.$error.required, function (field) {
                field.$setDirty();
            });
        }
    }
    
    $scope.showContactDeletePopup = false;
    $scope.confirmContactDeletion = function (item) {
        if (!$scope.showContactDeletePopup) {
            $scope.showContactDeletePopup = !$scope.showContactDeletePopup;
            $scope.showOverlay = !$scope.showOverlay;
        }
    }

    $scope.CallDeleteContact = function (contact) {
        ngNewtonFactory.postDeleteContact(contact.Id)
            .success(function () {
                $scope.ContactList.splice($scope.getIndex($scope.ContactList, contact), 1);
                $scope.closeAllPopups();
            })
    }

    $scope.UpdateNotifications = function (contact) {
        ngNewtonFactory.postUpdateContact(contact);
    }
    $scope.UpdateCustomerSpecific = function (customer) {
        ngNewtonFactory.postUpdateCustomer(customer);
    }
    //-----------------------------------------------------WEB LOGINS
    $scope.CreateAccount = function (site) {
        var url = $("#indexHeader").attr('data-url');
        ngNewtonFactory.callLoginsService(url, site, $scope.filteredContacts[$scope.getIndex($scope.filteredContacts, { Id: $scope.SelectedContactId })].Email, 0)
            .success(function (response) {
                CreateAccountJQuery(response, site);
            })
            .error(function () {
                alert('Error, please try again later');
            });
    }
    $scope.DeleteAccount = function (site) {
        var url = $("#indexHeader").attr('data-url');
        ngNewtonFactory.callLoginsService(url, site, $scope.filteredContacts[$scope.getIndex($scope.filteredContacts, { Id: $scope.SelectedContactId })].Email, 1)
            .success(function (response) {
                DeleteAccountJQuery(response, site);
            })
            .error(function () {
                alert('Error, please try again later');
            });
    }
    $scope.SuspendAccount = function (site) {
        var url = $("#indexHeader").attr('data-url');
        ngNewtonFactory.callLoginsService(url, site, $scope.filteredContacts[$scope.getIndex($scope.filteredContacts, { Id: $scope.SelectedContactId })].Email, 2)
            .success(function (response) {
                SuspendAccountJQuery(response, site);
            })
            .error(function () {
                alert('Error, please try again later');
            });
    }
    $scope.ActivateAccount = function (site) {
        var url = $("#indexHeader").attr('data-url');
        ngNewtonFactory.callLoginsService(url, site, $scope.filteredContacts[$scope.getIndex($scope.filteredContacts, { Id: $scope.SelectedContactId })].Email, 3)
            .success(function (response) {
                ActivateAccountJQuery(response, site);
            })
            .error(function () {
                alert('Error, please try again later');
            });
    }
    $scope.ResetPassword = function (site) {
        var url = $("#indexHeader").attr('data-url');
        ngNewtonFactory.callLoginsService(url, site, $scope.filteredContacts[$scope.getIndex($scope.filteredContacts, { Id: $scope.SelectedContactId })].Email, 4)
            .success(function (response) {
                ResetPasswordJQuery(response, site);
            })
            .error(function () {
                alert('Error, please try again later');
            });
    }
    //-------------------------------------------------------------------------HISTORY
    $scope.History = [];
    $scope.History.NewItem = {};
    $scope.History.Items = [];
    $scope.History.Filters = [];//show all on startup
    $scope.History.NewItem.DateCreated = null;

    $scope.createHistoryItem = function () {
        $scope.History.NewItem.DateCreated = new Date();
        $scope.History.NewItem.DateCreated.setSeconds(0, 0);
        $scope.showCreateHistory = !$scope.showCreateHistory;
    }

    function loadHistory(contactId) {
        ngNewtonFactory.getHistoryData(contactId)
            .success(function (response) {
                $scope.History.Items = response;
            })
            .finally(function () {
                $scope.showOverlay = false;
            });

        ngNewtonFactory.getAllHistorySubTypes(contactId)
            .success(function (response) {
                $scope.History.AllSubTypes = response;

                if ($.grep($scope.History.AllSubTypes, function (r) { return r.Name == 'Admin Comment'; }).length > 0) {
                    $scope.showAdminFilters = true;
                }

            });
    }

    ContainsHistoryType = function (items, type) {
        for (var t = 0; t < items.length; t++) {
            if (items[t].Id == type.TypeId) {
                return false;
            }
        }
        return true;
    }

    $scope.filterSubtypes = function (item) {
        return (item.TypeId == $scope.History.NewItem.HistoryTypeId);
    }

    $scope.History.Filters.Types = ['Comment', 'Notification', 'History', 'Contact', 'Email'];
    $scope.History.Filters.SelectedTypes = ['Comment', 'Notification', 'History', 'Contact', 'Email'];
    $scope.History.Filters.IsContactSelected = true;
    $scope.historyFilterFunction = function (item) {
        return (($scope.History.Filters.SelectedTypes.indexOf(item.TypeName) > -1) || ($scope.adminCommentChecked && item.SubTypeName == 'Admin Comment')
            && ($scope.History.Filters.IsContactSelected == false || ($scope.History.Filters.IsContactSelected == true && $scope.SelectedContactId == item.ContactId)));
    }

    $scope.changeTypeFilter = function (typeName) {
        if ($scope.History.Filters.SelectedTypes.indexOf(typeName) > -1) {
            $scope.History.Filters.SelectedTypes
                .splice($scope.History.Filters.SelectedTypes.indexOf(typeName), 1);
        }
        else {
            $scope.History.Filters.SelectedTypes.push(typeName);
        }
    }

    $scope.AddHistoryItem = function (historyItem) {
        if ($scope.frmHistoryItem.$valid) {
            historyItem.ContactId = $scope.SelectedContact.Id;
            historyItem.ContactName = ($scope.SelectedContact.FirstName == null ? '' : $scope.SelectedContact.FirstName) + ' ' + ($scope.SelectedContact.LastName == null ? '' : $scope.SelectedContact.LastName);
            
            ngNewtonFactory.postHistoryItem(historyItem)
                .success(function () {
                    loadHistory($scope.SelectedContactId);
                    //clear validation and history item and hide the form
                    $scope.frmHistoryItem.$setPristine();
                    $scope.History.NewItem = {};
                    $scope.History.NewItem.DateCreated = new Date();
                    $scope.showCreateHistory = false;
                })
                .error(function () {
                    alert('Error, please try again later');
                });
        }
        else {
            angular.forEach($scope.frmHistoryItem.$error.required, function (field) {
                field.$setDirty();
            });
        }
    }
    
    $scope.clearUser = function () {
        $scope.showAddButton = !$scope.showAddButton;
        if ($scope.showAddButton) {
            $scope.SelectedContact = {};
            $scope.SelectedContact.Id = 0;
        }
        else {
            $scope.loadCustomer($scope.SelectedContactId);
        }
    }

    $scope.filterTypes = function (item) {
        return ContainsType($scope.Reporting.Items, item);
    }

    ContainsType = function (items, reportType) {
        for (var i = 0; i < items.length; i++) {
            if (items[i].ReportType == reportType.Id) {
                return false;
            }
        }
        return true;
    }

    $scope.GenerateReport = function () {
        var ids = [];
        var data = '';
        if ($scope.filteredContacts.length != 0) {
            for (var i = 0; i < $scope.filteredContacts.length; i++) {
                data += '&contactId=' + $scope.filteredContacts[i].Id
                //ids.push(parseInt($scope.filteredContacts[i].Id));
            }

            $.fileDownload('/reportgenerator/downloadreport', {
                httpMethod: 'POST',
                data: data
            });
        }
        else {
            alert('No contacts selected, please find desired contacts to export');
        }
    }
});