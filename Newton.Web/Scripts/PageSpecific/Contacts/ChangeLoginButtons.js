﻿//Initialize buttons on first attempt
function InitiateLoginButtons(i, response) {
    if (response.Exists && response.IsActive) {
        $("#Login-" + (i + 1)).find(".CurrentStatus").text('Current status: Active');
        $("#Login-" + (i + 1)).find(".Suspend").attr('disabled', false);
        $("#Login-" + (i + 1)).find(".Activate").css('display', 'none');
        $("#Login-" + (i + 1)).find(".Suspend").css('display', 'inline-block');
        $("#Login-" + (i + 1)).find(".Create").css('display', 'none');
        $("#Login-" + (i + 1)).find(".Delete").css('display', 'inline-block');
        $("#Login-" + (i + 1)).find(".Reset").attr('disabled', false);
    } else if (response.Exists && !response.IsActive) {
        $("#Login-" + (i + 1)).find(".CurrentStatus").text('Current status: Suspended');
        $("#Login-" + (i + 1)).find(".Suspend").attr('disabled', false);
        $("#Login-" + (i + 1)).find(".Suspend").css('display', 'none');
        $("#Login-" + (i + 1)).find(".Activate").css('display', 'inline-block');
        $("#Login-" + (i + 1)).find(".Create").css('display', 'none');
        $("#Login-" + (i + 1)).find(".Delete").css('display', 'inline-block');
        $("#Login-" + (i + 1)).find(".Reset").attr('disabled', false);
    } else {
        $("#Login-" + (i + 1)).find(".CurrentStatus").text('Current status: N/A');
        $("#Login-" + (i + 1)).find(".Suspend").attr('disabled', true);
        $("#Login-" + (i + 1)).find(".Delete").css('display', 'none');
        $("#Login-" + (i + 1)).find(".Create").css('display', 'inline-block');
        $("#Login-" + (i + 1)).find(".Suspend").attr('disabled', true);
    }
}

//WEB LOGINS CALLS
function CreateAccountJQuery(response, site) {
    $("#Login-" + (site + 1)).find(".CurrentStatus").text('Current status: Active');
    $("#Login-" + (site + 1)).find(".Suspend").attr('disabled', false);
    $("#Login-" + (site + 1)).find(".Suspend").css('display', 'inline-block');
    $("#Login-" + (site + 1)).find(".Activate").css('display', 'none');
    $("#Login-" + (site + 1)).find(".Create").css('display', 'none');
    $("#Login-" + (site + 1)).find(".Delete").css('display', 'inline-block');
    $("#Login-" + (site + 1)).find(".Reset").attr('disabled', false);
}
function DeleteAccountJQuery(response, site) {
    $("#Login-" + (site + 1)).find(".CurrentStatus").text('Current status: N/A');
    $("#Login-" + (site + 1)).find(".Suspend").attr('disabled', true);
    $("#Login-" + (site + 1)).find(".Suspend").css('display', 'inline-block');
    $("#Login-" + (site + 1)).find(".Activate").css('display', 'none');
    $("#Login-" + (site + 1)).find(".Create").css('display', 'inline-block');
    $("#Login-" + (site + 1)).find(".Delete").css('display', 'none');
    $("#Login-" + (site + 1)).find(".Reset").attr('disabled', true);
}
function SuspendAccountJQuery(response, site) {
    $("#Login-" + (site + 1)).find(".CurrentStatus").text('Current status: Suspended');
    $("#Login-" + (site + 1)).find(".Suspend").css('display', 'none');
    $("#Login-" + (site + 1)).find(".Activate").css('display', 'inline-block');
    $("#Login-" + (site + 1)).find(".Reset").attr('disabled', false);
}
function ActivateAccountJQuery(response, site) {
    $("#Login-" + (site + 1)).find(".CurrentStatus").text('Current status: Active');
    $("#Login-" + (site + 1)).find(".Suspend").css('display', 'inline-block');
    $("#Login-" + (site + 1)).find(".Activate").css('display', 'none');
    $("#Login-" + (site + 1)).find(".Reset").attr('disabled', false);
}
function ResetPasswordJQuery(response, site) {
    $("#Login-" + (site + 1)).find(".Reset").attr('disabled', true);
}