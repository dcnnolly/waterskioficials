﻿
var app = angular.module('ngNewton', ['ngLoadingSpinner']);

app.factory('ngNewtonFactory', function ($http) {
    var myAPI = {};
    myAPI.getContactsAdvanced = function (adv) {
        return $http({
            method: 'POST',
            accept: 'json/application',
            data: JSON.stringify(adv),
            url: '/api/contactapi/advancedSearch/',
        });
    };
    myAPI.getHistoryData = function (contactId) {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/historyapi/allhistory/' + contactId,
        });
    };
    myAPI.getAllHistorySubTypes = function () {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/historyapi/allhistorysubtypes/'
        })
    };
    myAPI.getReportData = function (contactId) {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/contactapi/customerreports/' + contactId,
        });
    };

    myAPI.postUpdateContact = function (contact) {
        return $http({
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(contact),
            url: '/api/contactapi/updatecontactdetails/' + contact.Id,
        });
    };
    //---------------------NEW CODE
    myAPI.postDeleteContact = function (contactId) {
        return $http({
            method: 'POST',
            contentType: 'application/json',
            url: '/api/contactapi/deletecontact/' + contactId,
        });
    };
    //---------------------ENEW CODE
    myAPI.postUpdateCustomer = function (customer) {
        return $http({
            method: 'PUT',
            contentType: 'application/json',
            data: JSON.stringify(customer),
            url: '/api/customerapi/updatecustomer/',
        });
    };
    myAPI.updateRole = function (contactId, roleId) {
        return $http({
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(roleId),
            url: '/api/contactapi/updatecontactrole/' + contactId,
        });
    };
    myAPI.getContactData = function (id) {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/contactapi/contact/' + id,
        });
    };
    myAPI.getContactRoles = function (id) {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/contactapi/roleswithselections/' + id
        });
    };
    myAPI.getCustomerData = function (id) {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/customerapi/getcustomerdata/' + id,
        });
    };
    myAPI.getCustomerNumber = function (id) {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/customerapi/getcustomernumber/' + id,
        });
    };
    myAPI.postHistoryItem = function (historyItem) {
        return $http({
            method: 'POST',
            accept: 'json/application',
            data: JSON.stringify(historyItem),
            url: '/api/historyapi/savehistory/',
        });
    };

    myAPI.getReportTypes = function () {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/reportapi/allreporttypes'
        });
    };
    myAPI.getReportColumns = function (customerNumber) {
        return $http({
            method: 'GET',
            accept: 'json/application',
            params: { CustomerNumber: customerNumber },
            url: '/api/reportapi/allreportcolumns'
        });
    };
    myAPI.getSelectedColumns = function (customerNumber) {
        return $http({
            method: 'GET',
            accept: 'json/application',
            params: { CustomerNumber: customerNumber },
            url: '/api/reportapi/selectedreportcolumns'
        });
    };
    myAPI.getReportFormats = function () {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/reportapi/allreportformats'
        });
    }
    myAPI.postReport = function (report) {
        return $http({
            method: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(report),
            url: '/api/reportapi/savereport'
        })
    };
    myAPI.deleteReport = function (id) {
        return $http({
            method: 'POST',
            contentType: 'application/json',
            url: '/api/reportapi/deletereport/' + id
        })
    };
    myAPI.subscribeReport = function (reportId, contactId) {
        return $http({
            method: 'POST',
            contentType: 'application/json',
            params: { ReportId: reportId, ContactId: contactId },
            url: '/api/reportapi/subscribereport'
        })
    };
    myAPI.getReportColumnsWithSelections = function (reportId, customerNumber, reportType) {
        return $http({
            method: 'GET',
            contentType: 'application/json',
            params: { ReportId: reportId, CustomerNumber: customerNumber, TypeId: reportType },
            url: '/api/reportapi/columnswithselections'
        })
    };
    myAPI.callLoginsService = function (urladdress, site, email, action) {
        return $http({
            method: 'GET',
            contentType: 'application/json',
            url: urladdress + '?site=' + site + '&email=' + email + '&action=' + action
        })
    };
    myAPI.getAllRoles = function () {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/contactapi/getallroles'
        });
    };
    myAPI.getRegions = function () {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/contactapi/getRegions'
        })
    };
    myAPI.getCountries = function () {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/contactapi/getCountries'
        })
    };
    return myAPI;
});