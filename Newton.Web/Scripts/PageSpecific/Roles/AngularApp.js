﻿
var app = angular.module('ngNewton', []);

app.factory('ngNewtonFactory', function ($http) {

    var myAPI = {};

    myAPI.getRoleList = function () {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/roleapi/allroles',
        });
    };

    myAPI.getRoleData = function (roleId) {
        return $http({
            method: 'GET',
            accept: 'json/application',
            url: '/api/roleapi/' + roleId,
        });
    };


    myAPI.deleteRole = function (roleId) {
        return $http({
            method: 'POST',
            accept: 'json/application',
            url: '/api/roleapi/deleterole/' + roleId,
        });
    };

    return myAPI;
});

app.controller('ngNewtonCtrl', function ($scope, ngNewtonFactory) {

    $scope.showDeleteScreen = false;
    $scope.showOverlay = false;

    $scope.deleteRole = function (id) {
        window.location.href = "/Roles/Delete/" + id;
    }

    $scope.showRoleDetails = true;

    window.onload = function () {
        $scope.loadRolesData();
    };

    $scope.SelectedRole = [];
    $scope.RoleList = [];

    $scope.loadRolesData = function () {
        $scope.RoleList = [];

        ngNewtonFactory.getRoleList()
            .success(function (response) {
                $scope.RoleList = response;
            })
            .finally(function (response) {
            });
    }

    $scope.loadRole = function (id) {

        ngNewtonFactory.getContactData(id)
            .success(function (response) {
                $scope.SelectedRole = response;
                $scope.showRoleDetails = true;
            })
            .finally(function (response) {
            });
    }

    $scope.sortType = 'Name';
    $scope.sortReverse = false;//latest at the top
    $scope.sortBy = function (propertyName) {
        if ($scope.sortType == propertyName) {
            $scope.sortReverse = !$scope.sortReverse;//backtodefault
        }
        $scope.sortType = propertyName;
    };

    $scope.confirmRoleDeletion = function (item) {
        $scope.SelectedRole = item;

        if (!$scope.showDeleteScreen) {
            $scope.showDeleteScreen = !$scope.showDeleteScreen;
            $scope.showOverlay = !$scope.showOverlay;
        }
    }

    $scope.cancelRoleDeletion = function (item) {
        if ($scope.showDeleteScreen) {
            $scope.showDeleteScreen = !$scope.showDeleteScreen;
            $scope.showOverlay = !$scope.showOverlay;
        }
    }

    $scope.deleteRole = function (item) {
        if ($scope.showDeleteScreen) {
            $scope.showDeleteScreen = !$scope.showDeleteScreen;
            $scope.showOverlay = !$scope.showOverlay;
        }
        ngNewtonFactory.deleteRole($scope.SelectedRole.Id)
            .success(function () {
                $scope.RoleList.splice($scope.getIndex($scope.RoleList, $scope.SelectedRole), 1);
            })
    }

    $scope.getIndex = function (roles, r) {
        for (var i = 0; i < roles.length; i++) {
            if (r.Id == roles[i].Id && r.Name == roles[i].Name) {
                return i;
            }
        }
    }
});
