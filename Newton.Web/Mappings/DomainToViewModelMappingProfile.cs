﻿using AutoMapper;
using Newton.Domain.Models;
using Newton.Web.DTO;
using Newton.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.Mappings
{
    public class DomainToViewModelMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "DomainToViewModelMapping"; }
        }

        public DomainToViewModelMappingProfile()
        {
            CreateMap<Role, RoleViewModel>();
            CreateMap<HistoryType, HistoryTypeViewModel>();
            CreateMap<HistorySubType, HistorySubTypeViewModel>();
            CreateMap<Contact, ContactListItemDTO>()
                .ForMember(dt => dt.CustomerName, conf => conf.MapFrom(c => ""))
                .ForMember(dt => dt.Country, conf => conf.MapFrom(c => ""))
                .ForMember(dt => dt.Region, conf => conf.MapFrom(c => ""));
            CreateMap<History, HistoryListDTO>()
                .ForMember(dt => dt.SubTypeName, conf => conf.MapFrom(c => c.HistorySubType.Name))
                .ForMember(dt => dt.TypeId, conf => conf.MapFrom(c => c.HistorySubType.HistoryType.Id))
                .ForMember(dt => dt.DateCreated, conf => conf.MapFrom(c => c.DateCreated.ToString("dd/MM/yyyy hh:mmtt")))
                .ForMember(dt => dt.TypeName, conf => conf.MapFrom(c => c.HistorySubType.HistoryType.Name));
            CreateMap<HistorySubType, HistorySubTypesDTO>()
                .ForMember(dt => dt.TypeId, conf => conf.MapFrom(c => c.HistoryType.Id))
                .ForMember(dt => dt.TypeName, conf => conf.MapFrom(c => c.HistoryType.Name))
                .ForMember(dt => dt.Name, conf => conf.MapFrom(c => c.Name));
            CreateMap<HistorySubType, HistorySubTypeDTO>()
                .ForMember(dt => dt.HistoryTypeName, conf => conf.MapFrom(c => c.HistoryType.Name));
            CreateMap<Role, ContactRoleDTO>();
            CreateMap<CustomerInfo, CustomerInfoDTO>();
            CreateMap<Customer, CustomerInfoDTO>();
            CreateMap<ReportColumn, ReportColumnDTO>();
            CreateMap<AvailableReportColumn, ReportColumnDTO>();
        }
    }
}