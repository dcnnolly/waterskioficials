﻿using AutoMapper;
using Newton.Domain.Models;
using Newton.Web.DTO;
using Newton.Web.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.Mappings
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public override string ProfileName
        {
            get { return "ViewModelToDomainMapping"; }
        }

        public ViewModelToDomainMappingProfile()
        {
            CreateMap<RoleViewModel, Role>();
            CreateMap<HistoryTypeViewModel, HistoryType>();
            CreateMap<HistorySubTypeViewModel, HistorySubType>();
            CreateMap<CreateContactViewModel, Contact>();
            CreateMap<NewHistoryItemDTO, History>();
            CreateMap<ReportColumnDTO, ReportColumn>();
            
        }
    }
}