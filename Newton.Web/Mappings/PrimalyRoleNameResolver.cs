﻿using AutoMapper;
using Newton.DAL;
using Newton.Domain.Models;
using Newton.Web.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Newton.Web.Mappings
{
    public class PrimalyRoleNameResolver : IValueResolver<Contact, ContactReportItemDTO, string>
    {
        private WaterSkiContext _context;

        public PrimalyRoleNameResolver(WaterSkiContext context)
        {
            _context = context;
        }
        public string Resolve(Contact source, ContactReportItemDTO destination, string destMember, ResolutionContext context)
        {
            var role = _context.UserRoles.FirstOrDefault(r => r.Id == source.PrimaryRoleId);

            if (role == null)
            {
                return "";
            }

            return role.Name;
        }
    }
}