﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Owin.Hosting;
using Topshelf;
using System.Web.Configuration;
using Newton.Web.Helpers;

namespace Newton.ReportProcessor
{
    class Program
    {
        static void Main(string[] args)
        {
            HostFactory.Run(x =>
            {
                x.Service<ReportProcessor>(s =>
                {
                    s.ConstructUsing(name => new ReportProcessor());
                    s.WhenStarted(tc => tc.Start());
                    s.WhenStopped(tc => tc.Stop());
                });
                x.RunAsLocalSystem();

                x.SetDescription("Report Processing Worker");
                x.SetDisplayName("Report Processing Worker");
                x.SetServiceName("Newton.ReportProcessor");
            });
        }

        private class ReportProcessor
        {
            private IDisposable _host;

            public void Start()
            {
                var endpointUrl = WebConfigurationManager.AppSettings["EndpointUrl"];
                _host = WebApp.Start<Startup>(endpointUrl);

                Console.WriteLine();
                Console.WriteLine("Hangfire Server started");
                Console.WriteLine("Dashboard is available at {0}/hangfire", endpointUrl);
                Console.WriteLine();
            }

            public void Stop()
            {
                _host.Dispose();
            }
        }
    }
}
