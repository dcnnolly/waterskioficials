﻿using Newton.DAL;
using Newton.Domain.Models;
using Newton.ReportProcessor;
using Newton.ReportProcessor.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;

namespace Newton.Web.Helpers
{
    public class ReportProcessorHelper
    {
        private WaterSkiContext context = new WaterSkiContext();

        public async Task Process()
        {
            var invoiceReportEmailFrom = "accounts@mwh.ie";
            var invoiceSubject = String.Format("MicroWarehouse {0} - {1}", ReportType.InvoiceReport.ToString(), DateTime.Now.ToString("dd/MM/yyyy"));
            var invoiceBody = String.Format("Please find attached your {0}. For any queries please contact {1}", ReportType.InvoiceReport.ToString(), invoiceReportEmailFrom);

            var deliveryReportEmailFrom = "logistics@mwh.ie";
            var deliverySubject = String.Format("MicroWarehouse {0} - {1}", ReportType.DeliveryReport.ToString(), DateTime.Now.ToString("dd/MM/yyyy"));
            var deliveryBody = String.Format("Please find attached your {0}. For any queries please contact {1}", ReportType.DeliveryReport.ToString(), deliveryReportEmailFrom);

            var serialNumberReportEmailFrom = "logistics@mwh.ie";
            var serialNumberSubject = String.Format("MicroWarehouse {0} - {1}", ReportType.SerialNumberReport.ToString(), DateTime.Now.ToString("dd/MM/yyyy"));
            var serialNumberBody = String.Format("Please find attached your {0}. For any queries please contact {1}", ReportType.SerialNumberReport.ToString(), serialNumberReportEmailFrom);

            foreach (Report report in getAllScheduledReports())
            {

                foreach (ReportContact rc in report.Contacts) { 

                    string filePath = "";
                    switch (report.ReportType)
                    {
                        case ReportType.InvoiceReport:

                            filePath = CreateInvoiceReport(report);

                            await Task.Run(() =>
                            {
                                SendReportToContact(invoiceSubject, invoiceBody, invoiceReportEmailFrom, WebConfigurationManager.AppSettings["LoginUser"], WebConfigurationManager.AppSettings["LoginPassword"], rc.Contact.Email, filePath, bool.Parse(WebConfigurationManager.AppSettings["TestMode"]));
                            });

                            break;
                        case ReportType.DeliveryReport:

                            filePath = CreateDeliveryReport(report);

                            await Task.Run(() =>
                            {
                                SendReportToContact(deliverySubject, deliveryBody, deliveryReportEmailFrom, WebConfigurationManager.AppSettings["LoginUser"], WebConfigurationManager.AppSettings["LoginPassword"], rc.Contact.Email, filePath, bool.Parse(WebConfigurationManager.AppSettings["TestMode"]));
                            });

                            break;
                        case ReportType.SerialNumberReport:

                            filePath = CreateSerialNumberReport(report);

                            await Task.Run(() =>
                            {
                                SendReportToContact(serialNumberSubject, serialNumberBody, serialNumberReportEmailFrom, WebConfigurationManager.AppSettings["LoginUser"], WebConfigurationManager.AppSettings["LoginPassword"], rc.Contact.Email, filePath, bool.Parse(WebConfigurationManager.AppSettings["TestMode"]));
                            });

                            break;
                    }

                    rc.LastProcessed = DateTime.Now;
                    context.SaveChanges();

                    HistorySubType hst = getHistorySubTypeForReport(report.ReportType);
                    AddHistoryRecord(rc.Contact, hst);

                    if (context.SaveChanges() > 0)
                    {
                        if (File.Exists(filePath))
                        {
                            File.Delete(filePath);
                        }
                    }

                }

            }

        }

        private List<Report> getAllScheduledReports()
        {
            DateTime currentDate = DateTime.Now.Date;
            DateTime currentDateTime = DateTime.Now;
            int currentDayOfWeek = (int)currentDateTime.DayOfWeek;
            DateTime startOfMonthDateTime = currentDateTime.AddDays(-(DateTime.Now.Day + 1)).Date;
            DateTime startOfWeekDateTime = currentDateTime.Date;
            while (startOfWeekDateTime.DayOfWeek != DayOfWeek.Monday) { startOfWeekDateTime = startOfWeekDateTime.AddDays(-1); }

            var result = context.Reports.Include("Contacts").Include("Contacts.Contact").Include("ReportColumns").Where(r =>
                    r.Contacts.Count > 0 &&
                    (
                        (r.ReportScheduleFrequency == ReportScheduleFrequency.Daily && r.ExecutionTime.Value.Hour == currentDateTime.Hour) && (r.Contacts.Any(cr => !cr.LastProcessed.HasValue || cr.LastProcessed.Value.Day != currentDate.Day)) ||
                        (r.ReportScheduleFrequency == ReportScheduleFrequency.Weekly && r.ExecutionDayOfWeek.Value == currentDayOfWeek) && (r.Contacts.Any(cr => !cr.LastProcessed.HasValue || cr.LastProcessed.Value.Day != currentDate.Day)) ||
                        (r.ReportScheduleFrequency == ReportScheduleFrequency.Monthly && r.ExecutionDay.Value.Day == currentDateTime.Day) && (r.Contacts.Any(cr => !cr.LastProcessed.HasValue || cr.LastProcessed.Value.Month != currentDate.Month)) ||
                        (r.ReportScheduleFrequency == ReportScheduleFrequency.Hourly && (r.ScheduleStartTime.Value.Hour <= currentDateTime.Hour && r.ScheduleEndTime.Value.Hour >= currentDateTime.Hour))
                        && (r.Contacts.Any(cr => !cr.LastProcessed.HasValue || cr.LastProcessed.Value.Hour != currentDateTime.Hour))
                    )
                ).ToList();

            return result;
        }

        private HistorySubType getHistorySubTypeForReport(ReportType type) {

            HistoryType notificationHistoryType = context.HistoryTypes.Include("HistorySubTypes").Where(ht => ht.Name == "Notification").FirstOrDefault();
            int settingReportType = int.Parse(WebConfigurationManager.AppSettings[type.ToString() + "_SubTypeId"]);
            HistorySubType resultSubType = context.HistorySubTypes.Where(hst => hst.Id == settingReportType).FirstOrDefault();
            
            return resultSubType;
        }

        private void AddHistoryRecord(Contact contact, HistorySubType hst)
        {
            context.HistoryItems.Add(new History()
            {
                CustomerNumber = contact.CustomerNumber,
                ContactId = contact.Id,
                ContactName = contact.FirstName.Trim() + " " + contact.LastName.Trim(),
                CreatorName = "NewtonAdmin",
                DateCreated = DateTime.Now,
                HistorySubTypeId = hst.Id,
                Detail = hst.Name.ToString() + " report was sent"
            });
            context.SaveChanges();
        }

        private string CreateInvoiceReport(Report report)
        {
            DateTime CurrentDateValue = DateTime.Now;
            DateTime DefaultDateValue = DateTime.Parse("2000/01/01");

            string filePath = null;
            if (report != null)
            {

                var selectedColumnNames = report.ReportColumns.Where(rc => rc.IsSelected == true).OrderBy(rc => rc.OrderInReport).Select(rc => rc.ColumnName).ToList();
                var selectedNames = report.ReportColumns.Where(rc => rc.IsSelected == true).OrderBy(rc => rc.OrderInReport).Select(rc => rc.Name).ToList();

                List<InvoiceReportDataItem> reportData = null;

                if (report.Consolidate)
                {
                    var parentCustomerNumber = context.CustomerInfoItems.First(x => x.CustomerNumber == report.CustomerNumber).Parent;
                    reportData = context.InvoiceReportDataItems.Where(rdi => rdi.PARENT == parentCustomerNumber && DateTime.Compare(rdi.DATEREPORTED, DefaultDateValue) < 1).ToList();
                }
                else
                {
                    reportData = context.InvoiceReportDataItems.Where(rdi => rdi.CUSTNMBR == report.CustomerNumber && DateTime.Compare(rdi.DATEREPORTED, DefaultDateValue) < 1).ToList();
                }

                reportData.ForEach(cir => cir.DATEREPORTED = DateTime.Now);
                reportData.ForEach(cir => cir.REPORTSTATUS = "REPORTED");
                filePath = GenerateReport(report, selectedColumnNames, selectedNames, reportData);
            }
            
            return filePath;
        }

        private string CreateDeliveryReport(Report report)
        {
            DateTime CurrentDateValue = DateTime.Now;
            DateTime DefaultDateValue = DateTime.Parse("2000/01/01");

            string filePath = null;
            if (report != null)
            {

                var selectedColumnNames = report.ReportColumns.Where(rc => rc.IsSelected == true).OrderBy(rc => rc.OrderInReport).Select(rc => rc.ColumnName).ToList();
                var selectedNames = report.ReportColumns.Where(rc => rc.IsSelected == true).OrderBy(rc => rc.OrderInReport).Select(rc => rc.Name).ToList();
                List<DeliveryReportDataItem> reportData = null;
                
                if (report.Consolidate)
                {
                    var parentCustomerNumber = context.CustomerInfoItems.First(x => x.CustomerNumber == report.CustomerNumber).Parent;
                    reportData = context.DeliveryReportDataItems.Where(rdi => rdi.PARENT == parentCustomerNumber && DateTime.Compare(rdi.DATEREPORTED, DefaultDateValue) < 1).ToList();
                }
                else
                {
                    reportData = context.DeliveryReportDataItems.Where(rdi => rdi.CUSTNMBR == report.CustomerNumber && DateTime.Compare(rdi.DATEREPORTED, DefaultDateValue) < 1).ToList();
                }

                reportData.ForEach(cir => cir.DATEREPORTED = DateTime.Now);
                reportData.ForEach(cir => cir.REPORTSTATUS = "REPORTED");
                filePath = GenerateReport(report, selectedColumnNames, selectedNames, reportData);
            }

            return filePath;
        }

        private string CreateSerialNumberReport(Report report)
        {
            DateTime CurrentDateValue = DateTime.Now;
            DateTime DefaultDateValue = DateTime.Parse("2000/01/01");

            string filePath = null;
            if (report != null)
            {

                var selectedColumnNames = report.ReportColumns.Where(rc => rc.IsSelected == true).OrderBy(rc => rc.OrderInReport).Select(rc => rc.ColumnName).ToList();
                var selectedNames = report.ReportColumns.Where(rc => rc.IsSelected == true).OrderBy(rc => rc.OrderInReport).Select(rc => rc.Name).ToList();

                List<SerialNumberReportDataItem> reportData = null;

                if (report.Consolidate)
                {
                    var parentCustomerNumber = context.CustomerInfoItems.First(x => x.CustomerNumber == report.CustomerNumber).Parent;
                    reportData = context.SerialNumberReportDataItems.Where(rdi => rdi.PARENT == parentCustomerNumber && DateTime.Compare(rdi.DATEREPORTED, DefaultDateValue) < 1).ToList();
                }
                else
                {
                    reportData = context.SerialNumberReportDataItems.Where(rdi => rdi.CUSTNMBR == report.CustomerNumber && DateTime.Compare(rdi.DATEREPORTED, DefaultDateValue) < 1).ToList();
                }

                reportData.ForEach(cir => cir.DATEREPORTED = DateTime.Now);
                reportData.ForEach(cir => cir.REPORTSTATUS = "REPORTED");
                filePath = GenerateReport(report, selectedColumnNames, selectedNames, reportData);
            }

            return filePath;
        }

        private string GenerateReport<T>(Report report, List<string> selectedColumnNames, List<string> selectedNames, List<T> reports)
        {
            string extension = "";
            string joinChar = " ";
            if (report.ReportFormat == ReportFormat.text)
            {
                extension = ".txt";
                joinChar = "\t";
            }
            else if (report.ReportFormat == ReportFormat.csv)
            {
                extension = ".csv";
                joinChar = ",";
            }

            string filePath = Path.Combine(Path.GetTempPath(), "report_" + report.ReportType.ToString() + "_" + Guid.NewGuid() + extension);
            using (var stream = File.Create(filePath)) { 
                using (var streamWriter = new StreamWriter(stream))
                {
                    var contents = new StringBuilder();
                    if (report.IncludeHeaders)
                    {
                        var headerLine = String.Join(joinChar, selectedNames);

                        contents.AppendLine(headerLine);
                    }

                    foreach (var invoiceReport in reports)
                    {
                        var type = invoiceReport.GetType();
                        var dataLineArray = new List<string>();
                        foreach (var selectedColumnName in selectedColumnNames)
                        {
                            var value = type.GetProperty(selectedColumnName).GetValue(invoiceReport) != null ? type.GetProperty(selectedColumnName).GetValue(invoiceReport).ToString() : "";
                            dataLineArray.Add(value);
                        }
                        contents.AppendLine(String.Join(joinChar, dataLineArray));
                    }
                    streamWriter.Write(contents.ToString());
                }
            }

            return filePath;
        }

        private void SendReportToContact(string subject, string body,string emailFrom, string loginUser, string loginPassword, string emailTo, string filePath, bool testMode)
        {
            if (testMode)
            {
                body = String.Format("The message is meant for {0}", emailTo) + Environment.NewLine + body;
                emailTo = WebConfigurationManager.AppSettings["TestEmailTo"];

            }

            var emailService = new EmailService();
            emailService.SendEmail(subject, body, filePath, emailFrom, emailTo, "", "smtp.office365.com", 587, loginUser, loginPassword);
            
        }
        
    }
}