﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Newton.ReportProcessor.Extensions
{
    internal static class RuntimeTypeBuilder
    {
        private static readonly ModuleBuilder moduleBuilder;
        private static readonly IDictionary<string, Type> builtTypes;

        static RuntimeTypeBuilder()
        {
            var assemblyName = new AssemblyName { Name = "DynamicLinqTypes" };
            moduleBuilder = Thread.GetDomain()
                    .DefineDynamicAssembly(assemblyName, AssemblyBuilderAccess.Run)
                    .DefineDynamicModule(assemblyName.Name);
            builtTypes = new Dictionary<string, Type>();
        }

        internal static Type GetRuntimeType(IDictionary<string, PropertyInfo> fields)
        {
            var typeKey = GetTypeKey(fields);
            if (!builtTypes.ContainsKey(typeKey))
            {
                lock (moduleBuilder)
                {
                    builtTypes[typeKey] = GetRuntimeType(typeKey, fields);
                }
            }

            return builtTypes[typeKey];
        }

        private static Type GetRuntimeType(string typeName, IEnumerable<KeyValuePair<string, PropertyInfo>> properties)
        {
            var typeBuilder = moduleBuilder.DefineType(typeName, TypeAttributes.Public | TypeAttributes.Class | TypeAttributes.Serializable);
            foreach (var property in properties)
            {
                typeBuilder.DefineField(property.Key, property.Value.PropertyType, FieldAttributes.Public);
            }

            return typeBuilder.CreateType();
        }

        private static string GetTypeKey(IEnumerable<KeyValuePair<string, PropertyInfo>> fields)
        {
            return fields.Aggregate(string.Empty, (current, field) => current + (field.Key + ";" + field.Value.Name + ";"));
        }

        public static Type GetDynamicType(IEnumerable<KeyValuePair<string, PropertyInfo>> fields)
        {
            if (null == fields)
                throw new ArgumentNullException("fields");
            if (fields.Count() == 0)
                throw new ArgumentOutOfRangeException("fields", "fields must have at least 1 field definition");

            try
            {
                Monitor.Enter(builtTypes);
                string className = GetTypeKey(fields);

                if (builtTypes.ContainsKey(className))
                    return builtTypes[className];

                TypeBuilder typeBuilder = moduleBuilder.DefineType(className, TypeAttributes.Public | TypeAttributes.Class | TypeAttributes.Serializable);

                foreach (var field in fields)
                    typeBuilder.DefineField(field.Key, field.Value.PropertyType, FieldAttributes.Public);

                builtTypes[className] = typeBuilder.CreateType();

                return builtTypes[className];
            }
            catch (Exception ex)
            {
                // logging maybe
            }
            finally
            {
                Monitor.Exit(builtTypes);
            }

            return null;
        }

        //public static Type GetDynamicType(IEnumerable<KeyValuePair<string, PropertyInfo>> fields)
        //{
        //    return GetDynamicType(fields.ToDictionary(f => f.Name, f => f.PropertyType));
        //}
    }
}
