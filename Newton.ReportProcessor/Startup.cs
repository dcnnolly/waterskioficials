﻿using System;
using Hangfire.SqlServer;
using Microsoft.Owin;
using Owin;
using Hangfire;
using Newton.Web.Helpers;
using Newton.ReportProcessor.Filters;

[assembly: OwinStartup(typeof(Newton.ReportProcessor.Startup))]

namespace Newton.ReportProcessor
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage(
                "Newton.HangfireContext",
                new SqlServerStorageOptions { QueuePollInterval = TimeSpan.FromSeconds(1) });

            app.UseHangfireDashboard();
            //"/hangfire", new DashboardOptions
            //{
            //    AuthorizationFilters = new[] { new NewtonHangfireAuthorizationFilter() }
            //}
            app.UseHangfireServer();

            ReportProcessorHelper processor = new ReportProcessorHelper();

            RecurringJob.AddOrUpdate(
                () => processor.Process(),
                "0/60 5-19 * * *");
            //RecurringJobManager manager = new RecurringJobManager();
            //manager.AddOrUpdate()
        }
    }
}
