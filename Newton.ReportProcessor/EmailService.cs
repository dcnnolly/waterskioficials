﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace Newton.ReportProcessor
{
    public class EmailService
    {
        public void SendEmail(string subject, string body, string attachmentPath, string from, string to, string sendCopyTo, string smtpServer, int smtpPort, string loginUser, string loginPassword)
        {
            MailMessage message = new MailMessage(from, to);
            message.Subject = subject;
            message.Body = body;
            message.IsBodyHtml = true;
            message.Sender = new MailAddress(from);

            if (sendCopyTo != "")
            {
                message.CC.Add(sendCopyTo);
            }

            if (attachmentPath != null)
            {
                Attachment attachment = new Attachment(attachmentPath, MediaTypeNames.Application.Octet);
                message.Attachments.Add(attachment);
            }

            SmtpClient client = new SmtpClient(smtpServer, smtpPort);
            client.Credentials = new System.Net.NetworkCredential(loginUser, loginPassword);
            client.EnableSsl = true;

            client.Send(message);

            message.Attachments.ToList().ForEach(a => a.Dispose());
        }
    }
}
