﻿using Newton.DAL.Mapping;
using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;

namespace Newton.DAL
{
    public class NewtonInitializer : CreateDatabaseIfNotExists<WaterSkiContext>
    {
        protected override void Seed(WaterSkiContext context)
        {
            GetAllRoles().ForEach(ro => context.Set<Role>().Add(ro));
            context.SaveChanges();
            GetAllHistoryTypes().ForEach(ht => context.Set<HistoryType>().Add(ht));
            context.SaveChanges();
            GetAllHistorySubTypes().ForEach(hst => context.Set<HistorySubType>().Add(hst));
            context.SaveChanges();
            //var customers = context.Customers.Take(5).ToList();
            //GetAllCustomerInfoItems(customers).ForEach(cit => context.Set<CustomerInfo>().Add(cit));
            //context.SaveChanges();

            AddSomeCountries().ForEach(hst => context.Set<Country>().Add(hst));
            context.SaveChanges();
            AddSomeRegions().ForEach(hst => context.Set<Region>().Add(hst));
            context.SaveChanges();

            context.Contacts.Add(new Contact()
            {
                //CustomerNumber = customers[0].CustomerNumber,
                Email = "mindaugas@coronica.ie",
                FirstName = "Mindaugas",
                LastName = "Lee",
                Mobile = "+370 656 365665",
                Phone = "545555444544",
                CountryId = 1,
                RegionId = 1
            });
            context.Contacts.Add(new Contact()
            {
                //CustomerNumber = customers[0].CustomerNumber,
                Email = "martynas@coronica.ie",
                FirstName = "Martynas",
                LastName = "Zee",
                Mobile = "+370 656 365665",
                Phone = "545555444544",
                CountryId = 2,
                RegionId = 2
            });
            context.Contacts.Add(new Contact()
            {
                //CustomerNumber = customers[0].CustomerNumber,
                Email = "deividas@coronica.ie",
                FirstName = "Deividas",
                LastName = "Dee",
                Mobile = "+370 656 365665",
                Phone = "545555444544",
                CountryId = 3,
                RegionId = 3
            });
            context.SaveChanges();
            context.Database.ExecuteSqlCommand("INSERT INTO [Newton].[dbo].[Contacts] (FirstName,LastName,Email,AlternateEmail,Phone,Mobile,IsSystemContact,AppearInListOrderConfirmations,FirstInListOrderConfirmations,ReceiveAllOrderConfirmations, ReceiveOwnInvoices, ReceiveAllInvoices, ReceiveStatements,ReceiveShipConfirmations,ReceiveAllShipConfirmations,OptOutMarketing,ReceiveSpecials,ReceiveNewsletters,ReceiveTraining,CustomerNumber,PrimaryRoleId) SELECT NULLIF(FIRSTNAME,''),NULLIF(LASTNAME,''),NULLIF(EMAIL1,''),NULLIF(EMAIL2,''),NULLIF(WRKPHONE,''),NULLIF(MOBILEPH,''),0,1,1,0,0,1,0,1,1,0,1,0,0,CUSTNMBR,1 FROM [MWH].[dbo].[mwh_contact] WHERE RTRIM(LTRIM(CUSTNMBR)) IN (SELECT CustomerNumber FROM [Newton].[dbo].[Customers])");
            GetAllHistoryItems().ForEach(hi => context.Set<History>().Add(hi));
            context.SaveChanges();
            context.Database.ExecuteSqlCommand("INSERT INTO ContactRoles (ContactId, RoleId) VALUES (1,1),(1,2),(1,3)");
            //GetAllReports(customers).ForEach(re => context.Set<Report>().Add(re));
            //context.SaveChanges();
            //context.Database.ExecuteSqlCommand("INSERT INTO ReportContacts (ContactId, ReportId) VALUES (1,1),(1,2),(1,3)");
            //GetAllReportColumns(customers).ForEach(rc => context.Set<ReportColumn>().Add(rc));
            //context.SaveChanges();
            GetAllAvailableReportColumns().ForEach(arc => context.Set<AvailableReportColumn>().Add(arc));
            context.SaveChanges();

        }

        public static List<Country> AddSomeCountries()
        {
            var countries = new List<Country>();

            countries.Add(new Country()
            {
                Name = "Africa",
            });

            countries.Add(new Country()
            {
                Name = "USA"
            });

            countries.Add(new Country()
            {
                Name = "Lithuania"
            });

            return countries;
        }

        public static List<Region> AddSomeRegions()
        {
            var regions = new List<Region>();

            regions.Add(new Region()
            {
                Name = "Australia"
            });

            regions.Add(new Region()
            {
                Name = "Europe"
            });

            regions.Add(new Region()
            {
                Name = "Asia"
            });

            return regions;
        }

        public static List<Role> GetAllRoles()
        {
            var roles = new List<Role>()
            {
                new Role()
                {
                    Name = "Director – Managing",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Director – Sales",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Director – Operations",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Director – Finance",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Director – IT",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Manager – Sales",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Manager – Operations",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Manager – Logistics",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Manager – Finance",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Manager – Accounts Receivable",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Manager – Accounts Payable",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Manager – Purchasing",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Manager – Training",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Manager – Technical",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Sales Executive",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Sales Support",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Accounts Receivable",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Accounts Payable",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Purchasing",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Logistics",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Warehouse",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Warehouse – Goods Receivable",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Warehouse – Dispatch",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Technical Support",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Technical Training",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "Receptionist",
                    IsUnique = false
                },

                new Role()
                {
                    Name = "FromAthena-MD",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "FromAthena-Purchasing",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "FromAthena-Accounts",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "FromAthena-Sales",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "FromAthena-Technical",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "FromAthena-Director",
                    IsUnique = false
                },
                new Role()
                {
                    Name = "FromAthena-Other",
                    IsUnique = false
                },
            };
            return roles;
        }

        public static List<HistoryType> GetAllHistoryTypes()
        {
            var historyTypes = new List<HistoryType>()
            {
                new HistoryType()
                {
                    Name = "Comment"
                },
                new HistoryType()
                {
                    Name = "Contact"
                },
                new HistoryType()
                {
                    Name = "Notification"
                },
                new HistoryType()
                {
                    Name = "History"
                },
                new HistoryType()
                {
                    Name = "Email"
                },
            };

            return historyTypes;
        }

        public static List<HistorySubType> GetAllHistorySubTypes()
        {
            var historySubTypes = new List<HistorySubType>()
            {
                new HistorySubType()
                {
                    Name = "Warning",
                    HistoryTypeId = 1
                },
                new HistorySubType()
                {
                    Name = "Informational",
                    HistoryTypeId = 1
                },
                new HistorySubType()
                {
                    Name = "Training Session",
                    HistoryTypeId = 1
                },
                new HistorySubType()
                {
                    Name = "Admin Comment",
                    HistoryTypeId = 1
                },
                new HistorySubType()
                {
                    Name = "Accounts Contact",
                    HistoryTypeId = 2
                },
                new HistorySubType()
                {
                    Name = "Phone Call",
                    HistoryTypeId = 2
                },
                new HistorySubType()
                {
                    Name = "Meeting",
                    HistoryTypeId = 2
                },
                new HistorySubType()
                {
                    Name = "Training Session",
                    HistoryTypeId = 2
                },
                new HistorySubType()
                {
                    Name = "Email",
                    HistoryTypeId = 5
                },
                new HistorySubType()
                {
                    Name = "Visit",
                    HistoryTypeId = 5
                },
                new HistorySubType()
                {
                    Name = "Contact Change",
                    HistoryTypeId = 4
                },
                new HistorySubType()
                {
                    Name = "Informational",
                    HistoryTypeId = 4
                },
                new HistorySubType()
                {
                    Name = "Email",
                    HistoryTypeId = 4
                },
                new HistorySubType()
                {
                    Name = "Order Confirmation",
                    HistoryTypeId = 3
                },
                new HistorySubType()
                {
                    Name = "Phone Call",
                    HistoryTypeId = 3
                },
                new HistorySubType()
                {
                    Name = "Delivery Notification",
                    HistoryTypeId = 3
                },
                new HistorySubType()
                {
                    Name = "Marketing",
                    HistoryTypeId = 3
                },
                new HistorySubType()
                {
                    Name = "Invoice",
                    HistoryTypeId = 3
                },
                new HistorySubType()
                {
                    Name = "Statement",
                    HistoryTypeId = 3
                },
                new HistorySubType()
                {
                    Name = "Report - Invoices",
                    HistoryTypeId = 3
                },
                new HistorySubType()
                {
                    Name = "Report - Delivery",
                    HistoryTypeId = 3
                },
                new HistorySubType()
                {
                    Name = "Report - Serial Numbers",
                    HistoryTypeId = 3
                }
            };
            return historySubTypes;
        }
        
        public static List<CustomerInfo> GetAllCustomerInfoItems(List<Customer> customers)
        {
            var customerInfoItems = new List<CustomerInfo>()
            {
                new CustomerInfo()
                {
                    CustomerNumber = customers[0].CustomerNumber,
                    AppleDEPId = "4555554",
                    AppleResellerId = "555574454",
                    MicrosoftMPNId = "5756742584",
                    Parent = "123"
                },
                new CustomerInfo()
                {
                    CustomerNumber = customers[1].CustomerNumber,
                    AppleDEPId = "4555554",
                    AppleResellerId = "555574454",
                    MicrosoftMPNId = "5756742584",
                    Parent = "123"
                },
                new CustomerInfo()
                {
                    CustomerNumber = customers[2].CustomerNumber,
                    AppleDEPId = "4555554",
                    AppleResellerId = "555574454",
                    MicrosoftMPNId = "5756742584",
                    Parent = "123"
                },
                new CustomerInfo()
                {
                    CustomerNumber = customers[3].CustomerNumber,
                    AppleDEPId = "4555554",
                    AppleResellerId = "555574454",
                    MicrosoftMPNId = "5756742584",
                    Parent = "123"
                },
                new CustomerInfo()
                {
                    CustomerNumber = customers[4].CustomerNumber,
                    AppleDEPId = "4555554",
                    AppleResellerId = "555574454",
                    MicrosoftMPNId = "5756742584",
                    Parent = "123"
                },
            };
            return customerInfoItems;
        }

        public static List<History> GetAllHistoryItems()
        {
            var historyItems = new List<History>()
            {
                new History()
                {
                    ContactId = 1,
                    CreatorName = "Martynas",
                    ContactName = "John",
                    DateCreated = DateTime.Now,
                    HistorySubTypeId = 1,
                    Detail = "History sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 2,
                    CreatorName = "Mindaugas",
                    ContactName = "Linda",
                    DateCreated = DateTime.Now.AddDays(-1),
                    HistorySubTypeId = 2,
                    Detail = "Reporting sketch",
                    CustomerNumber= "LASERELEC      "

                },
                new History()
                {
                    ContactId = 3,
                    CreatorName = "Deividas",
                    ContactName = "Greg",
                    DateCreated = DateTime.Now.AddDays(-2),
                    HistorySubTypeId = 3,
                    Detail = "Notification sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 1,
                    CreatorName = "Martynas",
                    ContactName = "John",
                    DateCreated = DateTime.Now,
                    HistorySubTypeId = 4,
                    Detail = "History sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 2,
                    CreatorName = "Mindaugas",
                    ContactName = "Linda",
                    DateCreated = DateTime.Now.AddDays(-1),
                    HistorySubTypeId = 5,
                    Detail = "Reporting sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 3,
                    CreatorName = "Deividas",
                    ContactName = "Greg",
                    DateCreated = DateTime.Now.AddDays(-2),
                    HistorySubTypeId = 6,
                    Detail = "Notification sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 1,
                    CreatorName = "Martynas",
                    ContactName = "John",
                    DateCreated = DateTime.Now,
                    HistorySubTypeId = 1,
                    Detail = "History sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 2,
                    CreatorName = "Mindaugas",
                    ContactName = "Linda",
                    DateCreated = DateTime.Now.AddDays(-1),
                    HistorySubTypeId = 2,
                    Detail = "Reporting sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 3,
                    CreatorName = "Deividas",
                    ContactName = "Greg",
                    DateCreated = DateTime.Now.AddDays(-2),
                    HistorySubTypeId = 3,
                    Detail = "Notification sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 1,
                    CreatorName = "Martynas",
                    ContactName = "John",
                    DateCreated = DateTime.Now,
                    HistorySubTypeId = 4,
                    Detail = "History sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 2,
                    CreatorName = "Mindaugas",
                    ContactName = "Linda",
                    DateCreated = DateTime.Now.AddDays(-1),
                    HistorySubTypeId = 5,
                    Detail = "Reporting sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 3,
                    CreatorName = "Deividas",
                    ContactName = "Greg",
                    DateCreated = DateTime.Now.AddDays(-2),
                    HistorySubTypeId = 6,
                    Detail = "Notification sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 1,
                    CreatorName = "Martynas",
                    ContactName = "John",
                    DateCreated = DateTime.Now,
                    HistorySubTypeId = 1,
                    Detail = "History sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 2,
                    CreatorName = "Mindaugas",
                    ContactName = "Linda",
                    DateCreated = DateTime.Now.AddDays(-1),
                    HistorySubTypeId = 2,
                    Detail = "Reporting sketch",
                    CustomerNumber= "LASERELEC      "
                },
                new History()
                {
                    ContactId = 3,
                    CreatorName = "Deividas",
                    ContactName = "Greg",
                    DateCreated = DateTime.Now.AddDays(-2),
                    HistorySubTypeId = 3,
                    Detail = "Notification sketch",
                    CustomerNumber= "LASERELEC      "
                },
            };
            return historyItems;
        }

        public static List<Report> GetAllReports(List<Customer> customers)
        {
            var reports = new List<Report>()
            {

                new Report()
                {
                    CustomerNumber = customers[0].CustomerNumber,
                    Name = "Invoice Report",
                    ReportFormat = ReportFormat.csv,
                    ReportScheduleFrequency = ReportScheduleFrequency.Daily,
                    ReportType = ReportType.InvoiceReport,
                    ExecutionTime = DateTime.Parse("2016/05/06 12:00"),
                    IncludeHeaders = true,
                    Consolidate = true
                },
                new Report()
                {
                    CustomerNumber = customers[0].CustomerNumber,
                    Name = "Delivery Report",
                    ReportFormat = ReportFormat.text,
                    ReportScheduleFrequency = ReportScheduleFrequency.Hourly,
                    ReportType = ReportType.DeliveryReport,
                    ScheduleStartTime = DateTime.Parse("2016/05/06 14:00:00"),
                    ScheduleEndTime = DateTime.Parse("2016/05/06 18:00:00"),
                    IncludeHeaders = true,
                    Consolidate = false
                },
                new Report()
                {
                    CustomerNumber = customers[0].CustomerNumber,
                    Name = "Serial Number Report",
                    ReportFormat = ReportFormat.csv,
                    ReportScheduleFrequency = ReportScheduleFrequency.Monthly,
                    ReportType = ReportType.SerialNumberReport,
                    ExecutionDay = DateTime.Parse("2016/05/06"),
                    IncludeHeaders = true,
                    Consolidate = false
                },
                new Report()
                {
                    CustomerNumber = customers[1].CustomerNumber,
                    Name = "Invoice Report",
                    ReportFormat = ReportFormat.csv,
                    ReportScheduleFrequency = ReportScheduleFrequency.Daily,
                    ReportType = ReportType.InvoiceReport,
                    ExecutionTime = DateTime.Parse("2016/05/06 10:00"),
                    IncludeHeaders = true,
                    Consolidate = false
                },
                new Report()
                {
                    CustomerNumber = customers[1].CustomerNumber,
                    Name = "Delivery Report",
                    ReportFormat = ReportFormat.csv,
                    ReportScheduleFrequency = ReportScheduleFrequency.Daily,
                    ReportType = ReportType.DeliveryReport,
                    ExecutionTime = DateTime.Parse("2016/05/06 08:00"),
                    IncludeHeaders = true,
                    Consolidate = false
                }
            };
            return reports;
        }

        public static List<ReportColumn> GetAllReportColumns(List<Customer> customers)
        {
            var reportColumns = new List<ReportColumn>()
            {
                //----------------------------Report ID 1
                new ReportColumn()
                {
                    Name = "Report Status",
                    ColumnName = "REPORTSTATUS",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "New, Reported, History",
                    ReportId = 1,
                    IsSelected = false,
                    OrderInReport = 1
                },
                new ReportColumn()
                {
                    Name = "Date Added",
                    ColumnName = "DATEADDED",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Date Added to Newton",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 2
                },
                new ReportColumn()
                {
                    Name = "Date Reported",
                    ColumnName = "DATEREPORTED",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Date Reported",
                    ReportId = 1,
                    IsSelected = false,
                    OrderInReport = 3
                },
                new ReportColumn()
                {
                    Name = "Document Type",
                    ColumnName = "DOCTYPE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Document Type",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 4
                },
                new ReportColumn()
                {
                    Name = "Invoice Number",
                    ColumnName = "INVNUMBE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Invoice/Credit Note Number",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 5
                },
                new ReportColumn()
                {
                    Name = "Line Item Sequence",
                    ColumnName = "LNITMSEQ",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Line Item Sequence",
                    ReportId = 1,
                    IsSelected = false,
                    OrderInReport = 6
                },
                new ReportColumn()
                {
                    Name = "MWH Sales Order",
                    ColumnName = "ORIGNUMB",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Original (Sales Order) Number",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 7
                },
                new ReportColumn()
                {
                    Name = "MWHBASEID",
                    ColumnName = "MWHBASEID",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 1,
                    IsSelected = false,
                    OrderInReport = 8
                },
                new ReportColumn()
                {
                    Name = "Document Date",
                    ColumnName = "DOCDATE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Document Date",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 9
                },
                new ReportColumn()
                {
                    Name = "Customer Number",
                    ColumnName = "CUSTNMBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Customer Number",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 10
                },
                new ReportColumn()
                {
                    Name = "Customer Name",
                    ColumnName = "CUSTNAME",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Customer Name",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 11
                },
                new ReportColumn()
                {
                    Name = "Customer PO Number",
                    ColumnName = "CSTPONBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Customer PO Number",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 12
                },
                new ReportColumn()
                {
                    Name = "End User PO Number",
                    ColumnName = "EUSERPONUM",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Customer PO Number",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 13
                },
                new ReportColumn()
                {
                    Name = "Currency",
                    ColumnName = "CURNCYID",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Currency",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 14
                },
                new ReportColumn()
                {
                    Name = "Exchange Rate",
                    ColumnName = "EXCHRATE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Exchange Rate",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 15
                },
                new ReportColumn()
                {
                    Name = "MWH Salesperson",
                    ColumnName = "SLPRSNID",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "MicroWarehouse Salesperson",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 16
                },
                new ReportColumn()
                {
                    Name = "Item Number",
                    ColumnName = "ITEMNMBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Item Number",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 17
                },
                new ReportColumn()
                {
                    Name = "Item Description",
                    ColumnName = "ITEMDESC",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Item Description",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 18
                },
                new ReportColumn()
                {
                    Name = "Vendor Name",
                    ColumnName = "ITMSHNAM",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Vendor Name",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 19
                },
                new ReportColumn()
                {
                    Name = "Category",
                    ColumnName = "CATEGORY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Category",
                    ReportId = 1,
                    IsSelected = false,
                    OrderInReport = 20
                },
                new ReportColumn()
                {
                    Name = "Sub Category",
                    ColumnName = "SUBCATEGORY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Sub Category",
                    ReportId = 1,
                    IsSelected = false,
                    OrderInReport = 21
                },
                new ReportColumn()
                {
                    Name = "Quantity",
                    ColumnName = "QUANTITY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Quantity",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 22
                },
                new ReportColumn()
                {
                    Name = "Unit Price in Euro",
                    ColumnName = "UNITPRICE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Unit Price in Euro",
                    ReportId = 1,
                    IsSelected = false,
                    OrderInReport = 23
                },
                new ReportColumn()
                {
                    Name = "Price",
                    ColumnName = "O_UNITPRICE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Unit Price in Currency",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 24
                },
                new ReportColumn()
                {
                    Name = "EXTDPRICE",
                    ColumnName = "EXTDPRICE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Total Line Price in Euro",
                    ReportId = 1,
                    IsSelected = false,
                    OrderInReport = 25
                },
                new ReportColumn()
                {
                    Name = "Total",
                    ColumnName = "O_EXTDPRICE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Total Line Price in Currency",
                    ReportId = 1,
                    IsSelected = true,
                    OrderInReport = 26
                },
                //----------------------------Report ID 2
                new ReportColumn()
                {
                    Name = "Report Status",
                    ColumnName = "REPORTSTATUS",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "New, Reported, History",
                    ReportId = 2,
                    IsSelected = false,
                    OrderInReport = 1
                },
                new ReportColumn()
                {
                    Name = "Date Added",
                    ColumnName = "DATEADDED",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Date Added to Newton",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 2
                },
                new ReportColumn()
                {
                    Name = "Date Reported",
                    ColumnName = "DATEREPORTED",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Date Reported",
                    ReportId = 2,
                    IsSelected = false,
                    OrderInReport = 3
                },
                new ReportColumn()
                {
                    Name = "Docket Number",
                    ColumnName = "DNOTENUM",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 4
                },
                new ReportColumn()
                {
                    Name = "Create Date",
                    ColumnName = "CREATDAT",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 5
                },
                new ReportColumn()
                {
                    Name = "Created By",
                    ColumnName = "CREATEBY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = false,
                    OrderInReport = 6
                },
                new ReportColumn()
                {
                    Name = "Check Date",
                    ColumnName = "CHECKDAT",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = false,
                    OrderInReport = 7
                },
                new ReportColumn()
                {
                    Name = "Checked By",
                    ColumnName = "CHECKBY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = false,
                    OrderInReport = 8
                },
                new ReportColumn()
                {
                    Name = "Invoice Number",
                    ColumnName = "INVNUMBE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 9
                },
                new ReportColumn()
                {
                    Name = "Ordered By",
                    ColumnName = "ORDRDBY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = false,
                    OrderInReport = 10
                },
                new ReportColumn()
                {
                    Name = "Order Date",
                    ColumnName = "ORDRDATE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = false,
                    OrderInReport = 11
                },
                new ReportColumn()
                {
                    Name = "Sales Order Number",
                    ColumnName = "SOPNUMBE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 12
                },
                new ReportColumn()
                {
                    Name = "Customer Number",
                    ColumnName = "CUSTNMBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 13
                },
                new ReportColumn()
                {
                    Name = "Customer Name",
                    ColumnName = "CUSTNAME",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 14
                },
                new ReportColumn()
                {
                    Name = "Customer PO Number",
                    ColumnName = "CSTPONBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 15
                },
                new ReportColumn()
                {
                    Name = "End User PO Number",
                    ColumnName = "EUSERPONUM",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 16
                },
                new ReportColumn()
                {
                    Name = "Contact Person",
                    ColumnName = "CNTCPRSN",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 17
                },
                new ReportColumn()
                {
                    Name = "Sales Person",
                    ColumnName = "SLPRSNID",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 18
                },
                new ReportColumn()
                {
                    Name = "Ship Address 1",
                    ColumnName = "SHIPADD1",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 19
                },
                new ReportColumn()
                {
                    Name = "Ship Address 2",
                    ColumnName = "SHIPADD2",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 20
                },
                new ReportColumn()
                {
                    Name = "Ship Address 3",
                    ColumnName = "SHIPADD3",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 21
                },
                new ReportColumn()
                {
                    Name = "Ship Address 4",
                    ColumnName = "SHIPADD4",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 22
                },
                new ReportColumn()
                {
                    Name = "Ship Address 5",
                    ColumnName = "SHIPADD5",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 22
                },
                new ReportColumn()
                {
                    Name = "Ship Address 6",
                    ColumnName = "SHIPADD6",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 23
                },
                new ReportColumn()
                {
                    Name = "Ship Address 7",
                    ColumnName = "SHIPADD7",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 24
                },
                new ReportColumn()
                {
                    Name = "Ship Address 8",
                    ColumnName = "SHIPADD8",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 25
                },
                new ReportColumn()
                {
                    Name = "Ship Address 9",
                    ColumnName = "SHIPADD9",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 26
                },
                new ReportColumn()
                {
                    Name = "Item Line",
                    ColumnName = "LNITMSEQ",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 27
                },
                new ReportColumn()
                {
                    Name = "Vendor",
                    ColumnName = "VENDOR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 28
                },
                new ReportColumn()
                {
                    Name = " Item Number",
                    ColumnName = "ITEMNMBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 29
                },
                new ReportColumn()
                {
                    Name = "Item Description",
                    ColumnName = "ITEMDESC",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 30
                },
                new ReportColumn()
                {
                    Name = "Order Qty",
                    ColumnName = "QUANTITY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 31
                },
                new ReportColumn()
                {
                    Name = "Delivered Qty",
                    ColumnName = "QTYTPICK",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = true,
                    OrderInReport = 32
                },
                new ReportColumn()
                {
                    Name = "Comment 1",
                    ColumnName = "COMMENT1",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = false,
                    OrderInReport = 32
                },
                new ReportColumn()
                {
                    Name = "Comment 2",
                    ColumnName = "COMMENT2",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = false,
                    OrderInReport = 33
                },
                new ReportColumn()
                {
                    Name = "Comment 3",
                    ColumnName = "COMMENT3",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = false,
                    OrderInReport = 34
                },
                new ReportColumn()
                {
                    Name = "Comment 4",
                    ColumnName = "COMMENT4",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 2,
                    IsSelected = false,
                    OrderInReport = 35
                },
                //----------------------------Report ID 3
                new ReportColumn()
                {
                    Name = "REPORTSTATUS",
                    ColumnName = "REPORTSTATUS",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = false,
                    OrderInReport = 1
                },
                new ReportColumn()
                {
                    Name = "Date Added",
                    ColumnName = "DATEADDED",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Date Added to Newton",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 2
                },
                new ReportColumn()
                {
                    Name = "Date Reported",
                    ColumnName = "DATEREPORTED",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Date Reported",
                    ReportId = 3,
                    IsSelected = false,
                    OrderInReport = 3
                },
                new ReportColumn()
                {
                    Name = "DNOTENUM",
                    ColumnName = "DNOTENUM",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 6
                },
                new ReportColumn()
                {
                    Name = "CREATDAT",
                    ColumnName = "CREATDAT",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 7
                },
                new ReportColumn()
                {
                    Name = "Customer Number",
                    ColumnName = "CUSTNMBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 8
                },
                new ReportColumn()
                {
                    Name = "Invoice Number",
                    ColumnName = "INVNUMBE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 9
                },
                new ReportColumn()
                {
                    Name = "Sales Order",
                    ColumnName = "SOPNUMBE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 10
                },
                new ReportColumn()
                {
                    Name = "Customer PO",
                    ColumnName = "CSTPONBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 11
                },
                new ReportColumn()
                {
                    Name = "End User PO",
                    ColumnName = "EUSERPONUM",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 12
                },
                new ReportColumn()
                {
                    Name = "Ship Address",
                    ColumnName = "SHIPADD1",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 13
                },
                new ReportColumn()
                {
                    Name = "Item Number",
                    ColumnName = "ITEMNMBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 14
                },
                new ReportColumn()
                {
                    Name = "Item Description",
                    ColumnName = "ITEMDESC",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 15
                },
                new ReportColumn()
                {
                    Name = "Vendor",
                    ColumnName = "VENDOR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 16
                },
                new ReportColumn()
                {
                    Name = "Qty",
                    ColumnName = "QUANTITY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 17
                },
                new ReportColumn()
                {
                    Name = "Serial Number",
                    ColumnName = "SERIALNUM",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 3,
                    IsSelected = true,
                    OrderInReport = 18
                },
                //----------------------------Report ID 4
                new ReportColumn()
                {
                    Name = "Report Status",
                    ColumnName = "REPORTSTATUS",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "New, Reported, History",
                    ReportId = 4,
                    IsSelected = false,
                    OrderInReport = 1
                },
                new ReportColumn()
                {
                    Name = "Date Added",
                    ColumnName = "DATEADDED",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Date Added to Newton",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 2
                },
                new ReportColumn()
                {
                    Name = "Date Reported",
                    ColumnName = "DATEREPORTED",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Date Reported",
                    ReportId = 4,
                    IsSelected = false,
                    OrderInReport = 3
                },
                new ReportColumn()
                {
                    Name = "Document Type",
                    ColumnName = "DOCTYPE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Document Type",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 4
                },
                new ReportColumn()
                {
                    Name = "Invoice Number",
                    ColumnName = "INVNUMBE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Invoice/Credit Note Number",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 5
                },
                new ReportColumn()
                {
                    Name = "Line Item Sequence",
                    ColumnName = "LNITMSEQ",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Line Item Sequence",
                    ReportId = 4,
                    IsSelected = false,
                    OrderInReport = 6
                },
                new ReportColumn()
                {
                    Name = "MWH Sales Order",
                    ColumnName = "ORIGNUMB",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Original (Sales Order) Number",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 7
                },
                new ReportColumn()
                {
                    Name = "MWHBASEID",
                    ColumnName = "MWHBASEID",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 4,
                    IsSelected = false,
                    OrderInReport = 8
                },
                new ReportColumn()
                {
                    Name = "Document Date",
                    ColumnName = "DOCDATE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Document Date",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 8
                },
                new ReportColumn()
                {
                    Name = "Customer Number",
                    ColumnName = "CUSTNMBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Customer Number",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 9
                },
                new ReportColumn()
                {
                    Name = "Customer Name",
                    ColumnName = "CUSTNAME",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Customer Name",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 10
                },
                new ReportColumn()
                {
                    Name = "Customer PO Number",
                    ColumnName = "CSTPONBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Customer PO Number",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 11
                },
                new ReportColumn()
                {
                    Name = "End User PO Number",
                    ColumnName = "EUSERPONUM",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 12
                },
                new ReportColumn()
                {
                    Name = "Currency",
                    ColumnName = "CURNCYID",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Currency",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 13
                },
                new ReportColumn()
                {
                    Name = "Exchange Rate",
                    ColumnName = "EXCHRATE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Exchange Rate",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 14
                },
                new ReportColumn()
                {
                    Name = "MWH Salesperson",
                    ColumnName = "SLPRSNID",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "MicroWarehouse Salesperson",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 14
                },
                new ReportColumn()
                {
                    Name = "Item Number",
                    ColumnName = "ITEMNMBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Item Number",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 15
                },
                new ReportColumn()
                {
                    Name = "Item Description",
                    ColumnName = "ITEMDESC",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Item Description",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 16
                },
                new ReportColumn()
                {
                    Name = "Vendor Name",
                    ColumnName = "ITMSHNAM",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Vendor Name",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 17
                },
                new ReportColumn()
                {
                    Name = "Category",
                    ColumnName = "CATEGORY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Category",
                    ReportId = 4,
                    IsSelected = false,
                    OrderInReport = 18
                },
                new ReportColumn()
                {
                    Name = "Sub Category",
                    ColumnName = "SUBCATEGORY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Sub Category",
                    ReportId = 4,
                    IsSelected = false,
                    OrderInReport = 19
                },
                new ReportColumn()
                {
                    Name = "Quantity",
                    ColumnName = "QUANTITY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Quantity",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 20
                },
                new ReportColumn()
                {
                    Name = "Unit Price in Euro",
                    ColumnName = "UNITPRICE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Unit Price in Euro",
                    ReportId = 4,
                    IsSelected = false,
                    OrderInReport = 21
                },
                new ReportColumn()
                {
                    Name = "Price",
                    ColumnName = "O_UNITPRICE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Unit Price in Currency",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 22
                },
                new ReportColumn()
                {
                    Name = "EXTDPRICE",
                    ColumnName = "EXTDPRICE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Total Line Price in Euro",
                    ReportId = 4,
                    IsSelected = false,
                    OrderInReport = 23
                },
                new ReportColumn()
                {
                    Name = "Total",
                    ColumnName = "O_EXTDPRICE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Total Line Price in Currency",
                    ReportId = 4,
                    IsSelected = true,
                    OrderInReport = 24
                },
                //----------------------------Report ID 5
                new ReportColumn()
                {
                    Name = "Report Status",
                    ColumnName = "REPORTSTATUS",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "New, Reported, History",
                    ReportId = 5,
                    IsSelected = false,
                    OrderInReport = 1
                },
                new ReportColumn()
                {
                    Name = "Date Added",
                    ColumnName = "DATEADDED",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Date Added to Newton",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 2
                },
                new ReportColumn()
                {
                    Name = "Date Reported",
                    ColumnName = "DATEREPORTED",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "Date Reported",
                    ReportId = 5,
                    IsSelected = false,
                    OrderInReport = 3
                },
                new ReportColumn()
                {
                    Name = "Docket Number",
                    ColumnName = "DNOTENUM",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 4
                },
                new ReportColumn()
                {
                    Name = "Create Date",
                    ColumnName = "CREATDAT",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 5
                },
                new ReportColumn()
                {
                    Name = "Created By",
                    ColumnName = "CREATEBY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = false,
                    OrderInReport = 6
                },
                new ReportColumn()
                {
                    Name = "Check Date",
                    ColumnName = "CHECKDAT",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = false,
                    OrderInReport = 7
                },
                new ReportColumn()
                {
                    Name = "Checked By",
                    ColumnName = "CHECKBY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = false,
                    OrderInReport = 8
                },
                new ReportColumn()
                {
                    Name = "Invoice Number",
                    ColumnName = "INVNUMBE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 9
                },
                new ReportColumn()
                {
                    Name = "Ordered By",
                    ColumnName = "ORDRDBY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = false,
                    OrderInReport = 10
                },
                new ReportColumn()
                {
                    Name = "Order Date",
                    ColumnName = "ORDRDATE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = false,
                    OrderInReport = 11
                },
                new ReportColumn()
                {
                    Name = "Sales Order Number",
                    ColumnName = "SOPNUMBE",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 12
                },
                new ReportColumn()
                {
                    Name = "Customer Number",
                    ColumnName = "CUSTNMBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 13
                },
                new ReportColumn()
                {
                    Name = "Customer Name",
                    ColumnName = "CUSTNAME",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 14
                },
                new ReportColumn()
                {
                    Name = "Customer PO Number",
                    ColumnName = "CSTPONBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 15
                },
                new ReportColumn()
                {
                    Name = "End User PO Number",
                    ColumnName = "EUSERPONUM",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 16
                },
                new ReportColumn()
                {
                    Name = "Contact Person",
                    ColumnName = "CNTCPRSN",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 17
                },
                new ReportColumn()
                {
                    Name = "Sales Person",
                    ColumnName = "SLPRSNID",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 18
                },
                new ReportColumn()
                {
                    Name = "Ship Address 1",
                    ColumnName = "SHIPADD1",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 19
                },
                new ReportColumn()
                {
                    Name = "Ship Address 2",
                    ColumnName = "SHIPADD2",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 20
                },
                new ReportColumn()
                {
                    Name = "Ship Address 3",
                    ColumnName = "SHIPADD3",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 21
                },
                new ReportColumn()
                {
                    Name = "Ship Address 4",
                    ColumnName = "SHIPADD4",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 22
                },
                new ReportColumn()
                {
                    Name = "Ship Address 5",
                    ColumnName = "SHIPADD5",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 22
                },
                new ReportColumn()
                {
                    Name = "Ship Address 6",
                    ColumnName = "SHIPADD6",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 23
                },
                new ReportColumn()
                {
                    Name = "Ship Address 7",
                    ColumnName = "SHIPADD7",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 24
                },
                new ReportColumn()
                {
                    Name = "Ship Address 8",
                    ColumnName = "SHIPADD8",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 25
                },
                new ReportColumn()
                {
                    Name = "Ship Address 9",
                    ColumnName = "SHIPADD9",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 26
                },
                new ReportColumn()
                {
                    Name = "Item Line",
                    ColumnName = "LNITMSEQ",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 27
                },
                new ReportColumn()
                {
                    Name = "Vendor",
                    ColumnName = "VENDOR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 28
                },
                new ReportColumn()
                {
                    Name = " Item Number",
                    ColumnName = "ITEMNMBR",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 29
                },
                new ReportColumn()
                {
                    Name = "Item Description",
                    ColumnName = "ITEMDESC",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 30
                },
                new ReportColumn()
                {
                    Name = "Order Qty",
                    ColumnName = "QUANTITY",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 31
                },
                new ReportColumn()
                {
                    Name = "Delivered Qty",
                    ColumnName = "QTYTPICK",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = true,
                    OrderInReport = 32
                },
                new ReportColumn()
                {
                    Name = "Comment 1",
                    ColumnName = "COMMENT1",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = false,
                    OrderInReport = 32
                },
                new ReportColumn()
                {
                    Name = "Comment 2",
                    ColumnName = "COMMENT2",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = false,
                    OrderInReport = 33
                },
                new ReportColumn()
                {
                    Name = "Comment 3",
                    ColumnName = "COMMENT3",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = false,
                    OrderInReport = 34
                },
                new ReportColumn()
                {
                    Name = "Comment 4",
                    ColumnName = "COMMENT4",
                    CustomerNumber = customers[0].CustomerNumber,
                    Description = "",
                    ReportId = 5,
                    IsSelected = false,
                    OrderInReport = 35
                },
            };
            return reportColumns;
        }

        public static List<AvailableReportColumn> GetAllAvailableReportColumns()
        {
            var availableColumns = new List<AvailableReportColumn>()
            {
                // Invoice report available columns
                new AvailableReportColumn()
                {
                    Name = "REPORTSTATUS",
                    ColumnName = "REPORTSTATUS",
                    Description = "New, Reported, History",
                    IncludeByDefault = false,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "DATEADDED",
                    ColumnName = "DATEADDED",
                    Description = "Date Added to Newton",
                    IncludeByDefault = false,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "DATEREPORTED",
                    ColumnName = "DATEREPORTED",
                    Description = "Date Reported",
                    IncludeByDefault = false,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Document Type",
                    ColumnName = "DOCTYPE",
                    Description = "Document Type",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "INVNUMBE",
                    ColumnName = "INVNUMBE",
                    Description = "Invoice/Credit Note Number",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "LNITMSEQ",
                    ColumnName = "LNITMSEQ",
                    Description = "Line Item Sequence",
                    IncludeByDefault = false,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "MWH Sales Order",
                    ColumnName = "ORIGNUMB",
                    Description = "Original (Sales Order) Number",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "MWHBASEID",
                    ColumnName = "MWHBASEID",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Document Date",
                    ColumnName = "DOCDATE",
                    Description = "Document Date",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Customer Number",
                    ColumnName = "CUSTNMBR",
                    Description = "Customer Number",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Customer Name",
                    ColumnName = "CUSTNAME",
                    Description = "Customer Name",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Customer PO Number",
                    ColumnName = "CSTPONBR",
                    Description = "Customer PO Number",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "EUSERPONUM",
                    ColumnName = "EUSERPONUM",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Currency",
                    ColumnName = "CURNCYID",
                    Description = "Currency",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Exchange Rate",
                    ColumnName = "EXCHRATE",
                    Description = "Exchange Rate",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "MWH Salesperson",
                    ColumnName = "SLPRSNID",
                    Description = "MicroWarehouse Salesperson",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Item Number",
                    ColumnName = "ITEMNMBR",
                    Description = "Item Number",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Item Description",
                    ColumnName = "ITEMDESC",
                    Description = "Item Description",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Vendor",
                    ColumnName = "ITMSHNAM",
                    Description = "Vendor Name",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "CATEGORY",
                    ColumnName = "CATEGORY",
                    Description = "Category",
                    IncludeByDefault = false,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "SUBCATEGORY",
                    ColumnName = "SUBCATEGORY",
                    Description = "Sub Category",
                    IncludeByDefault = false,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Quantity",
                    ColumnName = "QUANTITY",
                    Description = "Quantity",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "UNITPRICE",
                    ColumnName = "UNITPRICE",
                    Description = "Unit Price in Euro",
                    IncludeByDefault = false,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Price",
                    ColumnName = "O_UNITPRICE",
                    Description = "Unit Price in Currency",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "EXTDPRICE",
                    ColumnName = "EXTDPRICE",
                    Description = "Total Line Price in Euro",
                    IncludeByDefault = false,
                    ReportType = ReportType.InvoiceReport
                },
                new AvailableReportColumn()
                {
                    Name = "Total",
                    ColumnName = "O_EXTDPRICE",
                    Description = "Total Line Price in Currency",
                    IncludeByDefault = true,
                    ReportType = ReportType.InvoiceReport
                },
                // Delivery report available columns
                new AvailableReportColumn()
                {
                    Name = "REPORTSTATUS",
                    ColumnName = "REPORTSTATUS",
                    Description = "New, Reported, History",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "DATEADDED",
                    ColumnName = "DATEADDED",
                    Description = "Date Added to Newton",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "DATEREPORTED",
                    ColumnName = "DATEREPORTED",
                    Description = "Date Reported",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Docket Number",
                    ColumnName = "DNOTENUM",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Create Date",
                    ColumnName = "CREATDAT",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Created By",
                    ColumnName = "CREATEBY",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Check Date",
                    ColumnName = "CHECKDAT",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Checked By",
                    ColumnName = "CHECKBY",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Invoice Number",
                    ColumnName = "INVNUMBE",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Ordered By",
                    ColumnName = "ORDRDBY",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Order Date",
                    ColumnName = "ORDRDATE",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Sales Order Number",
                    ColumnName = "SOPNUMBE",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Customer Number",
                    ColumnName = "CUSTNMBR",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Customer Name",
                    ColumnName = "CUSTNAME",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Customer PO Number",
                    ColumnName = "CSTPONBR",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "End User PO Number",
                    ColumnName = "EUSERPONUM",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Contact Person",
                    ColumnName = "CNTCPRSN",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Sales Person",
                    ColumnName = "SLPRSNID",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Ship Address 1",
                    ColumnName = "SHIPADD1",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Ship Address 2",
                    ColumnName = "SHIPADD2",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Ship Address 3",
                    ColumnName = "SHIPADD3",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Ship Address 4",
                    ColumnName = "SHIPADD4",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Ship Address 5",
                    ColumnName = "SHIPADD5",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Ship Address 6",
                    ColumnName = "SHIPADD6",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Ship Address 7",
                    ColumnName = "SHIPADD7",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Ship Address 8",
                    ColumnName = "SHIPADD8",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Ship Address 9",
                    ColumnName = "SHIPADD9",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Item Line",
                    ColumnName = "LNITMSEQ",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Vendor",
                    ColumnName = "VENDOR",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = " Item Number",
                    ColumnName = "ITEMNMBR",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Item Description",
                    ColumnName = "ITEMDESC",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Order Qty",
                    ColumnName = "QUANTITY",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Delivered Qty",
                    ColumnName = "QTYTPICK",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Comment 1",
                    ColumnName = "COMMENT1",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Comment 2",
                    ColumnName = "COMMENT2",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Comment 3",
                    ColumnName = "COMMENT3",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                new AvailableReportColumn()
                {
                    Name = "Comment 4",
                    ColumnName = "COMMENT4",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.DeliveryReport
                },
                // Serial Number report available columns
                new AvailableReportColumn()
                {
                    Name = "REPORTSTATUS",
                    ColumnName = "REPORTSTATUS",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "DATEADDED",
                    ColumnName = "DATEADDED",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "DATEREPORTED",
                    ColumnName = "DATEREPORTED",
                    Description = "",
                    IncludeByDefault = false,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "DNOTENUM",
                    ColumnName = "DNOTENUM",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "CREATDAT",
                    ColumnName = "CREATDAT",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "Customer Number",
                    ColumnName = "CUSTNMBR",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "Invoice Number",
                    ColumnName = "INVNUMBE",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "Sales Order",
                    ColumnName = "SOPNUMBE",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "Customer PO",
                    ColumnName = "CSTPONBR",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "End User PO",
                    ColumnName = "EUSERPONUM",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "Ship Address",
                    ColumnName = "SHIPADD1",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "Item Number",
                    ColumnName = "ITEMNMBR",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "Item Description",
                    ColumnName = "ITEMDESC",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "Vendor",
                    ColumnName = "VENDOR",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "Qty",
                    ColumnName = "QUANTITY",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
                new AvailableReportColumn()
                {
                    Name = "Serial Number",
                    ColumnName = "SERIALNUM",
                    Description = "",
                    IncludeByDefault = true,
                    ReportType = ReportType.SerialNumberReport
                },
            };
            return availableColumns;
        }
    }
}
