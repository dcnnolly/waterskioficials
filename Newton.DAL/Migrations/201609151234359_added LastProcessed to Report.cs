namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedLastProcessedtoReport : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reports", "LastProcessed", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reports", "LastProcessed");
        }
    }
}
