namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedformatstocontact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "PDF", c => c.Boolean());
            AddColumn("dbo.Contacts", "XML", c => c.Boolean());
            AddColumn("dbo.Contacts", "CSV", c => c.Boolean());
            AddColumn("dbo.Contacts", "CSPSummary", c => c.Boolean());
            AddColumn("dbo.Contacts", "CSPDetail", c => c.Boolean());
            AddColumn("dbo.Contacts", "CSPRaw", c => c.Boolean());
            DropColumn("dbo.Contacts", "InvoiceFormat_PDF");
            DropColumn("dbo.Contacts", "InvoiceFormat_XML");
            DropColumn("dbo.Contacts", "InvoiceFormat_CSV");
            DropColumn("dbo.Contacts", "InvoiceFormat_CSPSummary");
            DropColumn("dbo.Contacts", "InvoiceFormat_CSPDetail");
            DropColumn("dbo.Contacts", "InvoiceFormat_CSPRaw");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contacts", "InvoiceFormat_CSPRaw", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormat_CSPDetail", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormat_CSPSummary", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormat_CSV", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormat_XML", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormat_PDF", c => c.Boolean());
            DropColumn("dbo.Contacts", "CSPRaw");
            DropColumn("dbo.Contacts", "CSPDetail");
            DropColumn("dbo.Contacts", "CSPSummary");
            DropColumn("dbo.Contacts", "CSV");
            DropColumn("dbo.Contacts", "XML");
            DropColumn("dbo.Contacts", "PDF");
        }
    }
}
