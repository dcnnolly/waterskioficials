namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedcountryandregion : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Countries",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Regions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            AddColumn("dbo.Contacts", "CountryId", c => c.Int());
            AddColumn("dbo.Contacts", "RegionId", c => c.Int());
            CreateIndex("dbo.Contacts", "CountryId");
            CreateIndex("dbo.Contacts", "RegionId");
            AddForeignKey("dbo.Contacts", "CountryId", "dbo.Countries", "Id");
            AddForeignKey("dbo.Contacts", "RegionId", "dbo.Regions", "Id");
            DropColumn("dbo.Contacts", "IsSystemContact");
            DropColumn("dbo.Contacts", "AppearInListOrderConfirmations");
            DropColumn("dbo.Contacts", "FirstInListOrderConfirmations");
            DropColumn("dbo.Contacts", "ReceiveAllOrderConfirmations");
            DropColumn("dbo.Contacts", "ReceiveOwnInvoices");
            DropColumn("dbo.Contacts", "ReceiveAllInvoices");
            DropColumn("dbo.Contacts", "ReceiveStatements");
            DropColumn("dbo.Contacts", "ReceiveShipConfirmations");
            DropColumn("dbo.Contacts", "ReceiveAllShipConfirmations");
            DropColumn("dbo.Contacts", "OptOutMarketing");
            DropColumn("dbo.Contacts", "ReceiveSpecials");
            DropColumn("dbo.Contacts", "ReceiveNewsletters");
            DropColumn("dbo.Contacts", "ReceiveTraining");
            DropColumn("dbo.Contacts", "PrimaryRoleId");
            DropColumn("dbo.Contacts", "PDF");
            DropColumn("dbo.Contacts", "XML");
            DropColumn("dbo.Contacts", "CSV");
            DropColumn("dbo.Contacts", "CSPSummary");
            DropColumn("dbo.Contacts", "CSPDetail");
            DropColumn("dbo.Contacts", "CSPRaw");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contacts", "CSPRaw", c => c.Boolean());
            AddColumn("dbo.Contacts", "CSPDetail", c => c.Boolean());
            AddColumn("dbo.Contacts", "CSPSummary", c => c.Boolean());
            AddColumn("dbo.Contacts", "CSV", c => c.Boolean());
            AddColumn("dbo.Contacts", "XML", c => c.Boolean());
            AddColumn("dbo.Contacts", "PDF", c => c.Boolean());
            AddColumn("dbo.Contacts", "PrimaryRoleId", c => c.Int());
            AddColumn("dbo.Contacts", "ReceiveTraining", c => c.Boolean());
            AddColumn("dbo.Contacts", "ReceiveNewsletters", c => c.Boolean());
            AddColumn("dbo.Contacts", "ReceiveSpecials", c => c.Boolean());
            AddColumn("dbo.Contacts", "OptOutMarketing", c => c.Boolean());
            AddColumn("dbo.Contacts", "ReceiveAllShipConfirmations", c => c.Boolean(nullable: false));
            AddColumn("dbo.Contacts", "ReceiveShipConfirmations", c => c.Boolean(nullable: false));
            AddColumn("dbo.Contacts", "ReceiveStatements", c => c.Boolean(nullable: false));
            AddColumn("dbo.Contacts", "ReceiveAllInvoices", c => c.Boolean(nullable: false));
            AddColumn("dbo.Contacts", "ReceiveOwnInvoices", c => c.Boolean(nullable: false));
            AddColumn("dbo.Contacts", "ReceiveAllOrderConfirmations", c => c.Boolean(nullable: false));
            AddColumn("dbo.Contacts", "FirstInListOrderConfirmations", c => c.Boolean(nullable: false));
            AddColumn("dbo.Contacts", "AppearInListOrderConfirmations", c => c.Boolean(nullable: false));
            AddColumn("dbo.Contacts", "IsSystemContact", c => c.Boolean(nullable: false));
            DropForeignKey("dbo.Contacts", "RegionId", "dbo.Regions");
            DropForeignKey("dbo.Contacts", "CountryId", "dbo.Countries");
            DropIndex("dbo.Contacts", new[] { "RegionId" });
            DropIndex("dbo.Contacts", new[] { "CountryId" });
            DropColumn("dbo.Contacts", "RegionId");
            DropColumn("dbo.Contacts", "CountryId");
            DropTable("dbo.Regions");
            DropTable("dbo.Countries");
        }
    }
}
