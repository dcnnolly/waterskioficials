namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedreportcontactmodel : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ContactReports", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.ContactReports", "ReportId", "dbo.Reports");
            DropIndex("dbo.ContactReports", new[] { "ContactId" });
            DropIndex("dbo.ContactReports", new[] { "ReportId" });
            CreateTable(
                "dbo.ReportContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ReportId = c.Int(nullable: false),
                        ContactId = c.Int(nullable: false),
                        LastProcessed = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .ForeignKey("dbo.Reports", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ReportId)
                .Index(t => t.ContactId);
            
            DropColumn("dbo.Reports", "LastProcessed");
            DropTable("dbo.ContactReports");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.ContactReports",
                c => new
                    {
                        ContactId = c.Int(nullable: false),
                        ReportId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ContactId, t.ReportId });
            
            AddColumn("dbo.Reports", "LastProcessed", c => c.DateTime());
            DropForeignKey("dbo.ReportContacts", "ReportId", "dbo.Reports");
            DropForeignKey("dbo.ReportContacts", "ContactId", "dbo.Contacts");
            DropIndex("dbo.ReportContacts", new[] { "ContactId" });
            DropIndex("dbo.ReportContacts", new[] { "ReportId" });
            DropTable("dbo.ReportContacts");
            CreateIndex("dbo.ContactReports", "ReportId");
            CreateIndex("dbo.ContactReports", "ContactId");
            AddForeignKey("dbo.ContactReports", "ReportId", "dbo.Reports", "Id", cascadeDelete: true);
            AddForeignKey("dbo.ContactReports", "ContactId", "dbo.Contacts", "Id", cascadeDelete: true);
        }
    }
}
