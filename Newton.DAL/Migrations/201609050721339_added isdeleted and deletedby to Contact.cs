namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedisdeletedanddeletedbytoContact : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "IsDeleted", c => c.Boolean(nullable: false));
            AddColumn("dbo.Contacts", "DeletedBy", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contacts", "DeletedBy");
            DropColumn("dbo.Contacts", "IsDeleted");
        }
    }
}
