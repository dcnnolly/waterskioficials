namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class invoiceformat2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "InvoiceFormat_PDF", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormat_XML", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormat_CSV", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormat_CSPSummary", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormat_CSPDetail", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormat_CSPRaw", c => c.Boolean());
            DropColumn("dbo.Contacts", "InvoiceFormats_PDFFormat");
            DropColumn("dbo.Contacts", "InvoiceFormats_XMLFormat");
            DropColumn("dbo.Contacts", "InvoiceFormats_CSVFormat");
            DropColumn("dbo.Contacts", "InvoiceFormats_CSPSummaryFormat");
            DropColumn("dbo.Contacts", "InvoiceFormats_CSPDetailFormat");
            DropColumn("dbo.Contacts", "InvoiceFormats_CSPRaw");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Contacts", "InvoiceFormats_CSPRaw", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormats_CSPDetailFormat", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormats_CSPSummaryFormat", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormats_CSVFormat", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormats_XMLFormat", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormats_PDFFormat", c => c.Boolean());
            DropColumn("dbo.Contacts", "InvoiceFormat_CSPRaw");
            DropColumn("dbo.Contacts", "InvoiceFormat_CSPDetail");
            DropColumn("dbo.Contacts", "InvoiceFormat_CSPSummary");
            DropColumn("dbo.Contacts", "InvoiceFormat_CSV");
            DropColumn("dbo.Contacts", "InvoiceFormat_XML");
            DropColumn("dbo.Contacts", "InvoiceFormat_PDF");
        }
    }
}
