namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addparenttoreport3 : DbMigration
    {
        public override void Up()
        {
            //AddColumn("dbo.DeliveryReportDataItems", "PARENT", c => c.String());
            //AddColumn("dbo.InvoiceReportDataItems", "PARENT", c => c.String());
            //AddColumn("dbo.SerialNumberReportDataItems", "PARENT", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SerialNumberReportDataItems", "PARENT");
            DropColumn("dbo.InvoiceReportDataItems", "PARENT");
            DropColumn("dbo.DeliveryReportDataItems", "PARENT");
        }
    }
}
