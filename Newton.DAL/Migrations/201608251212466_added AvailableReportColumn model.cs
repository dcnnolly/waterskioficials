namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedAvailableReportColumnmodel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AvailableReportColumns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ColumnName = c.String(nullable: false, maxLength: 50),
                        Description = c.String(),
                        IncludeByDefault = c.Boolean(nullable: false),
                        ReportType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.AvailableReportColumns");
        }
    }
}
