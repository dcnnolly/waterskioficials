namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedIsSelectedByDefaultfromReportColumnmodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportColumns", "OrderInReport", c => c.Int(nullable: false));
            DropColumn("dbo.ReportColumns", "IncludedByDefault");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ReportColumns", "IncludedByDefault", c => c.Boolean(nullable: false));
            DropColumn("dbo.ReportColumns", "OrderInReport");
        }
    }
}
