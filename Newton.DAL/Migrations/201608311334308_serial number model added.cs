namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class serialnumbermodeladded : DbMigration
    {
        public override void Up()
        {
            //DropPrimaryKey("dbo.DeliveryReportDataItems");
            //DropPrimaryKey("dbo.InvoiceReportDataItems");
            //CreateTable(
            //    "dbo.SerialNumberReportDataItems",
            //    c => new
            //        {
            //            Id = c.Guid(nullable: false),
            //            REPORTSTATUS = c.String(),
            //            DATEADDED = c.DateTime(nullable: false),
            //            DNOTENUM = c.Int(nullable: false),
            //            CREATDAT = c.DateTime(nullable: false),
            //            INVNUMBE = c.String(),
            //            SOPNUMBE = c.String(),
            //            CSTPONBR = c.String(),
            //            EUSERPONUM = c.String(),
            //            SHIPADD1 = c.String(),
            //            ITEMNMBR = c.String(),
            //            ITEMDESC = c.String(),
            //            VENDOR = c.String(),
            //            QUANTITY = c.Int(nullable: false),
            //            SERIALNUM = c.String(),
            //            CUSTNMBR = c.String(),
            //            DATEREPORTED = c.DateTime(nullable: false),
            //        })
            //    .PrimaryKey(t => t.Id);
            Sql("CREATE VIEW [dbo].[SerialNumberReportDataItems] AS SELECT * FROM [Newton.ReportData].[dbo].[report_data_serialnum]");

            //AlterColumn("dbo.DeliveryReportDataItems", "Id", c => c.Guid(nullable: false));
            //AlterColumn("dbo.InvoiceReportDataItems", "Id", c => c.Guid(nullable: false));
            //AddPrimaryKey("dbo.DeliveryReportDataItems", "Id");
            //AddPrimaryKey("dbo.InvoiceReportDataItems", "Id");
        }
        
        public override void Down()
        {
            //DropPrimaryKey("dbo.InvoiceReportDataItems");
            //DropPrimaryKey("dbo.DeliveryReportDataItems");
            //AlterColumn("dbo.InvoiceReportDataItems", "Id", c => c.String(nullable: false, maxLength: 128));
            //AlterColumn("dbo.DeliveryReportDataItems", "Id", c => c.String(nullable: false, maxLength: 128));
            //DropTable("dbo.SerialNumberReportDataItems");
            //AddPrimaryKey("dbo.InvoiceReportDataItems", "Id");
            //AddPrimaryKey("dbo.DeliveryReportDataItems", "Id");
            Sql("DROP VIEW dbo.SerialNumberReportDataItems");
        }
    }
}
