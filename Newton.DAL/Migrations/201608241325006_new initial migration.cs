namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class newinitialmigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(),
                        LastName = c.String(),
                        Email = c.String(),
                        AlternateEmail = c.String(),
                        Phone = c.String(),
                        Mobile = c.String(),
                        IsSystemContact = c.Boolean(nullable: false),
                        AppearInListOrderConfirmations = c.Boolean(nullable: false),
                        FirstInListOrderConfirmations = c.Boolean(nullable: false),
                        ReceiveAllOrderConfirmations = c.Boolean(nullable: false),
                        ReceiveOwnInvoices = c.Boolean(nullable: false),
                        ReceiveAllInvoices = c.Boolean(nullable: false),
                        ReceiveStatements = c.Boolean(nullable: false),
                        ReceiveShipConfirmations = c.Boolean(nullable: false),
                        ReceiveAllShipConfirmations = c.Boolean(nullable: false),
                        OptOutMarketing = c.Boolean(),
                        ReceiveSpecials = c.Boolean(),
                        ReceiveNewsletters = c.Boolean(),
                        ReceiveTraining = c.Boolean(),
                        CustomerNumber = c.String(maxLength: 15, fixedLength: true),
                        PrimaryRoleId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Reports",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        ReportType = c.Int(nullable: false),
                        ReportFormat = c.Int(nullable: false),
                        ReportScheduleFrequency = c.Int(nullable: false),
                        ScheduleStartTime = c.DateTime(),
                        ScheduleEndTime = c.DateTime(),
                        ExecutionTime = c.DateTime(),
                        ExecutionDayOfWeek = c.Int(),
                        ExecutionDay = c.DateTime(),
                        CustomerNumber = c.String(maxLength: 15, fixedLength: true),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReportColumns",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        ColumnName = c.String(nullable: false, maxLength: 200),
                        CustomerNumber = c.String(),
                        Description = c.String(),
                        ReportId = c.Int(nullable: false),
                        IncludedByDefault = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Reports", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ReportId);
            
            CreateTable(
                "dbo.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        IsUnique = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CustomerInfo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CustomerNumber = c.String(maxLength: 15, fixedLength: true),
                        AppleResellerId = c.String(),
                        AppleDEPId = c.String(),
                        MicrosoftMPNId = c.String(),
                    })
                .PrimaryKey(t => t.Id);

            //CreateTable(
            //    "dbo.Customers",
            //    c => new
            //        {
            //            CustomerNumber = c.String(nullable: false, maxLength: 15, fixedLength: true),
            //            Name = c.String(),
            //        })
            //    .PrimaryKey(t => t.CustomerNumber);
            CreateTable(
                "dbo.HistoryItems",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ContactId = c.Int(nullable: false),
                        CreatorName = c.String(nullable: false, maxLength: 100),
                        ContactName = c.String(nullable: false, maxLength: 100),
                        DateCreated = c.DateTime(nullable: false),
                        HistorySubTypeId = c.Int(nullable: false),
                        Detail = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HistorySubTypes", t => t.HistorySubTypeId, cascadeDelete: true)
                .Index(t => t.HistorySubTypeId);
            
            CreateTable(
                "dbo.HistorySubTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        HistoryTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HistoryTypes", t => t.HistoryTypeId, cascadeDelete: true)
                .Index(t => t.HistoryTypeId);
            
            CreateTable(
                "dbo.HistoryTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ReportProcessingLog",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        ReportId = c.Int(nullable: false),
                        ContactId = c.Int(nullable: false),
                        DateProcessed = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .ForeignKey("dbo.Reports", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ReportId)
                .Index(t => t.ContactId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 200),
                        Username = c.String(),
                        UserType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ContactReports",
                c => new
                    {
                        ContactId = c.Int(nullable: false),
                        ReportId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ContactId, t.ReportId })
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .ForeignKey("dbo.Reports", t => t.ReportId, cascadeDelete: true)
                .Index(t => t.ContactId)
                .Index(t => t.ReportId);
            
            CreateTable(
                "dbo.ContactRoles",
                c => new
                    {
                        ContactId = c.Int(nullable: false),
                        RoleId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ContactId, t.RoleId })
                .ForeignKey("dbo.Contacts", t => t.ContactId, cascadeDelete: true)
                .ForeignKey("dbo.Roles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.ContactId)
                .Index(t => t.RoleId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReportProcessingLog", "ReportId", "dbo.Reports");
            DropForeignKey("dbo.ReportProcessingLog", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.HistorySubTypes", "HistoryTypeId", "dbo.HistoryTypes");
            DropForeignKey("dbo.HistoryItems", "HistorySubTypeId", "dbo.HistorySubTypes");
            DropForeignKey("dbo.ContactRoles", "RoleId", "dbo.Roles");
            DropForeignKey("dbo.ContactRoles", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.ContactReports", "ReportId", "dbo.Reports");
            DropForeignKey("dbo.ContactReports", "ContactId", "dbo.Contacts");
            DropForeignKey("dbo.ReportColumns", "ReportId", "dbo.Reports");
            DropIndex("dbo.ContactRoles", new[] { "RoleId" });
            DropIndex("dbo.ContactRoles", new[] { "ContactId" });
            DropIndex("dbo.ContactReports", new[] { "ReportId" });
            DropIndex("dbo.ContactReports", new[] { "ContactId" });
            DropIndex("dbo.ReportProcessingLog", new[] { "ContactId" });
            DropIndex("dbo.ReportProcessingLog", new[] { "ReportId" });
            DropIndex("dbo.HistorySubTypes", new[] { "HistoryTypeId" });
            DropIndex("dbo.HistoryItems", new[] { "HistorySubTypeId" });
            DropIndex("dbo.ReportColumns", new[] { "ReportId" });
            DropTable("dbo.ContactRoles");
            DropTable("dbo.ContactReports");
            DropTable("dbo.Users");
            DropTable("dbo.ReportProcessingLog");
            DropTable("dbo.HistoryTypes");
            DropTable("dbo.HistorySubTypes");
            DropTable("dbo.HistoryItems");
            DropTable("dbo.Customers");
            DropTable("dbo.CustomerInfo");
            DropTable("dbo.Roles");
            DropTable("dbo.ReportColumns");
            DropTable("dbo.Reports");
            DropTable("dbo.Contacts");
        }
    }
}
