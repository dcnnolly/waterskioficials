namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class consolidatefunctionalityadded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reports", "Consolidate", c => c.Boolean(nullable: false));
            AddColumn("dbo.CustomerInfo", "Parent", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomerInfo", "Parent");
            DropColumn("dbo.Reports", "Consolidate");
        }
    }
}
