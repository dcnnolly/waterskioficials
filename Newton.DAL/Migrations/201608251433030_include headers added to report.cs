namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class includeheadersaddedtoreport : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Reports", "IncludeHeaders", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Reports", "IncludeHeaders");
        }
    }
}
