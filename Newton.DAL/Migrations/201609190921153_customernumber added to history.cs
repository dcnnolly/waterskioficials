namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class customernumberaddedtohistory : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HistoryItems", "CustomerNumber", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.HistoryItems", "CustomerNumber");
        }
    }
}
