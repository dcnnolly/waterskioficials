namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class formatsadded : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "InvoiceFormats_PDFFormat", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormats_XMLFormat", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormats_CSVFormat", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormats_CSPSummaryFormat", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormats_CSPDetailFormat", c => c.Boolean());
            AddColumn("dbo.Contacts", "InvoiceFormats_CSPRaw", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contacts", "InvoiceFormats_CSPRaw");
            DropColumn("dbo.Contacts", "InvoiceFormats_CSPDetailFormat");
            DropColumn("dbo.Contacts", "InvoiceFormats_CSPSummaryFormat");
            DropColumn("dbo.Contacts", "InvoiceFormats_CSVFormat");
            DropColumn("dbo.Contacts", "InvoiceFormats_XMLFormat");
            DropColumn("dbo.Contacts", "InvoiceFormats_PDFFormat");
        }
    }
}
