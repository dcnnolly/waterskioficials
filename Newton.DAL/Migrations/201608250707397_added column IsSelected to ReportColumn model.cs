namespace Newton.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addedcolumnIsSelectedtoReportColumnmodel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReportColumns", "IsSelected", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReportColumns", "IsSelected");
        }
    }
}
