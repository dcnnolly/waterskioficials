﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class CustomerMap : EntityTypeConfiguration<Customer>
    {
        public CustomerMap()
        {
            // Primary Key
            this.HasKey(c => c.CustomerNumber);

            this.Property(c => c.CustomerNumber)
                .HasMaxLength(15)
                .IsFixedLength();

            // Table & Column Mappings
            this.ToTable("Customers");
            this.Property(c => c.CustomerNumber).HasColumnName("CustomerNumber");
            this.Property(c => c.Name).HasColumnName("Name");
        }
    }
}
