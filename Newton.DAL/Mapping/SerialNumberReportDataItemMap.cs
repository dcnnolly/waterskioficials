﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class SerialNumberReportDataItemMap : EntityTypeConfiguration<SerialNumberReportDataItem>
    {
        public SerialNumberReportDataItemMap()
        {
            this.HasKey(snrdi => snrdi.Id);

            this.ToTable("SerialNumberReportDataItems");
        }
    }
}
