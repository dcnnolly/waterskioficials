﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class HistoryMap : EntityTypeConfiguration<History>
    {
        public HistoryMap()
        {
            // Primary Key
            this.HasKey(h => h.Id);

            // Properties
            this.Property(c => c.CreatorName)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(c => c.ContactName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("HistoryItems");
            this.Property(c => c.Id).HasColumnName("Id");
            this.Property(c => c.CreatorName).HasColumnName("CreatorName");
            this.Property(c => c.ContactName).HasColumnName("ContactName");
            this.Property(c => c.DateCreated).HasColumnName("DateCreated");
            this.Property(c => c.Detail).HasColumnName("Detail");
            this.Property(c => c.HistorySubTypeId).HasColumnName("HistorySubTypeId");
        }
    }
}
