﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class ContactMap : EntityTypeConfiguration<Contact>
    {
        public ContactMap()
        {
            // Primary Key
            this.HasKey(c => c.Id);

            this.Property(c => c.CustomerNumber)
                .HasMaxLength(15)
                .IsFixedLength();

            // Table & Column Mappings
            this.ToTable("Contacts");
            this.Property(c => c.Id).HasColumnName("Id");
            this.Property(c => c.AlternateEmail).HasColumnName("AlternateEmail");
            this.Property(c => c.CustomerNumber).HasColumnName("CustomerNumber");
            this.Property(c => c.Email).HasColumnName("Email");
            this.Property(c => c.FirstName).HasColumnName("FirstName");
            this.Property(c => c.LastName).HasColumnName("LastName");
            this.Property(c => c.Mobile).HasColumnName("Mobile");
            this.Property(c => c.Phone).HasColumnName("Phone");
            
            this.HasMany(c => c.Roles)
                .WithMany(r => r.Contacts)
                .Map(m =>
                {
                    m.ToTable("ContactRoles");
                    m.MapLeftKey("ContactId");
                    m.MapRightKey("RoleId");
                });
            
        }
    }
}
