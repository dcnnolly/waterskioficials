﻿using Newton.Domain.Models;
using System.Data.Entity.ModelConfiguration;

namespace Newton.DAL.Mapping
{
    public class DeliveryReportDataItemMap : EntityTypeConfiguration<DeliveryReportDataItem>
    {
        public DeliveryReportDataItemMap()
        {
            this.HasKey(drdi => drdi.Id);

            this.ToTable("DeliveryReportDataItems");
        }
    }
}