﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class AvailableReportColumnMap : EntityTypeConfiguration<AvailableReportColumn>
    {
        public AvailableReportColumnMap()
        {
            // primary key
            this.HasKey(arc => arc.Id);

            this.Property(c => c.ColumnName)
                .HasMaxLength(50)
                .IsRequired();

            // Table & Column Mappings
            this.ToTable("AvailableReportColumns");
            this.Property(arc => arc.Id).HasColumnName("Id");
            this.Property(arc => arc.Name).HasColumnName("Name");
            this.Property(arc => arc.ColumnName).HasColumnName("ColumnName");
            this.Property(arc => arc.Description).HasColumnName("Description");
            this.Property(arc => arc.IncludeByDefault).HasColumnName("IncludeByDefault");
            this.Property(arc => arc.ReportType).HasColumnName("ReportType");
        }
    }
}
