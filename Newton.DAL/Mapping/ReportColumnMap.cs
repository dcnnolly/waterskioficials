﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class ReportColumnMap : EntityTypeConfiguration<ReportColumn>
    {
        public ReportColumnMap()
        {
            // Primary Key
            this.HasKey(rc => rc.Id);

            // Properties
            this.Property(rc => rc.Name)
                .IsRequired()
                .HasMaxLength(200);
            this.Property(rc => rc.ColumnName)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("ReportColumns");
            this.Property(rc => rc.Id).HasColumnName("Id");
            this.Property(rc => rc.Name).HasColumnName("Name");
            this.Property(rc => rc.ColumnName).HasColumnName("ColumnName");
            this.Property(rc => rc.Description).HasColumnName("Description");
            this.Property(rc => rc.ReportId).HasColumnName("ReportId");
            this.Property(rc => rc.IsSelected).HasColumnName("IsSelected");
            this.Property(rc => rc.OrderInReport).HasColumnName("OrderInReport");
        }
    }
}
