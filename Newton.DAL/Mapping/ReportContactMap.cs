﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    class ReportContactMap : EntityTypeConfiguration<ReportContact>
    {
        public ReportContactMap()
        {
            this.HasKey(rc => rc.Id);

            this.ToTable("ReportContacts");

            this.Property(rc => rc.ReportId).IsRequired();
            this.Property(rc => rc.ContactId).IsRequired();
        }
    }
}
