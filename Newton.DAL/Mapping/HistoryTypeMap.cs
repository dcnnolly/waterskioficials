﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class HistoryTypeMap : EntityTypeConfiguration<HistoryType>
    {
        public HistoryTypeMap()
        {
            // Primary Key
            this.HasKey(ht => ht.Id);

            // Properties
            this.Property(ht => ht.Name)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("HistoryTypes");
            this.Property(c => c.Id).HasColumnName("Id");
            this.Property(c => c.Name).HasColumnName("Name");
        }
    }
}
