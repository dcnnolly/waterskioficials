﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class HistorySubTypeMap : EntityTypeConfiguration<HistorySubType>
    {
        public HistorySubTypeMap()
        {
            // Primary Key
            this.HasKey(c => c.Id);

            // Properties
            this.Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("HistorySubTypes");
            this.Property(c => c.Id).HasColumnName("Id");
            this.Property(c => c.Name).HasColumnName("Name");
            this.Property(c => c.HistoryTypeId).HasColumnName("HistoryTypeId");
        }
    }
}
