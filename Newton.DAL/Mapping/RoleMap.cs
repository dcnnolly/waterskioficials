﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class RoleMap : EntityTypeConfiguration<Role>
    {
        public RoleMap()
        {
            // Primary Key
            this.HasKey(r => r.Id);

            // Properties
            this.Property(c => c.Name)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Roles");
            this.Property(c => c.Id).HasColumnName("Id");
            this.Property(c => c.Name).HasColumnName("Name");
            this.Property(c => c.IsUnique).HasColumnName("IsUnique");
        }
    }
}
