﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class ReportMap : EntityTypeConfiguration<Report>
    {
        public ReportMap()
        {
            // Primary Key
            this.HasKey(r => r.Id);

            // Properties
            this.Property(r => r.Name)
                .IsRequired()
                .HasMaxLength(200);
            this.Property(r => r.ExecutionTime)
                .IsOptional();
            this.Property(r => r.ScheduleEndTime)
                .IsOptional();
            this.Property(r => r.ScheduleStartTime)
                .IsOptional();
            this.Property(r => r.CustomerNumber)
                .HasMaxLength(15)
                .IsFixedLength();

            // Table & Column Mappings
            this.ToTable("Reports");
            this.Property(r => r.Id).HasColumnName("Id");
            this.Property(r => r.Name).HasColumnName("Name");
            this.Property(r => r.ReportFormat).HasColumnName("ReportFormat");
            this.Property(r => r.ReportType).HasColumnName("ReportType");
            this.Property(r => r.ReportScheduleFrequency).HasColumnName("ReportScheduleFrequency");
            this.Property(r => r.ExecutionTime).HasColumnName("ExecutionTime");
            this.Property(r => r.ExecutionDayOfWeek).HasColumnName("ExecutionDayOfWeek");
            this.Property(r => r.ExecutionDay).HasColumnName("ExecutionDay");
            this.Property(r => r.ScheduleStartTime).HasColumnName("ScheduleStartTime");
            this.Property(r => r.ScheduleEndTime).HasColumnName("ScheduleEndTime");
            this.Property(r => r.CustomerNumber).HasColumnName("CustomerNumber");

            this.HasMany(r => r.ReportColumns)
                .WithRequired(rc => rc.Report)
                .HasForeignKey(r => r.ReportId);
        }
    }
}
