﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class InvoiceReportDataItemMap : EntityTypeConfiguration<InvoiceReportDataItem>
    {
        public InvoiceReportDataItemMap()
        {
            this.HasKey(irdi => irdi.Id);

            this.ToTable("InvoiceReportDataItems");
        }
    }
}
