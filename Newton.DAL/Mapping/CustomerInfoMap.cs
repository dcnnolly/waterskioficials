﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class CustomerInfoMap : EntityTypeConfiguration<CustomerInfo>
    {
        public CustomerInfoMap()
        {
            // Primary Key
            this.HasKey(ci => ci.Id);

            this.Property(ci => ci.CustomerNumber)
                .HasMaxLength(15)
                .IsFixedLength();

            // Table & Column Mappings
            this.ToTable("CustomerInfo");
            this.Property(ci => ci.Id).HasColumnName("Id");
            this.Property(ci => ci.CustomerNumber).HasColumnName("CustomerNumber");
            this.Property(ci => ci.AppleDEPId).HasColumnName("AppleDEPId");
            this.Property(ci => ci.AppleResellerId).HasColumnName("AppleResellerId");
            this.Property(ci => ci.MicrosoftMPNId).HasColumnName("MicrosoftMPNId");
        }
    }
}
