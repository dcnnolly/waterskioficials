﻿using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Newton.DAL.Mapping
{
    public class ReportProcessingLogMap : EntityTypeConfiguration<ReportProcessingLog>
    {
        public ReportProcessingLogMap()
        {
            this.HasKey(rpl => rpl.Id);
            this.Property(rpl => rpl.Id).HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);

            this.ToTable("ReportProcessingLog");
            this.Property(rpl => rpl.Id).HasColumnName("Id");
            this.Property(rpl => rpl.ContactId).HasColumnName("ContactId");
            this.Property(rpl => rpl.ReportId).HasColumnName("ReportId");
            this.Property(rpl => rpl.DateProcessed).HasColumnName("DateProcessed");
        }
    }
}
