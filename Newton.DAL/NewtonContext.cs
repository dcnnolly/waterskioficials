﻿using Microsoft.AspNet.Identity.EntityFramework;
using Newton.DAL.Mapping;
using Newton.Domain.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkManagerCMS.Data;

namespace Newton.DAL
{
    public class WaterSkiContext : IdentityDbContext<ApplicationUser>
    {
        public WaterSkiContext()
            : base("WaterSkiContext", throwIfV1Schema: false)
        {
            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<WorkManagerContext, Configuration>());
            Configuration.LazyLoadingEnabled = false;
        }

        public DbSet<Contact> Contacts { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<CustomerInfo> CustomerInfoItems { get; set; }
        public DbSet<History> HistoryItems { get; set; }
        public DbSet<Role> UserRoles { get; set; }
        public DbSet<User> ApplicationUsers { get; set; }
        public DbSet<HistoryType> HistoryTypes { get; set; }
        public DbSet<HistorySubType> HistorySubTypes { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<ReportColumn> ReportColumns { get; set; }
        public DbSet<ReportProcessingLog> ReportProcessingLog { get; set; }
        public DbSet<AvailableReportColumn> AvailableReportColumns { get; set; }

        // report models
        public DbSet<InvoiceReportDataItem> InvoiceReportDataItems { get; set; }
        public DbSet<DeliveryReportDataItem> DeliveryReportDataItems { get; set; }
        public DbSet<SerialNumberReportDataItem> SerialNumberReportDataItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ContactMap());
            modelBuilder.Configurations.Add(new CustomerMap());
            modelBuilder.Configurations.Add(new CustomerInfoMap());
            modelBuilder.Configurations.Add(new HistoryMap());
            modelBuilder.Configurations.Add(new RoleMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new HistoryTypeMap());
            modelBuilder.Configurations.Add(new HistorySubTypeMap());
            modelBuilder.Configurations.Add(new ReportMap());
            modelBuilder.Configurations.Add(new ReportColumnMap());
            modelBuilder.Configurations.Add(new ReportProcessingLogMap());
            modelBuilder.Configurations.Add(new AvailableReportColumnMap());

            // report model mappings
            modelBuilder.Configurations.Add(new InvoiceReportDataItemMap());
            modelBuilder.Configurations.Add(new DeliveryReportDataItemMap());
            modelBuilder.Configurations.Add(new SerialNumberReportDataItemMap());

            base.OnModelCreating(modelBuilder);
        }
    }
}
